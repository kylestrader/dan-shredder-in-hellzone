// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "FretboardNote.generated.h"

class USphereComponent;

UENUM(BlueprintType)
enum class E_NoteState : uint8
{
	E_IDLE			UMETA(DisplayName = "IDLE"),
	E_MOVING		UMETA(DisplayName = "MOVING")
};

UCLASS()
class ROCKINRAILS_API AFretboardNote : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFretboardNote();

	virtual void OnConstruction(const FTransform& Transform);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	void SetColor(FVector color);

	void SetState(E_NoteState state);


	UFUNCTION()
		void SetNoteSpeed(float speed);


private:


	UFUNCTION()
	void Move(float delta);

	UPROPERTY()
	float mNoteSpeed;
	UPROPERTY()
	E_NoteState mState;

	UPROPERTY()
	USphereComponent* mRootComponent;
	UPROPERTY()
	USphereComponent* mSphere;
	UPROPERTY()
	UStaticMeshComponent* mNoteMesh;
	UPROPERTY()
	UMaterialInstanceDynamic* mMID;

	UPROPERTY()
	FVector mColor;

	UPROPERTY()
		float originOffset_X;
};

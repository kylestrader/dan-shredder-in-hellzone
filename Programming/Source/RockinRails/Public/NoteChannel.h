   // Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "NoteStroke.h"
#include "GameDefines.h"

/**
 * 
 */
class ROCKINRAILS_API NoteChannel
{
public:
	NoteChannel(){};
	~NoteChannel(){};

	void PushNote(NoteStroke note) { m_Notes.Push(note); }
	inline void RemoveNoteAt(int index) { m_Notes.RemoveAt(index); }
	inline void ClearChannel() { m_Notes.Empty(); }
	TArray<NoteStroke> GetNotes() { return m_Notes; }

private:
	TArray<NoteStroke> m_Notes;
};

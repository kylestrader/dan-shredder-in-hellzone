// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "RiffBank.h"
#include "NoteConsumer.h"
#include "FretBoard.generated.h"

class AFretboardNote;
class AEnemy;


UENUM(BlueprintType)
enum EFretBoardState
{
	NORMAL				UMETA(DisplayName = "NORMAL"),
	SWAY				UMETA(DisplayName = "SWAY")
};

UCLASS()
class ROCKINRAILS_API AFretBoard : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFretBoard();

	virtual void PostInitializeComponents() override;

	virtual void OnConstruction(const FTransform& Transform);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick(float DeltaSeconds) override;
	
	UFUNCTION()
	void InterpretFretStates(int32 fretInput);

	UFUNCTION()
	void ClearSong();
	UFUNCTION()
	void PlaySong();
	UFUNCTION()
	void PlayAudio();

	UFUNCTION()
	void SetIsHyperDriveActive(bool state);

	UFUNCTION()
	float GenerateHyperdriveAmount();

	UFUNCTION()
	bool GetIsHyperDriveActive();

	UPROPERTY()
	UAudioComponent* RiffAudioComponent;
	UFUNCTION()
	void SetState(EFretBoardState state);

	UFUNCTION()
	void PointBeamAtEnemy(AEnemy* a_Enemy);

	UFUNCTION()
	void SpawnNotes(TArray<FNoteMetaData> notes);

	UFUNCTION()
	void SetGreenReceiverActive(bool active);

	UFUNCTION()
	void SetRedReceiverActive(bool active);

	UFUNCTION()
	void SetYellowReceiverActive(bool active);

	UFUNCTION()
	void SetBlueReceiverActive(bool active);

	UFUNCTION()
	void SetOrangeReceiverActive(bool active);

private:
	UPROPERTY()
	UStaticMeshComponent* mFretBoard;
	UPROPERTY()
	UStaticMeshComponent* mFretBoardMesh;
	UPROPERTY()
	UChildActorComponent* mGreenReciever;
	UPROPERTY()
	UChildActorComponent* mRedReciever;
	UPROPERTY()
	UChildActorComponent* mYellowReciever;
	UPROPERTY()
	UChildActorComponent* mBlueReciever;
	UPROPERTY()
	UChildActorComponent* mOrangeReciever;
	UPROPERTY()
	UPointLightComponent* mPointLight1;
	UPROPERTY()
	UPointLightComponent* mPointLight2;
	UPROPERTY()
	UPointLightComponent* mPointLight3;
	UPROPERTY()
	UParticleSystemComponent* mLightningAttackRight;
	UPROPERTY()
	UParticleSystemComponent* mLightningAttackLeft;
	UPROPERTY()
	UParticleSystemComponent* mSparksEnemy;
	UPROPERTY()
	UAudioComponent* mLaserSound;

	UFUNCTION()
	void SetRecieverDepressed(UChildActorComponent* recieverComp);

	UPROPERTY()
	int32 m_LowerBound;

	UPROPERTY()
	int32 m_UpperBound;

	UPROPERTY()
	int32 m_CurrentNote;

	UPROPERTY()
	FVector m_NewNoteColor;

	UPROPERTY()
	float m_Delay;

	UPROPERTY()
	float m_NoteSpeed;

	UPROPERTY()
	float m_SyncVal;

	UPROPERTY()
	float m_MaxHyperdrivePercent;

	UPROPERTY()
	bool m_IsHyperDriveActive;

	UPROPERTY()
	TArray<AFretboardNote*> m_Notes;

	UPROPERTY(EditAnywhere)
	ARiffBank* m_RiffBank;

	UPROPERTY()
	USoundWave* m_CurrentWaveForm;

	UPROPERTY()
	FTimerHandle m_SongTimerHandle;

	UPROPERTY()
	float m_TotalNoteCount;

	UPROPERTY()
	float m_HitPercent;

	UPROPERTY()
	float m_MissedCount;

	UPROPERTY()
	float m_HitCount;

	UPROPERTY()
	TEnumAsByte<EFretBoardState> m_State;
		

	UPROPERTY(EditAnywhere)
		UChildActorComponent * m_NoteConsumer;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "MeleeEnemy.h"
#include "Grunt.generated.h"

UCLASS()
class ROCKINRAILS_API AGrunt : public AMeleeEnemy
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGrunt();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

private:

	void Attack();

	UFUNCTION(BlueprintCallable, Category = "Enemy")
		void OnPathFinished();

	UFUNCTION()
		void OnOverlap(AActor* OtherActor);

};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Enemy.h"
#include "MeleeEnemy.generated.h"

UCLASS()
class ROCKINRAILS_API AMeleeEnemy : public AEnemy
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMeleeEnemy();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UFUNCTION(BlueprintCallable, Category = "Enemy")
		void OverlapEvent(AActor* other);

protected:

	void OnPathFinished();

private:

	// Utility variables

	UPROPERTY()
	FVector _playerLoc;

	UPROPERTY()
	FVector _enemyLoc;

	UPROPERTY()
	FVector _p2EDir;

	UPROPERTY()
	FRotator _lookRot;

	UPROPERTY()
	FRotator _facePlayerRot;

	UPROPERTY()
	bool mAttacking;

	UPROPERTY()
	float mAttackAnimTimer;

	const int MOVE_INPUT_SCALE = 100;
	const int DIE_DMG_AMT = 100;
	const int DIE_IMP_STR = 10;
	const float ATTACK_ANIM_MAX = 2.0f;

private:

	void SetTargeterVisibility();

	//UFUNCTION()
	//void OnBeginOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	//UFUNCTION(BlueprintCallable, Category = "Enemy")
	//void OnEndOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
};

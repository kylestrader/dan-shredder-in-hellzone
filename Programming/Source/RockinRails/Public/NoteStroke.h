// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameDefines.h"
#include "Utilities.h"

/**
 * 
 */
class ROCKINRAILS_API NoteStroke
{
public:
	NoteStroke(uint startTime, uint duration, uint note, uint channel, uint velocity) :
		m_StartTime(startTime),
		m_Duration(duration),
		m_Note(note),
		m_Channel(channel),
		m_Velocity(velocity) {}
	~NoteStroke(){};

	//Setters
	inline void SetStartTime(uint startTime) { m_StartTime = startTime; }
	inline void SetDuration(uint duration) { m_Duration = duration; }
	inline void SetNote(uint note) { m_Note = note; }
	inline void SetChanel(uint channel) { m_Channel = channel; }
	inline void SetVelocity(uint velocity) { m_Velocity = velocity; }

	//Getters
	inline uint GetStartTime() { return m_StartTime; }
	inline uint GetDuration() { return m_Duration; }
	inline uint GetNote() { return m_Note; }
	inline uint GetChannel() { return m_Channel; }
	inline uint GetVelocity() { return m_Velocity; }

private:
	uint m_StartTime;
	uint m_Duration;
	uint m_Note;
	uint m_Channel;
	uint m_Velocity;
};

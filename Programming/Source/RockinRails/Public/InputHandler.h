// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "PlayerGuitarController.h"
#include "InputHandler.generated.h"



UENUM(BlueprintType)
enum EInputEnum
{
	GUITAR_GREEN		UMETA(DisplayName = "GREEN"),
	GUITAR_RED			UMETA(DisplayName = "RED"),
	GUITAR_YELLOW		UMETA(DisplayName = "YELLOW"),
	GUITAR_BLUE			UMETA(DisplayName = "BLUE"),
	GUITAR_ORANGE		UMETA(DisplayName = "ORANGE"),
	GUITAR_STRUM_UP		UMETA(DisplayName = "STRUM UP"),
	GUITAR_STRUM_DOWN	UMETA(DisplayName = "STRUM DOWN"),
	GUITAR_WHAMMY		UMETA(DisplayName = "WHAMMY"),
	GUITAR_START		UMETA(DisplayName = "GUITAR START"),
	GUITAR_DPAD_UP		UMETA(DisplayName = "GUITAR UP"),
	GUITAR_DPAD_DOWN	UMETA(DisplayName = "GUITAR DOWN"),
	GUITAR_DPAD_LEFT	UMETA(DisplayName = "GUITAR LEFT"),
	GUITAR_DPAD_RIGHT	UMETA(DisplayName = "GUITAR RIGHT"),
	PAD_UP				UMETA(DisplayName = "PAD UP"),
	PAD_DOWN			UMETA(DisplayName = "PAD DOWN"),
	PAD_LEFT			UMETA(DisplayName = "PAD LEFT"),
	PAD_RIGHT			UMETA(DisplayName = "PAD RIGHT"),
	PAD_A				UMETA(DisplayName = "PAD A"),
	PAD_B				UMETA(DisplayName = "PAD B"),
	PAD_X				UMETA(DisplayName = "PAD X"),
	PAD_Y				UMETA(DisplayName = "PAD Y"),
	PAD_START			UMETA(DisplayName = "PAD START"),
	PAD_BACK			UMETA(DisplayName = "PAD BACK"),
	PAD_UP_RELEASE		UMETA(DisplayName = "PAD UP RELEASE"),
	PAD_DOWN_RELEASE	UMETA(DisplayName = "PAD DOWN RELEASE"),
	PAD_LEFT_RELEASE	UMETA(DisplayName = "PAD LEFT RELEASE"),
	PAD_RIGHT_RELEASE	UMETA(DisplayName = "PAD RIGHT RELEASE")
};

USTRUCT()
struct FInputAnalogStruct
{
	GENERATED_USTRUCT_BODY()


public:
	UPROPERTY()
	float AnalogInput = 0.0;

	UPROPERTY()
	TEnumAsByte<EInputEnum> Input = EInputEnum::GUITAR_GREEN;

	FInputAnalogStruct(){}

};


UCLASS()
class ROCKINRAILS_API AInputHandler : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AInputHandler();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	void HandleInputEvent(EInputEnum input);
	void HandleInputEventAnalog(EInputEnum input, float analogInput);

	void ArbitrateInput(APlayerGuitarController* player, EInputEnum input);
	void ArbitrateAnalogInput( APlayerGuitarController* player, FInputAnalogStruct input);

	void UpdateQueue(APlayerGuitarController* player);
private:
	UPROPERTY()
	TArray<TEnumAsByte<EInputEnum>> m_InputQueue;
	UPROPERTY()
	TArray<FInputAnalogStruct> m_InputAnalogQueue;
};

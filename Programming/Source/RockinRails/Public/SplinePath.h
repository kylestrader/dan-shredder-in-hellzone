// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "PathNode.h"
#include "SplinePath.generated.h"

// Forward declarations
class USplineComponent;

UCLASS()
class ROCKINRAILS_API ASplinePath : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASplinePath();

	// Called after the object needs to be updated
	virtual void OnConstruction(const FTransform& Transform) override;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UFUNCTION(BlueprintCallable, Category = "Accessors")
	USplineComponent* GetSplineComponent();

	FVector GetLocationAtDistanceAlongSpline(float Distance);

private:
	UPROPERTY()
	TArray<APathNode*> mPathNodeList;
	UPROPERTY(EditAnywhere)
	USplineComponent *mSplineComponent;
};

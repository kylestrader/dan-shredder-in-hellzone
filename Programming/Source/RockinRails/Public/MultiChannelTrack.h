// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <map>
#include <memory>

#include "GameDefines.h"
#include "Utilities.h"
#include "NoteChannel.h"

/**
 * 
 */
class ROCKINRAILS_API MultiChannelTrack
{
public:
	MultiChannelTrack(){};
	~MultiChannelTrack();

	void PushNoteStroke(uint startTime, uint duration, uint note, uint channel, uint velocity);
	void ClearTrack();
	void ClearChannel(uint channel);

	//Getters
	inline uint GetNumChannels(){ return m_Channels.size(); }
	inline std::shared_ptr<NoteChannel> GetChannel(int index) { return m_Channels[index]; }
	inline bool IsValidChannel(int index) { return !(m_Channels.find(index) == m_Channels.end()); }

private:
	std::map<int, std::shared_ptr<NoteChannel>> m_Channels;

	void PushNoteStroke(NoteStroke note);
};

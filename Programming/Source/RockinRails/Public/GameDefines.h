// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#define QUANTIZATION float(1/16)
#define XML_MIDI_DIRECTORY FPaths::GameContentDir() + "SongFiles/XmlMidiSheets/"

UENUM(BlueprintType)
enum class E_RefKey : uint8
{
	E_LEFT		UMETA(DisplayName = "LEFT"),
	E_RIGHT		UMETA(DisplayName = "RIGHT"),
	E_UP		UMETA(DisplayName = "UP"),
	E_DOWN		UMETA(DisplayName = "DOWN"),
	E_GREEN		UMETA(DisplayName = "GREEN"),
	E_YELLOW	UMETA(DisplayName = "YELLOW"),
	E_BLUE		UMETA(DisplayName = "BLUE"),
	E_RED		UMETA(DisplayName = "RED"),
	E_ORANGE	UMETA(DisplayName = "ORANGE")
};

/**
 * 
 */
class ROCKINRAILS_API GameDefines
{
public:
	GameDefines();
	~GameDefines();
};

#define GREEN_COLOR FVector(0.0f,1.0f,0.0f)
#define RED_COLOR FVector(1.0f,0.0f,0.0f)
#define YELLOW_COLOR FVector(1.0f,1.0f,0.0f)
#define BLUE_COLOR FVector(0.0f,0.0f,1.0f)
#define ORANGE_COLOR FVector(1.0f,0.05f,0.0f)

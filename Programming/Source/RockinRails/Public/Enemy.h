// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "PlayerGuitarController.h"
#include <map>
#include "Enemy.generated.h"

class UAudioComponent;
class UArrowComponent;
class USkeletalMesh;
class UChildActorComponent;
class UDestructibleComponent;
class UBillboardComponent;
class USplineComponent;

UENUM(BlueprintType)
enum class E_EnemyState : uint8
{
	E_IDLE			UMETA(DisplayName = "IDLE"),
	E_STAGGERED		UMETA(DisplayName = "STAGGERED"),
	E_PATHING		UMETA(DisplayName = "PATHING"),
	E_FIGHTING		UMETA(DisplayName = "FIGHTING"),
	E_DEAD			UMETA(DisplayName = "DEAD")
};

UCLASS()
class ROCKINRAILS_API AEnemy : public ACharacter
{
	GENERATED_BODY()

private:

	const int32 MOVE_INPUT_SCALE = 100;
	const int32 DIE_DMG_AMT = 100;
	const int32 DIE_IMP_STR = 10;
	const float DIE_DELAY_MAX = 2.0f;

private:

	void SetupComponents();

protected:

	// Enemy Characteristic Values
	UPROPERTY()
	int32 mHP;
	UPROPERTY()
	bool mAlive;
	UPROPERTY()
	bool mSpeaking;

	// Utility Variables
	UPROPERTY()
	int32 mCurrentVoiceLine;
	UPROPERTY()
	int32 mPrevVoiceLine;
	UPROPERTY()
	int32 mTargetPathNode;
	UPROPERTY()
	int32 mPrevPathNode;
	UPROPERTY()
	float mDeathDelayTimer;

	UPROPERTY(EditAnywhere)
	APlayerGuitarController *mPlayerRef;
	UPROPERTY(EditAnywhere)
	TArray<USoundWave*> mVoiceLinesKill;
	UPROPERTY(EditAnywhere)
	TArray<USoundWave*> mVoiceLinesSpawn;
	UPROPERTY(EditAnywhere)
	UAnimSequence* mAttackAnim;
	UPROPERTY(EditAnywhere)
	UAnimSequence* mIdleAnim;
	UPROPERTY(EditAnywhere)
	UAnimSequence* mStunAnim;

#pragma region Components
	UPROPERTY()
	UAudioComponent *mVoiceLinePlayer;
	UPROPERTY()
	UAudioComponent *mDeathSoundPlayer;
	UPROPERTY()
	UAudioComponent *mSlashSoundPlayer;
	UPROPERTY()
	UAudioComponent *mWindUpSoundPlayer;
	UPROPERTY()
	UArrowComponent *mArrowComponent;
	UPROPERTY()
	UChildActorComponent *mTargetComponent;
	UPROPERTY()
	UDestructibleComponent *mSwitchOut;
	UPROPERTY()
	UBillboardComponent *mArrowCenter;
	UPROPERTY()
	UBillboardComponent *mArrowUp;
	UPROPERTY()
	UBillboardComponent *mArrowDown;
	UPROPERTY()
	UBillboardComponent *mArrowLeft;
	UPROPERTY()
	UBillboardComponent *mArrowRight;
#pragma endregion Components

	//animation stuff needed
	UPROPERTY()
	TArray<E_RefKey> mSelectionKeys;
	UPROPERTY()
	USplineComponent* mSplineComponent;
	UPROPERTY()
	E_EnemyState mCurrentState;


protected:

	// Enemy Action Functions
	virtual void StartPath();

	UFUNCTION(BlueprintNativeEvent, Category = "Enemy")
	void Attack();

	void PlaySpawnVoiceLine();
	void PlayKillVoiceLine();

	// Event Call Functions

	virtual void OnPathFinished();

	UFUNCTION()
	void OnComponentBeginOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void OnComponentEndOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	UFUNCTION(BlueprintCallable, Category = "Enemy")
	void Die();
	UFUNCTION(BlueprintCallable, Category = "Enemy")
	void DamagePlayer();	

	UFUNCTION(BlueprintCallable, Category = "Enemy")
	void PlayIdleAnim();
	UFUNCTION(BlueprintCallable, Category = "Enemy")
	void PlayAttackAnim();
	UFUNCTION(BlueprintCallable, Category = "Enemy")
	void PlayStunAnim();

public:
	// Sets default values for this actor's properties
	AEnemy();
	virtual void OnConstruction(const FTransform& Transform) override;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	//Getters
	E_EnemyState GetState();
	bool IsAlive();
	TArray<E_RefKey> GetRefKey();

	UFUNCTION(BlueprintImplementableEvent, Category = "Enemy")
	void OnTargeted();

	UFUNCTION()
		void SetState(E_EnemyState state);

	UFUNCTION(BlueprintCallable, Category = "Enemy")
		void EnemyTakeDmg(int32 dmg); // Enemy HP damage, not pawn damage

	UFUNCTION()
		UChildActorComponent* GetChildActorComponent();
};

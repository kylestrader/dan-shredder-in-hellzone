// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#define print_text(text) if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 10, FColor::White,text)
#define print_error(text) if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::Red,text)
#define print_num(num) if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::White,FString::FromInt(num))
#define int2str(num) static_cast<ostringstream*>( &(ostringstream() << num) )->str();

#define uint unsigned int

/**
 * 
 */
class ROCKINRAILS_API Utilities
{
public:
	Utilities();
	~Utilities();
};

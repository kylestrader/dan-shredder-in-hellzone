// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "GrudgeBar.generated.h"

#define MAX_GRUDGE 100
#define MIN_GRUDGE 0
#define MS_PER_DEC 1

UCLASS()
class ROCKINRAILS_API AGrudgeBar : public AActor
{
	GENERATED_BODY()

private:

	UPROPERTY()
	float mCurrentValue;

	UPROPERTY(EditAnywhere)
	float mDefDec;

	UPROPERTY(EditAnywhere)
	float mMsSinceDec;

	UPROPERTY(EditAnywhere)
	bool mShouldDec;

public:	
	// Sets default values for this actor's properties
	AGrudgeBar();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	inline void SetValue(float newVal) { mCurrentValue = newVal; };

	UFUNCTION(BlueprintCallable, Category = "GrudgeBar")
	void ModValue(float mod);

	UFUNCTION(BlueprintCallable, Category = "GrudgeBar")
	float GetValue();
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <map>

#include "MultiChannelTrack.h"
#include "NoteStroke.h"

#include "GameFramework/Actor.h"
#include "RiffBank.generated.h"

USTRUCT(BlueprintType)
struct FRiffMetaData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Meta Data")
	FString m_XMLMidiName;	
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Meta Data")
	int32 m_BPM;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Meta Data")
	USoundWave* m_SoundWave;

	std::shared_ptr<MultiChannelTrack> m_MultiChannelTrack;
};

USTRUCT(BlueprintType)
struct FNoteMetaData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Meta Data")
	int32 m_StartTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Meta Data")
	int32 m_Duration;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Meta Data")
	int32 m_Note;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Meta Data")
	int32 m_Channel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Meta Data")
	int32 m_Velocity;
};

USTRUCT(BlueprintType)
struct FChannelMetaData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Meta Data")
	FString m_XMLMidiName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Meta Data")
	USoundWave* m_SoundWave;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Meta Data")
	TArray<FNoteMetaData> m_Notes;
};

USTRUCT(BlueprintType)
struct FChannelReturn
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Meta Data")
	USoundWave* m_SoundWave;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Meta Data")
	TArray<FNoteMetaData> m_Notes;
};

UCLASS()
class ROCKINRAILS_API ARiffBank : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARiffBank();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called when the actor is modified in the Editor
	virtual void OnConstruction(const FTransform& Transform) override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	//Convert channel into blueprintable meta data
	UFUNCTION(BlueprintCallable, Category = "Note Retrieval")
	FChannelReturn GetTrackAsNoteArray(FString trackName, int32 channelIndex = 1);

	UFUNCTION(BlueprintCallable, Category = "Note Retrieval")
	int32 GetNumTracks();

	UFUNCTION(BlueprintCallable, Category = "Note Retrieval")
	FChannelReturn GetRandomTrackAsArray(int32 channelIndex = 1);

	UFUNCTION(BlueprintCallable, Category = "Note Retrieval")
	FChannelReturn GetRandomTrackAsArrayWithExceptions(TArray<int32> exceptions, int32 channelIndex = 1);

	UFUNCTION(BlueprintCallable, Category = "Note Retrieval")
	FChannelReturn GetTrackAsArrayByIndex(int32 index, int32 channelIndex = 1);

private:
	UPROPERTY(EditAnywhere)
	TArray<FRiffMetaData> m_Riffs;

};

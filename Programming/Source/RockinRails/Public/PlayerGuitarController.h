// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "FretBoard.h"
#include "Blueprint/UserWidget.h"
#include "GameDefines.h"
#include "PlayerGuitarController.generated.h"


class AEnemy;
class AInputHandler;
class APathNode;
class ASplinePath;

UENUM(BlueprintType)
enum EPlayerState
{
	STATE_WALKING		UMETA(DisplayName = "Walking"),
	STATE_FIGHTING			UMETA(DisplayName = "Fighting")
};

UCLASS()
class ROCKINRAILS_API APlayerGuitarController : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerGuitarController();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void PostInitializeComponents() override;

	virtual void OnConstruction(const FTransform& Transform);

	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	UFUNCTION(BlueprintNativeEvent, Category = "Guitar Note Event")
	void GuitarGreen();
	UFUNCTION(BlueprintNativeEvent, Category = "Guitar Note Event")
	void GuitarRed();
	UFUNCTION(BlueprintNativeEvent, Category = "Guitar Note Event")
	void GuitarYellow();
	UFUNCTION(BlueprintNativeEvent, Category = "Guitar Note Event")
	void GuitarBlue();
	UFUNCTION(BlueprintNativeEvent, Category = "Guitar Note Event")
	void GuitarOrange();
	UFUNCTION(BlueprintNativeEvent, Category = "Dance Pad Event")
	void PadUp();
	UFUNCTION(BlueprintNativeEvent, Category = "Dance Pad Event")
	void PadDown();
	UFUNCTION(BlueprintNativeEvent, Category = "Dance Pad Event")
	void PadLeft();
	UFUNCTION(BlueprintNativeEvent, Category = "Dance Pad Event")
	void PadRight();
	UFUNCTION()
	void GuitarRedPressed();
	UFUNCTION()
	void GuitarGreenPressed();
	UFUNCTION()
	void GuitarYellowPressed();
	UFUNCTION()
	void GuitarBluePressed();
	UFUNCTION()
	void GuitarOrangePressed();
	UFUNCTION()
	void GuitarRedReleased();
	UFUNCTION()
	void GuitarGreenReleased();
	UFUNCTION()
	void GuitarYellowReleased();
	UFUNCTION()
	void GuitarBlueReleased();
	UFUNCTION()
	void GuitarOrangeReleased();
	UFUNCTION()
	void StrumUpPressed();
	UFUNCTION()
	void StrumDownPressed();
	UFUNCTION()
	void StrumUpReleased();
	UFUNCTION()
	void StrumDownReleased();
	UFUNCTION()
	void ProcessStrum();
	UFUNCTION()
	void BeginBattle();
	UFUNCTION()
	void BeginWalk();
	UFUNCTION()
	void TakeHit();
	UFUNCTION()
	void KillPlayer();
	UFUNCTION()
	void KillTarget();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<class UUserWidget> wBoard;

	UFUNCTION()
	AEnemy* GetTargetEnemy();

	UFUNCTION()
	void OnComponentBeginOverlap(AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION()
	void OnComponentEndOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

private:
	UFUNCTION()
	void PlayVoiceLineKill();
	UFUNCTION()
	void ActivateHyperDriver();
	UFUNCTION()
	void DeactivateHyperDriver();
	UFUNCTION()
	void PlayVoiceLine();
	UFUNCTION()
	void WalkOnPath(float delta);
	UFUNCTION()
	void ReadInputForKill(float delta);
	UFUNCTION()
	void LerpToTargetRotation(float delta);

#pragma region Components
	UPROPERTY()
	UParticleSystemComponent* m_ParticleSystem;
	UPROPERTY()
	UCameraComponent* m_CameraComponent;
	UPROPERTY()
	UCapsuleComponent* m_CapsuleComponent;
	UPROPERTY(EditAnywhere)
	AFretBoard* m_FretBoard;
	UPROPERTY(EditAnywhere)
	UChildActorComponent* m_InputChildComponent;
	UPROPERTY(EditAnywhere)
	AInputHandler* m_InputHandler;
	UPROPERTY()
	APathNode* m_CurrentNode;
	UPROPERTY()
	AEnemy* m_TargetEnemy;
	UPROPERTY(EditAnywhere)
	TArray<USoundWave*> m_VoiceLinesKill;
	UPROPERTY()
	int32 m_LastPlayedLine;
	UPROPERTY(EditAnywhere)
	UAudioComponent* m_VoiceLinesComponent;
	UPROPERTY(EditAnywhere)
	ASplinePath* m_SplinePath;
	UPROPERTY()
	TEnumAsByte<EPlayerState> m_PlayerState;
	UPROPERTY()
	FRotator m_TargetRotation;
#pragma endregion

	UPROPERTY()
	float m_MyHealth;
	UPROPERTY()
	bool m_GreenNote;
	UPROPERTY()
	bool m_RedNote;
	UPROPERTY()
	bool m_YellowNote;
	UPROPERTY()
	bool m_BlueNote;
	UPROPERTY()
	bool m_OrangeNote;
	UPROPERTY()
	bool m_PadLeftPressed;
	UPROPERTY()
	bool m_PadRightPressed;
	UPROPERTY()
	bool m_PadUpPressed;
	UPROPERTY()
	bool m_PadDownPressed;
	UPROPERTY()
	bool m_RedHold;
	UPROPERTY()
	bool m_GreenHold;
	UPROPERTY()
	bool m_YellowHold;
	UPROPERTY()
	bool m_BlueHold;
	UPROPERTY()
	bool m_OrangeHold;
	UPROPERTY()
	bool m_StrumUpHold;
	UPROPERTY()
	bool m_StrumDownHold;
	//UPROPERTY()
		//TArray<TEnumAsByte<EInputEnum>> m_CurrentDancePadCombination;

	UPROPERTY()
	float m_DistanceTraveled;



	UUserWidget* m_MyBoardWidget;
};

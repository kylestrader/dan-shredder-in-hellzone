// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "ProgressFillBar.generated.h"

UCLASS()
class ROCKINRAILS_API AProgressFillBar : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProgressFillBar();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

private:
	UPROPERTY()
	UStaticMeshComponent* m_Bar;
	UPROPERTY()
	UStaticMeshComponent* m_Cylinder;
	UPROPERTY()
	UPointLightComponent* m_PointLight;
	UPROPERTY()
	UParticleSystemComponent* m_Lightning;
};

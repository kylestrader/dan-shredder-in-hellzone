// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "GameDefines.h"
#include "PathNode.generated.h"

class ASplinePath;
class AEnemy;

UCLASS()
class ROCKINRAILS_API APathNode : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APathNode();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called after the object needs to be updated
	virtual void OnConstruction(const FTransform& Transform) override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Setters 
	void SetSplinePath(ASplinePath *SplinePath);

	void UpdatePositionAlongSpline();

	AEnemy* TargetEnemyWithKey(const TArray<E_RefKey>& Key);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Meta Data")
	float mDistance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Meta Data")
	TArray<AEnemy*> mRelativeEnemies;

private:
	UPROPERTY(EditAnywhere)
	ASplinePath *mSplinePath;

	UPROPERTY()
	USphereComponent* mSphereComponent;

	UPROPERTY()
	UArrowComponent* mArrowComponent;

	UPROPERTY()
	bool mActive;

	UPROPERTY()
	bool mUsed;
};

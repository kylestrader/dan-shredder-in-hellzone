// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "XMLParser.h"
#include "GameDefines.h"
#include "Utilities.h"
#include "MultiChannelTrack.h"

/**
*
*/

struct NoteEvent
{
	NoteEvent(int startTime, int note, int channel) :
	m_StartTime(startTime),
	m_Note(note),
	m_Channel(channel){};

	~NoteEvent(){}

	int m_StartTime;
	int m_Note;
	int m_Channel;
};

static class ROCKINRAILS_API MidiXmlParser
{
public:
	static bool LoadXmlSheet(FString sysUrl, uint16 bpm, std::shared_ptr<MultiChannelTrack>);

private:
	MidiXmlParser(){};
	~MidiXmlParser(){};

	static TArray<FXmlNode*> FindChildNodesWithTag(const FXmlNode* tmpNode, FString tag);
};

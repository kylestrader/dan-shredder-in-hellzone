// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "DanShredderGameMode.generated.h"

/**
 * 
 */
UCLASS()
class ROCKINRAILS_API ADanShredderGameMode : public AGameMode
{
	GENERATED_BODY()
	
public:
	UPROPERTY()
		int32 Difficulty;
	
	
};

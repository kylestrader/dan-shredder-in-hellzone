// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Reciever.generated.h"

UCLASS()
class ROCKINRAILS_API AReciever : public AActor
{
	GENERATED_BODY()
public:	
	// Sets default values for this actor's properties
	AReciever();

	virtual void OnConstruction(const FTransform& Transform) override;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UFUNCTION()
	void SetColor(FVector color);

	UFUNCTION()
	void SetGlow(float glow);

	UFUNCTION()
	void SetIsActive(bool active);

	UFUNCTION()
	void HandleNoteHit();

	UFUNCTION()
	void SetDepressed(bool depressed) ;
	UFUNCTION()
	bool GetDepressed();

	UFUNCTION()
	void SetHitNoteCount(int32 amount);
	UFUNCTION()
	int32 GetHitNoteCount();

	UPROPERTY(EditAnywhere)
	float FretDepressOffSet;
private:

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* mRecieverMesh;
	UPROPERTY()
	UBoxComponent* mBox;
	UPROPERTY()
	UParticleSystemComponent* mParticleSystem;
	UPROPERTY()
	UMaterialInstanceDynamic* mMID;


	UPROPERTY()
	float m_BazeZPosition;
	UPROPERTY()
	bool m_isDepressed; //my entire life
	UPROPERTY()
	bool m_isActive;
	UPROPERTY()
	bool m_CollideNote;

	UPROPERTY()
	AActor* m_ContainObj;
	UPROPERTY()
	int32 m_HitNoteCount;

	UPROPERTY()
	FVector mColor;
	UPROPERTY()
	float mGlow;



	UFUNCTION()
	void OnComponentBeginOverlap(AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION()
	void OnComponentEndOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
};

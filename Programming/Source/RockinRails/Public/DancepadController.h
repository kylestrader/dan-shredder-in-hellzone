// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "InputHandler.h"
#include "DancepadController.generated.h"

UCLASS()
class ROCKINRAILS_API ADancepadController : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ADancepadController();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	UPROPERTY(EditAnywhere)
	AInputHandler* m_InputHandler;

	void UpPressedEvent();
	void DownPressedEvent();
	void LeftPressedEvent();
	void RightPressedEvent();
	void UpReleasedEvent();
	void DownReleasedEvent();
	void LeftReleasedEvent();
	void RightReleasedEvent();
private:

	UPROPERTY()
	bool m_UpHold;

	UPROPERTY()
	bool m_DownHold;

	UPROPERTY()
	bool m_LeftHold;

	UPROPERTY()
	bool m_RightHold;


};

// Fill out your copyright notice in the Description page of Project Settings.

#include "RockinRails.h"
#include "RiffBank.h"

#include "GameDefines.h"
#include "MidiXMLParser.h"
#include "Utilities.h"

// Sets default values
ARiffBank::ARiffBank()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ARiffBank::BeginPlay()
{
	Super::BeginPlay();

	for (uint i = 0; i < uint(m_Riffs.Num()); i++)
	{
		if (m_Riffs[i].m_XMLMidiName != "")
		{
			m_Riffs[i].m_MultiChannelTrack.reset();
			m_Riffs[i].m_MultiChannelTrack = std::make_shared<MultiChannelTrack>();
			std::shared_ptr<MultiChannelTrack> track = m_Riffs[i].m_MultiChannelTrack;
			MidiXmlParser::LoadXmlSheet(XML_MIDI_DIRECTORY + m_Riffs[i].m_XMLMidiName, m_Riffs[i].m_BPM, track);
			print_num(m_Riffs[i].m_MultiChannelTrack->GetNumChannels());
			std::shared_ptr<MultiChannelTrack> trackattempt = m_Riffs[i].m_MultiChannelTrack;
		}
	}
}

void ARiffBank::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);
}

// Called every frame
void ARiffBank::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

FChannelReturn ARiffBank::GetTrackAsNoteArray(FString trackName, int32 channelIndex)
{
	FChannelReturn return_data;
	TArray<FNoteMetaData> notes;
	USoundWave* s_wave = nullptr;

	for (uint i = 0; i < uint(m_Riffs.Num()); i++)
	{
		if (m_Riffs[i].m_XMLMidiName == trackName)
		{
			std::shared_ptr<MultiChannelTrack> track = m_Riffs[i].m_MultiChannelTrack;
			s_wave = m_Riffs[i].m_SoundWave;
			std::shared_ptr<NoteChannel> channel = track->GetChannel(channelIndex);
			TArray<NoteStroke> t_notes = channel->GetNotes();

			for (uint i = 0; i < uint(t_notes.Num()); i++)
			{
				FNoteMetaData note;
				note.m_Channel = t_notes[i].GetChannel();
				note.m_Duration = t_notes[i].GetDuration();
				note.m_Note = t_notes[i].GetNote();
				note.m_StartTime = t_notes[i].GetStartTime();
				note.m_Velocity = t_notes[i].GetVelocity();
				notes.Push(note);
			}

			break;
		}
	}

	return_data.m_Notes = notes;
	return_data.m_SoundWave = s_wave;

	return return_data;
}

int32 ARiffBank::GetNumTracks()
{
	return m_Riffs.Num();
}

FChannelReturn ARiffBank::GetRandomTrackAsArray(int32 channelIndex)
{
	FChannelReturn return_data;

	int rand = FMath::Rand();
		
	rand %= (m_Riffs.Num());

	print_num(rand);

	for (uint i = 0; i < uint(m_Riffs.Num()); i++)
	{
		if (i == rand)
		{
			
			return_data = GetTrackAsArrayByIndex(rand, channelIndex);
			return return_data;
		}
	}

	return return_data;
}

FChannelReturn ARiffBank::GetRandomTrackAsArrayWithExceptions(TArray<int32> exceptions, int32 channelIndex)
{
	FChannelReturn return_data;

	uint32 rand = -1;
	
	while (rand < 0 || exceptions.Contains(rand))
	{
		rand = FMath::Rand() % (m_Riffs.Num());
	}

	for (uint i = 0; i < uint(m_Riffs.Num()); i++)
	{
		if (i == rand)
		{
			return_data = GetTrackAsArrayByIndex(rand, channelIndex);
			return return_data;
		}
	}

	return return_data;
}

FChannelReturn ARiffBank::GetTrackAsArrayByIndex(int32 index, int32 channelIndex)
{
	FChannelReturn return_data;
	TArray<FNoteMetaData> notes;

	if (m_Riffs.IsValidIndex(index))
	{
		std::shared_ptr<MultiChannelTrack> track = m_Riffs[index].m_MultiChannelTrack;
		USoundWave* s_wave = m_Riffs[index].m_SoundWave;
		if (track->IsValidChannel(channelIndex))
		{
			std::shared_ptr<NoteChannel> channel = track->GetChannel(channelIndex);
			TArray<NoteStroke> t_notes = channel->GetNotes();

			for (uint i = 0; i < uint(t_notes.Num()); i++)
			{
				FNoteMetaData note;
				note.m_Channel = t_notes[i].GetChannel();
				note.m_Duration = t_notes[i].GetDuration();
				note.m_Note = t_notes[i].GetNote();
				note.m_StartTime = t_notes[i].GetStartTime();
				note.m_Velocity = t_notes[i].GetVelocity();
				notes.Push(note);
			}
		}
		return_data.m_Notes = notes;
		return_data.m_SoundWave = s_wave;
	}

	return return_data;
}


// Fill out your copyright notice in the Description page of Project Settings.

#include "RockinRails.h"
#include "MultiChannelTrack.h"

MultiChannelTrack::~MultiChannelTrack()
{
	ClearTrack();
}

void MultiChannelTrack::PushNoteStroke(uint startTime, uint duration, uint note, uint channel, uint velocity)
{
	PushNoteStroke(NoteStroke(startTime, duration, note, channel, velocity));
}

void MultiChannelTrack::PushNoteStroke(NoteStroke note)
{
	uint desiredChannel = note.GetChannel();

	if (m_Channels.find(desiredChannel) == m_Channels.end())
	{
		// Channel was not found
		m_Channels[desiredChannel] = std::make_shared<NoteChannel>();
	}

	unsigned int8 = m_Channels.size();
	unsigned int32 = m_Channels[desiredChannel]->GetNotes().Num();

	m_Channels[desiredChannel]->PushNote(note);
}

void MultiChannelTrack::ClearTrack()
{
	m_Channels.clear();
}

void MultiChannelTrack::ClearChannel(uint channel)
{
	m_Channels[channel]->ClearChannel();
}

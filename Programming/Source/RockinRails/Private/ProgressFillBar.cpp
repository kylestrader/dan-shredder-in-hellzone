// Fill out your copyright notice in the Description page of Project Settings.

#include "RockinRails.h"
#include "ProgressFillBar.h"


// Sets default values
AProgressFillBar::AProgressFillBar()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Create the Root Component
	USphereComponent* sphere = CreateDefaultSubobject<USphereComponent>(TEXT("Root"));
	RootComponent = sphere;
	sphere->InitSphereRadius(1.0f);
	sphere->SetCollisionProfileName(TEXT("None"));

	//M_BAR
	//Create the Static Mesh Component for "m_Bar"
	m_Bar = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bar Mesh"));
	m_Bar->AttachTo(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> m_BarMesh(TEXT("StaticMesh'/Game/HUD/FretboardProgressBar/BarBspMesh_Length100.BarBspMesh_Length100'"));
	if (m_BarMesh.Succeeded())
	{
		m_Bar->SetStaticMesh(m_BarMesh.Object);
		m_Bar->SetRelativeLocation(FVector(0.0f, 0.0f, 00.0f));
		m_Bar->SetWorldScale3D(FVector(10.f, 2.f, 2.f));
	}

	// Set "m_bar's" material
	static ConstructorHelpers::FObjectFinder<UMaterialInstance> m_BarMaterial(TEXT("MaterialInstance'/Game/Materials/MatProgressBar_Inst.MatProgressBar_Inst'"));
	if (m_BarMaterial.Succeeded())
	{
		UMaterialInterface* mat = (UMaterialInstance*)m_BarMaterial.Object;
		m_Bar->SetMaterial(0, mat);
	}

	//M_CYLINDER
	//Create the Static Mesh Component for "m_Cylinder"
	m_Cylinder = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Cylinder"));
	m_Cylinder->AttachTo(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> m_CylinderMesh(TEXT("StaticMesh'/Engine/BasicShapes/Cylinder.Cylinder'"));
	if (m_CylinderMesh.Succeeded())
	{
		m_Cylinder->SetStaticMesh(m_CylinderMesh.Object);
		m_Cylinder->SetRelativeLocation(FVector(500.f, 0.0f, 00.0f));
		m_Cylinder->SetRelativeRotation(FRotator(0.f, 90.f, 90.f));
		m_Cylinder->SetWorldScale3D(FVector(0.25f, 0.25f, 10.f));
	}

	// Set "m_CapEnd's" material
	static ConstructorHelpers::FObjectFinder<UMaterialInstance> m_CylinderMaterial(TEXT("MaterialInstance'/Game/StarterContent/Materials/M_Glass_Inst.M_Glass_Inst'"));
	if (m_CylinderMaterial.Succeeded())
	{
		UMaterialInterface* mat = (UMaterialInstance*)m_CylinderMaterial.Object;
		m_Cylinder->SetMaterial(0, mat);
	}

	m_PointLight = CreateDefaultSubobject<UPointLightComponent>(TEXT("PointLight"));
	m_PointLight->AttachTo(RootComponent);
	m_PointLight->SetIntensity(100);
	m_PointLight->SetLightColor(FLinearColor(FColor(0, 255, 255)));
	m_PointLight->SetSourceLength(300);

	// Create Particle System
	m_Lightning = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Particle System"));
	m_Lightning->AttachTo(RootComponent);
	m_Lightning->bAutoActivate = true;
	m_Lightning->SetRelativeLocation(FVector::ZeroVector);

	static ConstructorHelpers::FObjectFinder<UParticleSystem> m_LightningParticleSystem(TEXT("ParticleSystem'/Game/Particles/P_ProgressBar.P_Progressbar'"));
	if (m_LightningParticleSystem.Succeeded())
	{
		m_Lightning->SetTemplate(m_LightningParticleSystem.Object);
	}
}

// Called when the game starts or when spawned
void AProgressFillBar::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AProgressFillBar::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}


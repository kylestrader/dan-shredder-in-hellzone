// Fill out your copyright notice in the Description page of Project Settings.

#include "RockinRails.h"
#include "SplinePath.h"
#include "Runtime/Engine/Classes/Components/SplineComponent.h"


// Sets default values
ASplinePath::ASplinePath()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	mSplineComponent = CreateDefaultSubobject<USplineComponent>(TEXT("SplineComponent"));
}

void ASplinePath::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	uint32 count = mPathNodeList.Num();

	for (uint32 i = 0; i < count; i++)
	{
		mPathNodeList[i]->SetSplinePath(this);
		mPathNodeList[i]->UpdatePositionAlongSpline();
	}
}

// Called when the game starts or when spawned
void ASplinePath::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ASplinePath::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

// Called to access a reference to the Spline Component of this path
USplineComponent* ASplinePath::GetSplineComponent()
{
	
	return mSplineComponent;
}

FVector ASplinePath::GetLocationAtDistanceAlongSpline(float Distance)
{
	return mSplineComponent->GetLocationAtDistanceAlongSpline(
		Distance,
		ESplineCoordinateSpace::World);
}


// Fill out your copyright notice in the Description page of Project Settings.

#include "RockinRails.h"
#include "FretboardNote.h"
#include "NoteConsumer.h"
#include "Utilities.h"


// Sets default values
ANoteConsumer::ANoteConsumer()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	m_ConsumerComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Consumer Hit Box"));
	m_ConsumerComponent->SetCollisionProfileName(TEXT("BlockAllDynamic"));
	m_ConsumerComponent->SetBoxExtent(FVector(5.f, 100.0f, 5.0f), true);
	m_ConsumerComponent->OnComponentBeginOverlap.AddDynamic(this, &ANoteConsumer::OnComponentBeginOverlap);

}

// Called when the game starts or when spawned
void ANoteConsumer::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ANoteConsumer::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void ANoteConsumer::OnComponentBeginOverlap(AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	AFretboardNote* note = Cast<AFretboardNote>(OtherActor);

	if (note != nullptr)
	{
		if (note->IsValidLowLevel() && !note->IsPendingKill())
		{
			print_text("Destroyed a note");
			OtherActor->Destroy();
		}
	}


}


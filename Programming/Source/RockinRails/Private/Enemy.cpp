// Fill out your copyright notice in the Description page of Project Settings.

#include "RockinRails.h"
#include "Runtime/Engine/Classes/Components/AudioComponent.h"
#include "Runtime/Engine/Classes/Components/ArrowComponent.h"
#include "Runtime/Engine/Classes/Components/SkeletalMeshComponent.h"
#include "Runtime/Engine/Classes/Components/ChildActorComponent.h"
#include "Runtime/Engine/Classes/Components/DestructibleComponent.h"
#include "Runtime/Engine/Classes/Components/BillboardComponent.h"
#include "Runtime/Engine/Classes/Components/SplineComponent.h"
#include "Enemy.h"
#include "Utilities.h"


// Sets default values
AEnemy::AEnemy()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SetupComponents();
}

void AEnemy::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	mDeathDelayTimer = DIE_DELAY_MAX;
	mAlive = true;
	mHP = 1;
}

void AEnemy::SetupComponents()
{
	UCapsuleComponent* cap = GetCapsuleComponent();
	cap->OnComponentBeginOverlap.AddDynamic(this, &AEnemy::OnComponentBeginOverlap);
	cap->OnComponentEndOverlap.AddDynamic(this, &AEnemy::OnComponentEndOverlap);

	mVoiceLinePlayer = CreateDefaultSubobject<UAudioComponent>(TEXT("VoiceLines"));
	mVoiceLinePlayer->AttachTo(RootComponent);

	mDeathSoundPlayer = CreateDefaultSubobject<UAudioComponent>(TEXT("DeathSound"));
	mDeathSoundPlayer->AttachTo(RootComponent);

	mSlashSoundPlayer = CreateDefaultSubobject<UAudioComponent>(TEXT("SlashSound"));
	mSlashSoundPlayer->AttachTo(RootComponent);

	mWindUpSoundPlayer = CreateDefaultSubobject<UAudioComponent>(TEXT("WindUpSound"));
	mWindUpSoundPlayer->AttachTo(RootComponent);

	mArrowComponent = CreateDefaultSubobject<UArrowComponent>(TEXT("ArrowComponent"));
	mArrowComponent->AttachTo(RootComponent);

	//mMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	//mMeshComponent->AttachTo(mRootComponent);

	GetMesh()->SetRelativeLocation(FVector::ZeroVector);

	mTargetComponent = CreateDefaultSubobject<UChildActorComponent>(TEXT("Target"));
	mTargetComponent->AttachTo(RootComponent);

	mSwitchOut = CreateDefaultSubobject<UDestructibleComponent>(TEXT("SwitchOut"));
	mSwitchOut->AttachTo(RootComponent);

	mArrowCenter = CreateDefaultSubobject<UBillboardComponent>(TEXT("Center_Arrow"));
	mArrowCenter->AttachTo(RootComponent);

	mArrowUp = CreateDefaultSubobject<UBillboardComponent>(TEXT("Arrow_Up"));
	mArrowUp->AttachTo(RootComponent);
	mArrowUp->SetVisibility(false);

	mArrowDown = CreateDefaultSubobject<UBillboardComponent>(TEXT("Arrow_Down"));
	mArrowDown->AttachTo(RootComponent);
	mArrowDown->SetVisibility(false);

	mArrowLeft = CreateDefaultSubobject<UBillboardComponent>(TEXT("Arrow_Left"));
	mArrowLeft->AttachTo(RootComponent);
	mArrowLeft->SetVisibility(false);

	mArrowRight = CreateDefaultSubobject<UBillboardComponent>(TEXT("Arrow_Right"));
	mArrowRight->AttachTo(RootComponent);
	mArrowRight->SetVisibility(false);

}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();
	PlayIdleAnim();
}

// Called every frame
void AEnemy::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	if (!mAlive)
	{
		mDeathDelayTimer -= DeltaTime;

		if (mDeathDelayTimer <= 0)
		{
			print_text("Dying!");
			Destroy();
		}
	}
}

void AEnemy::PlayAttackAnim()
{
	if (mAttackAnim->IsValidLowLevel()) GetMesh()->PlayAnimation(mAttackAnim, false);
}

void AEnemy::PlayIdleAnim()
{
	if (mIdleAnim->IsValidLowLevel()) GetMesh()->PlayAnimation(mIdleAnim, true);
}

void AEnemy::PlayStunAnim()
{
	if (mStunAnim->IsValidLowLevel()) GetMesh()->PlayAnimation(mStunAnim, false);
}

void AEnemy::DamagePlayer()
{
	//mPlayerRef->HurtMe();
}

void AEnemy::SetState(E_EnemyState state)
{
	mCurrentState = state;
}

void AEnemy::EnemyTakeDmg(int32 dmg)
{
	mHP -= dmg;
}

UChildActorComponent* AEnemy::GetChildActorComponent()
{
	return mTargetComponent;
}

E_EnemyState AEnemy::GetState()
{
	return mCurrentState;
}

bool AEnemy::IsAlive()
{
	return mAlive;
}

TArray<E_RefKey> AEnemy::GetRefKey()
{
	return mSelectionKeys;
}

void AEnemy::PlaySpawnVoiceLine()
{
	if (mVoiceLinesSpawn.Num() > 0)
	{
		while (mCurrentVoiceLine == mPrevVoiceLine)
		{
			int rNum = rand() % (mVoiceLinesSpawn.Num());

			mCurrentVoiceLine = rNum;
		}
		mVoiceLinePlayer->SetSound(mVoiceLinesSpawn[mCurrentVoiceLine]);
		mVoiceLinePlayer->Play();
	}
}

void AEnemy::PlayKillVoiceLine()
{
	if (mVoiceLinesKill.Num() > 0)
	{
		while (mCurrentVoiceLine == mPrevVoiceLine)
		{
			int rNum = (rand() % (mVoiceLinesKill.Num()));

			mCurrentVoiceLine = rNum;
		}
		mDeathSoundPlayer->SetSound(mVoiceLinesKill[mCurrentVoiceLine]);
		mDeathSoundPlayer->Play();
	}
}

void AEnemy::StartPath()
{

}

void AEnemy::Attack_Implementation()
{
	print_text("Attack Native C++ function call");
}

void AEnemy::Die()
{
	GetMesh()->SetVisibility(false);
	mSwitchOut->SetVisibility(true);

	mSwitchOut->SetActive(true, true);

	FVector hitLoc = RootComponent->GetComponentLocation();
	FVector impulseDir = RootComponent->GetForwardVector() * -1;

	mSwitchOut->ApplyDamage(DIE_DMG_AMT, hitLoc, impulseDir, DIE_IMP_STR);

	// TODO: PlayDieSound();

	mAlive = false;
	mCurrentState = E_EnemyState::E_DEAD;

	PlayKillVoiceLine();

	// TODO: Reset State of Fretboard
	// mPlayerRef->OnEnemyDefeated();

	print_text("Enemy Died!");
}

void AEnemy::OnPathFinished()
{
	mCurrentState = E_EnemyState::E_FIGHTING;
}

void AEnemy::OnComponentBeginOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	print_text("[ENEMY] Base class OverlapBegin");
}

void AEnemy::OnComponentEndOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	print_text("[ENEMY] Base class OverlapEnd");
}

// Fill out your copyright notice in the Description page of Project Settings.

#include "RockinRails.h"
#include "GreedNote.h"


// Sets default values
AGreedNote::AGreedNote()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AGreedNote::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGreedNote::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}


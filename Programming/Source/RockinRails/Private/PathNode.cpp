// Fill out your copyright notice in the Description page of Project Settings.

#include "RockinRails.h"
#include "PathNode.h"
#include "SplinePath.h"
#include "Enemy.h"


// Sets default values
APathNode::APathNode()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	mSphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere"));
	mSphereComponent->SetRelativeScale3D(FVector(3.0f, 3.0f, 3.0f));
	RootComponent = mSphereComponent;
	mSphereComponent->bGenerateOverlapEvents = true;
	mSphereComponent->SetCollisionProfileName(TEXT("BlockAllDynamic"));

	mArrowComponent = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow"));
	mArrowComponent->AttachTo(RootComponent);

}

// Called when the game starts or when spawned
void APathNode::BeginPlay()
{
	Super::BeginPlay();	
}

void APathNode::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	/*
	Make sure that after the public distance variable has been changed, 
	the node automatically repositions itself along the spline.
	*/
	UpdatePositionAlongSpline();
	mActive = false;
	mUsed = false;
}

// Called every frame
void APathNode::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

void APathNode::SetSplinePath(ASplinePath *SplinePath)
{
	mSplinePath = SplinePath;
}

void APathNode::UpdatePositionAlongSpline()
{
	if (!mSplinePath->IsValidLowLevel())
		return;

	FVector newLoc = mSplinePath->GetLocationAtDistanceAlongSpline(mDistance);
	this->SetActorLocation(newLoc);
}

AEnemy* APathNode::TargetEnemyWithKey(const TArray<E_RefKey>& Key)
{
	AEnemy* temp = nullptr;

	for (int32 i = 0; i < mRelativeEnemies.Num(); i++)
	{
		temp = mRelativeEnemies[i];

		if (!temp->IsValidLowLevel())
			continue;

		if (!temp->IsAlive())
			continue;

		E_EnemyState state = temp->GetState();

		if (state != E_EnemyState::E_STAGGERED
			&& state != E_EnemyState::E_FIGHTING)
			continue;

		TArray<E_RefKey> refKey = temp->GetRefKey();

		if (refKey.Num() <= 0)
			continue;

		bool valid = true;
		for (int32 i = 0; i < refKey.Num(); i++)
		{
			if (!Key.Contains(refKey[i]))
				break;
		}

		if (valid)
		{
			temp->OnTargeted();
			return temp;
		}
	}

	return nullptr;
}


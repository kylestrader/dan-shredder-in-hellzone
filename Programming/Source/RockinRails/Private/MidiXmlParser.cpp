// Fill out your copyright notice in the Description page of Project Settings.

#include "RockinRails.h"
#include "MidiXmlParser.h"

// This function WILL clear the track that you pass into it.
bool MidiXmlParser::LoadXmlSheet(FString sysUrl, uint16 bpm, const std::shared_ptr<MultiChannelTrack> track)
{

	std::string url(TCHAR_TO_UTF8(*sysUrl));
	track->ClearTrack();
	//Load our file.
	const FXmlFile file(sysUrl);

	if (!file.IsValid())
	{
		print_error("File did not load successfully..." + sysUrl);
		return false;
	}

	//Establish the root node of the XML MIDI sheet.
	const FXmlNode* rootNode = file.GetRootNode();

	//pulses per beat
 	FString ticksPerBeat = FindChildNodesWithTag(rootNode, TEXT("TicksPerBeat"))[0]->GetContent();
	int32 PPB = 0;
	if (ticksPerBeat != "")
		PPB = FCString::Atoi(*ticksPerBeat);
	else return false;

	float PPQ = PPB;// 4.f;

	//Find the first (should only be one) track node in the XML sheet.
	const TArray<FXmlNode*> trackNodes = FindChildNodesWithTag(rootNode, TEXT("Track"));

	//Set our track node.
	FXmlNode* trackNode = nullptr;
	if (trackNodes.Num() <= 0) return false;

	for (uint trackIndex = 0; trackIndex < uint(trackNodes.Num()); trackIndex++)
	{
		trackNode = trackNodes[trackIndex];
		//Find all event nodes.
		const TArray<FXmlNode*> eventNodes = FindChildNodesWithTag(trackNode, TEXT("Event"));

		TArray<NoteEvent> noteEvents;

		//Loop through each event
		for (uint i = 0; i < uint(eventNodes.Num()); i++)
		{
			FXmlNode* tmpNode = eventNodes[i];
			//There should only be 2 child nodes, the Delta time and the note information.
			const TArray<FXmlNode*> detNodes = tmpNode->GetChildrenNodes();
			uint delta = 0.0; //in ms
			uint channel = 0;
			uint note = 0;
			uint velocity = 0;

			bool foundNoteTag = false;

			for (uint i = 0; i < uint(detNodes.Num()); i++)
			{
				if (detNodes[i]->GetTag() == TEXT("Absolute"))
				{
					FString deltaStr = detNodes[i]->GetContent();
					delta = FCString::Atoi(*deltaStr);
				}
				else if (detNodes[i]->GetTag() == TEXT("NoteOn") ||
					detNodes[i]->GetTag() == TEXT("NoteOff"))
				{
					FString channelStr = detNodes[i]->GetAttribute(TEXT("Channel"));
					channel = FCString::Atoi(*channelStr);
					FString noteStr = detNodes[i]->GetAttribute(TEXT("Note"));
					note = FCString::Atoi(*noteStr);
					FString velStr = detNodes[i]->GetAttribute(TEXT("Velocity"));
					velocity = FCString::Atoi(*velStr);

					foundNoteTag = true;
				}
			}

			if (foundNoteTag)
			{
				//push event to song object
				NoteEvent ev = NoteEvent(delta, note, channel);

				bool foundDup = false;

				for (uint i = 0; i < uint(noteEvents.Num());)
				{
					if (noteEvents[i].m_Channel == ev.m_Channel &&
						noteEvents[i].m_Note == ev.m_Note)
					{
						foundDup = true;
						int num = noteEvents.Num();
						uint duration = ev.m_StartTime - noteEvents[i].m_StartTime;
						uint startTime = noteEvents[i].m_StartTime;
						float tick = (60000 / (bpm * PPQ));
						startTime *= tick; // in ms
						track->PushNoteStroke(startTime, duration, note, trackIndex, velocity);
						noteEvents.RemoveAt(i);
						break;
					}
					i++;
				}

				if (!foundDup)
				{
					noteEvents.Push(ev);
					
				}
			}
		}
	}

	return true;
}

TArray<FXmlNode*> MidiXmlParser::FindChildNodesWithTag(const FXmlNode* tmpNode, FString tag)
{
	TArray<FXmlNode*> retNodes = tmpNode->GetChildrenNodes();
	for (uint i = 0; i < uint(retNodes.Num());)
	{
		if (retNodes[i]->GetTag() != tag)
			retNodes.RemoveAt(i);
		else
			i++;
	}

	return retNodes;
}

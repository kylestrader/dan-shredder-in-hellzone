// Fill out your copyright notice in the Description page of Project Settings.

#include "RockinRails.h"
#include "Utilities.h"
#include "InputHandler.h"
#include "PlayerGuitarController.h"
#include "PathNode.h"
#include "Enemy.h"
#include "SplinePath.h"

#include "../Public/PlayerGuitarController.h"
#include "Runtime/Engine/Classes/Components/SplineComponent.h"


// Sets default values
APlayerGuitarController::APlayerGuitarController()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	m_InputChildComponent = CreateDefaultSubobject<UChildActorComponent>(TEXT("Input"));
	m_InputChildComponent->AttachTo(RootComponent);

	// Create Particle System
	m_ParticleSystem = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Particle System"));
	m_ParticleSystem->AttachTo(RootComponent);
	m_ParticleSystem->bAutoActivate = false;
	m_ParticleSystem->SetRelativeLocation(FVector(0.f, 0.f, 50.f));
	// Find asset and set the result to the particle system's template

	static ConstructorHelpers::FObjectFinder<UParticleSystem> ReceiverParticleSystem(TEXT("ParticleSystem'/Game/StarterContent/Particles/P_Explosion.P_Explosion'"));
	if (ReceiverParticleSystem.Succeeded())
	{
		m_ParticleSystem->SetTemplate(ReceiverParticleSystem.Object);
	}

	// Create camera component
	m_CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	m_CameraComponent->AttachTo(RootComponent);
	m_CameraComponent->SetRelativeLocation(FVector(0.f, 0.f, 100.f));

	// Create capsule component
	m_CapsuleComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule Component"));
	m_CapsuleComponent->AttachTo(RootComponent);
	m_CapsuleComponent->SetRelativeLocation(FVector(35.f, 0.f, 50.f));
	m_CapsuleComponent->SetRelativeScale3D(FVector(5.f, 5.f, 2.f));
	m_CapsuleComponent->bGenerateOverlapEvents = true;
	m_CapsuleComponent->SetCollisionProfileName(TEXT("OverlapAllDynamic"));

	// Create voice line component
	m_VoiceLinesComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("Voice Line Component"));
	m_VoiceLinesComponent->AttachTo(RootComponent);
	m_VoiceLinesComponent->VolumeMultiplier = 2;

	m_TargetEnemy = nullptr;
}

// Called when the game starts or when spawned
void APlayerGuitarController::BeginPlay()
{
	//Requires Board Widget in C++

	Super::BeginPlay();
	m_MyHealth = 3;
	m_DistanceTraveled = 0;

	if (wBoard)
	{
		m_MyBoardWidget = CreateWidget<UUserWidget>(this->GetWorld(), wBoard);
		if (m_MyBoardWidget)
		{
			m_MyBoardWidget->AddToViewport();
		}
	}
}

void APlayerGuitarController::PostInitializeComponents()
{
	Super::PostInitializeComponents();
}

void APlayerGuitarController::OnConstruction(const FTransform & Transform)
{
	m_InputChildComponent->SetChildActorClass(AInputHandler::StaticClass());
	m_InputChildComponent->CreateChildActor();

	m_InputHandler = Cast<AInputHandler>(m_InputChildComponent->ChildActor);
}

// Called every frame
void APlayerGuitarController::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	m_InputHandler->UpdateQueue(this);
	if (m_MyHealth < 0)
	{
		KillPlayer();
	}

	switch (m_PlayerState)
	{
	case EPlayerState::STATE_WALKING:
		WalkOnPath(DeltaTime);
		break;
	case EPlayerState::STATE_FIGHTING:
		ReadInputForKill(DeltaTime);
		break;
	}
	//auto widget = wBoard->GetDefaultObject();
}

// Called to bind functionality to input
void APlayerGuitarController::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

	//Fret Buttons Pressed
	InputComponent->BindAction("B", IE_Pressed, this, &APlayerGuitarController::GuitarRedPressed);
	InputComponent->BindAction("Y", IE_Pressed, this, &APlayerGuitarController::GuitarYellowPressed);
	InputComponent->BindAction("A", IE_Pressed, this, &APlayerGuitarController::GuitarGreenPressed);
	InputComponent->BindAction("X", IE_Pressed, this, &APlayerGuitarController::GuitarBluePressed);
	InputComponent->BindAction("L1", IE_Pressed, this, &APlayerGuitarController::GuitarOrangePressed);

	//Fret Buttons Released
	InputComponent->BindAction("B",  IE_Released, this, &APlayerGuitarController::GuitarRedReleased);
	InputComponent->BindAction("Y",  IE_Released, this, &APlayerGuitarController::GuitarYellowReleased);
	InputComponent->BindAction("A",  IE_Released, this, &APlayerGuitarController::GuitarGreenReleased);
	InputComponent->BindAction("X",  IE_Released, this, &APlayerGuitarController::GuitarBlueReleased);
	InputComponent->BindAction("L1", IE_Released, this, &APlayerGuitarController::GuitarOrangeReleased);

	//Strum Pressed
	InputComponent->BindAction("D-Pad Up", IE_Pressed, this, &APlayerGuitarController::StrumUpPressed);
	InputComponent->BindAction("D-Pad Down", IE_Pressed, this, &APlayerGuitarController::StrumDownPressed);

	//Strum Released
	InputComponent->BindAction("D-Pad Down", IE_Released, this, &APlayerGuitarController::StrumDownReleased);
	InputComponent->BindAction("D-Pad Up", IE_Released, this, &APlayerGuitarController::StrumUpReleased);


}

void APlayerGuitarController::GuitarGreen_Implementation()
{
	m_FretBoard->SetGreenReceiverActive(true);
}

void APlayerGuitarController::GuitarRed_Implementation()
{
	m_FretBoard->SetRedReceiverActive(true);
}

void APlayerGuitarController::GuitarYellow_Implementation()
{
	m_FretBoard->SetYellowReceiverActive(true);
}

void APlayerGuitarController::GuitarBlue_Implementation()
{
	m_FretBoard->SetBlueReceiverActive(true);
}

void APlayerGuitarController::GuitarOrange_Implementation()
{
	m_FretBoard->SetOrangeReceiverActive(true);
}

void APlayerGuitarController::PadUp_Implementation()
{
	m_PadUpPressed = true;
}
void APlayerGuitarController::PadRight_Implementation()
{
	m_PadRightPressed = true;
}
void APlayerGuitarController::PadDown_Implementation()
{
	m_PadDownPressed = true;
}
void APlayerGuitarController::PadLeft_Implementation()
{
	m_PadLeftPressed = true;
}

void APlayerGuitarController::GuitarRedPressed()
{
	m_RedHold = true;
	m_FretBoard->InterpretFretStates(EInputEnum::GUITAR_RED);
}

void APlayerGuitarController::GuitarGreenPressed()
{
	m_GreenHold = true;
	m_FretBoard->InterpretFretStates(EInputEnum::GUITAR_GREEN);
}

void APlayerGuitarController::GuitarYellowPressed()
{
	m_YellowHold = true;
	m_FretBoard->InterpretFretStates(EInputEnum::GUITAR_YELLOW);
}

void APlayerGuitarController::GuitarBluePressed()
{
	m_BlueHold = true;
	m_FretBoard->InterpretFretStates(EInputEnum::GUITAR_BLUE);
}

void APlayerGuitarController::GuitarOrangePressed()
{
	m_OrangeHold = true;
	m_FretBoard->InterpretFretStates(EInputEnum::GUITAR_ORANGE);
}

void APlayerGuitarController::GuitarRedReleased()
{
	m_RedHold = false;
	m_FretBoard->InterpretFretStates(EInputEnum::GUITAR_RED);
}

void APlayerGuitarController::GuitarGreenReleased()
{
	m_GreenHold = false;
	m_FretBoard->InterpretFretStates(EInputEnum::GUITAR_GREEN);
}

void APlayerGuitarController::GuitarYellowReleased()
{
	m_YellowHold = false;
	m_FretBoard->InterpretFretStates(EInputEnum::GUITAR_YELLOW);
}

void APlayerGuitarController::GuitarBlueReleased()
{
	m_BlueHold = false;
	m_FretBoard->InterpretFretStates(EInputEnum::GUITAR_BLUE);
}

void APlayerGuitarController::GuitarOrangeReleased()
{
	m_OrangeNote = false;
	m_FretBoard->InterpretFretStates(EInputEnum::GUITAR_ORANGE);
}

void APlayerGuitarController::StrumUpPressed()
{
	if (m_StrumUpHold)
		return;
	m_StrumUpHold = true;
	ProcessStrum();
}

void APlayerGuitarController::StrumDownPressed()
{
	if (m_StrumDownHold)
		return;
	m_StrumDownHold = true;
	ProcessStrum();
}

void APlayerGuitarController::StrumUpReleased()
{
	if (!m_StrumUpHold)
		return;
	m_StrumUpHold = false;
}

void APlayerGuitarController::StrumDownReleased()
{
	if (!m_StrumDownHold)
		return;
	m_StrumDownHold = false;
}

void APlayerGuitarController::ProcessStrum()
{
	if (m_StrumUpHold || m_StrumDownHold)
	{
		if (m_OrangeHold)
		{
			m_InputHandler->HandleInputEvent(GUITAR_ORANGE);
		}
		if (m_BlueHold)
		{
			m_InputHandler->HandleInputEvent(GUITAR_BLUE);
		}
		if (m_YellowHold)
		{
			m_InputHandler->HandleInputEvent(GUITAR_YELLOW);
		}
		if (m_RedHold)
		{
			m_InputHandler->HandleInputEvent(GUITAR_RED);
		}
		if (m_GreenHold)
		{
			m_InputHandler->HandleInputEvent(GUITAR_GREEN);
		}
	}

}

void APlayerGuitarController::BeginBattle()
{
	m_PlayerState = STATE_FIGHTING;
}

void APlayerGuitarController::BeginWalk()
{
	m_PlayerState = STATE_WALKING;

	if (m_CurrentNode != nullptr)
	{
		auto rot = m_CurrentNode->GetActorRotation();
		this->SetActorRotation(rot);

		m_FretBoard->ClearSong();
	}
}

void APlayerGuitarController::TakeHit()
{
	m_MyHealth--;
	m_ParticleSystem->Activate(true);
	//requires BoardWidget to be C++
}

void APlayerGuitarController::KillPlayer()
{
	GetCharacterMovement()->MaxWalkSpeed = 0.f;
	//TODO: InitGame Over Screen
}

void APlayerGuitarController::KillTarget()
{
	//ScoreKeeper Things first

	
	m_TargetEnemy->EnemyTakeDmg(1);

	if (FMath::RandRange(0, 3) == 1)
	{
		PlayVoiceLineKill();
	}
}

AEnemy* APlayerGuitarController::GetTargetEnemy()
{
	return m_TargetEnemy;
}

void APlayerGuitarController::PlayVoiceLineKill()
{

	//TODO implement
	FTimerHandle timerHandle;

	GetWorldTimerManager().SetTimer(timerHandle, this, &APlayerGuitarController::PlayVoiceLine, 1000, false, 0.0f);
	
}

void APlayerGuitarController::ActivateHyperDriver()
{
	if (m_TargetEnemy->IsValidLowLevel())
	{
		m_TargetEnemy->SetState(E_EnemyState::E_STAGGERED);
		m_FretBoard->SetIsHyperDriveActive(true);
	}

}

void APlayerGuitarController::DeactivateHyperDriver()
{
	if (m_TargetEnemy->IsValidLowLevel())
	{
		//Logic for when Jumper is implemented in C++ code. "Jumper" is temporary until it can be replaced with the actual reference
		/*if (m_TargetEnemy->GetClass() == Jumper)
		{
			if (Jumper->IsStunned())
			{
				m_FretBoard->SetIsHyperDriveActive(false);
			}
			else
			{
				m_TargetEnemy->SetState(E_EnemyState::E_FIGHTING);
				m_FretBoard->SetIsHyperDriveActive(false);
			}
		}
		else
		{
			m_TargetEnemy->SetState(E_EnemyState::E_FIGHTING);
			m_FretBoard->SetIsHyperDriveActive(false);
		}*/

		m_TargetEnemy->SetState(E_EnemyState::E_FIGHTING);
		m_FretBoard->SetIsHyperDriveActive(false);
	}
}

void APlayerGuitarController::PlayVoiceLine()
{
	int32 currentLine = 0;

	currentLine = FMath::RandRange(0, m_VoiceLinesKill.Num() - 1);

	if (currentLine != m_LastPlayedLine)
	{
		if (m_VoiceLinesComponent != nullptr)
		{
			m_VoiceLinesComponent->SetSound((m_VoiceLinesKill[currentLine]));
			m_VoiceLinesComponent->Play();
			m_LastPlayedLine = currentLine;
		}
	}
}

void APlayerGuitarController::WalkOnPath(float delta)
{
	m_DistanceTraveled += (float)GetCharacterMovement()->MaxWalkSpeed * delta;
	USplineComponent* spline = m_SplinePath->GetSplineComponent();

	if (spline)
	{
		SetActorLocation(spline->GetLocationAtDistanceAlongSpline(m_DistanceTraveled, ESplineCoordinateSpace::World));
		m_TargetRotation = spline->GetRotationAtDistanceAlongSpline(m_DistanceTraveled, ESplineCoordinateSpace::World);
		SetActorRotation(m_TargetRotation);
	}

}

void APlayerGuitarController::ReadInputForKill(float delta)
{
	if (m_PadUpPressed)
	{

	}
		
}

void APlayerGuitarController::LerpToTargetRotation(float delta)
{
	SetActorRotation(FMath::RInterpTo(GetActorRotation(), m_TargetRotation, delta, 7));
}

void APlayerGuitarController::OnComponentBeginOverlap(AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (OtherActor->GetClass() == APathNode::StaticClass())
	{
		m_PlayerState = EPlayerState::STATE_FIGHTING;
	}
}

void APlayerGuitarController::OnComponentEndOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}

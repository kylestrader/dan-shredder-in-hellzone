// Fill out your copyright notice in the Description page of Project Settings.

#include "RockinRails.h"
#include "FretboardNote.h"
#include "Runtime/Engine/Classes/Components/SphereComponent.h"
#include "GameDefines.h"
#define NOTE_SPEED 100.0f

// Sets default values
AFretboardNote::AFretboardNote()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create the Root Component
	USphereComponent* sphere = CreateDefaultSubobject<USphereComponent>(TEXT("Root"));
	RootComponent = sphere;
	sphere->InitSphereRadius(1.0f);
	sphere->SetCollisionProfileName(TEXT("None"));

	//Create the component that will represent the collision boundaries
	mSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere"));
	mSphere->AttachTo(RootComponent);
	mSphere->InitSphereRadius(23.0f);
	mSphere->SetCollisionProfileName(TEXT("BlockAllDynamic"));

	//Create the Static Mesh Component
	mNoteMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Note"));
	mNoteMesh->AttachTo(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> NoteMesh(TEXT("StaticMesh'/Game/HUD/Fret_Bar_Assets/Note.Note'"));
	if (NoteMesh.Succeeded())
	{
		mNoteMesh->SetStaticMesh(NoteMesh.Object);
		mNoteMesh->SetRelativeLocation(FVector::ZeroVector);
		mNoteMesh->SetWorldScale3D(FVector(1.f));
	}

	// Set the ReceiverMesh's material
	static ConstructorHelpers::FObjectFinder<UMaterialInstance> NoteMaterial(TEXT("MaterialInstanceConstant'/Game/HUD/Fret_Bar_Textures/Note_Instance_Yellow.Note_Instance_Yellow'"));
	if (NoteMaterial.Succeeded())
	{
		UMaterialInterface* mat = (UMaterialInstance*)NoteMaterial.Object;
		mNoteMesh->SetMaterial(0, mat);
	}
}

void AFretboardNote::OnConstruction(const FTransform& Transform)
{
	mState = E_NoteState::E_MOVING;
	mNoteSpeed = NOTE_SPEED;
}

// Called when the game starts or when spawned
void AFretboardNote::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AFretboardNote::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	
	switch (mState)
	{
	case E_NoteState::E_IDLE:
		break;
	case E_NoteState::E_MOVING:
		Move(DeltaTime);
		break;
	}
}

void AFretboardNote::SetColor(FVector color)
{
	mColor = color;
	mMID = mNoteMesh->CreateDynamicMaterialInstance(0);

	if (mMID != nullptr)
	{
		mMID->SetVectorParameterValue("Note_Base_Color", FLinearColor(mColor));
		mNoteMesh->SetMaterial(0, mMID);

	}

}

void AFretboardNote::SetState(E_NoteState state)
{
	mState = state;
}

void AFretboardNote::Move(float delta)
{
	FVector newPos = FVector::ZeroVector;
	float distance = (mNoteSpeed * -1) * delta;
	newPos = RootComponent->GetRelativeTransform().GetLocation() + FVector(distance, 0, 0);
	RootComponent->SetRelativeLocation(newPos);
}

void AFretboardNote::SetNoteSpeed(float speed)
{
	mNoteSpeed = speed;
}


// Fill out your copyright notice in the Description page of Project Settings.

#include "RockinRails.h"
#include "GrudgeBar.h"
#include "Utilities.h"

// Sets default values
AGrudgeBar::AGrudgeBar()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	mDefDec = -2.0f;
	mMsSinceDec = MS_PER_DEC;
	mShouldDec = true;
}

// Called when the game starts or when spawned
void AGrudgeBar::BeginPlay()
{
	Super::BeginPlay();
	mCurrentValue = MAX_GRUDGE / 2;
	
}

// Called every frame
void AGrudgeBar::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	if (mShouldDec)
	{
		mMsSinceDec -= DeltaTime;

		if (mMsSinceDec <= 0)
		{
			ModValue(mDefDec);
			mMsSinceDec = MS_PER_DEC;
		}
	}
	//print_num(GetValue());
	//if (mCurrentValue >= MAX_GRUDGE) print_text("Matt sucks");
}

float AGrudgeBar::GetValue()
{
	return mCurrentValue;
}

void AGrudgeBar::ModValue(float mod)
{
	mCurrentValue += mod;

	if (mCurrentValue >= MAX_GRUDGE) mCurrentValue = MAX_GRUDGE;
	else if (mCurrentValue <= MIN_GRUDGE) mCurrentValue = MIN_GRUDGE;
}


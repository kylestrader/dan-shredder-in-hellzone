// Fill out your copyright notice in the Description page of Project Settings.

#include "RockinRails.h"
#include "InputHandler.h"


// Sets default values
AInputHandler::AInputHandler()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AInputHandler::BeginPlay()
{
	Super::BeginPlay();
	//m_InputQueue.Add(EInputEnum::GUITAR_GREEN);
	//m_InputAnalogQueue.Add(5);
}

// Called every frame
void AInputHandler::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

void AInputHandler::HandleInputEvent(EInputEnum input)
{
	m_InputQueue.Add(input);
}

void AInputHandler::HandleInputEventAnalog(EInputEnum input, float analogInput)
{
	FInputAnalogStruct inputStruct = FInputAnalogStruct();

	inputStruct.Input = input;
	inputStruct.AnalogInput = analogInput;

	m_InputAnalogQueue.Add(inputStruct);
}

void AInputHandler::ArbitrateInput(APlayerGuitarController* player, EInputEnum input)
{
	//player.GetActorClass();
	
	switch (input)
	{
	case EInputEnum::GUITAR_GREEN:
		player->GuitarGreen_Implementation();
		break;
	case EInputEnum::GUITAR_RED:
		player->GuitarRed_Implementation();
		break;
	case EInputEnum::GUITAR_YELLOW:
		player->GuitarYellow_Implementation();
		break;
	case EInputEnum::GUITAR_BLUE:
		player->GuitarBlue_Implementation();
		break;
	case EInputEnum::GUITAR_ORANGE:
		player->GuitarOrange_Implementation();
		break;	
	default:
		break;
	}

}

void AInputHandler::ArbitrateAnalogInput(APlayerGuitarController * player, FInputAnalogStruct input)
{
}

void AInputHandler::UpdateQueue(APlayerGuitarController* player)
{
	if (true)
	{
		for (int i = 0; i < m_InputQueue.Num(); i++)
		{
			ArbitrateInput(player, m_InputQueue[i]);

		}
	}

	m_InputQueue.RemoveAll([]( TEnumAsByte<EInputEnum> Ptr) {return true; });

	for (int i = 0; i < m_InputAnalogQueue.Num(); i++)
	{
		ArbitrateAnalogInput(player, m_InputAnalogQueue[i]);

	}
	m_InputAnalogQueue.RemoveAll([](FInputAnalogStruct ptr) {return true; });

}



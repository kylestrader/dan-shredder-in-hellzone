// Fill out your copyright notice in the Description page of Project Settings.

#include "RockinRails.h"
#include "MeleeEnemy.h"
#include "Utilities.h"



// Sets default values
AMeleeEnemy::AMeleeEnemy()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	mCurrentState = E_EnemyState::E_PATHING;
	mAttackAnimTimer = ATTACK_ANIM_MAX;
	mAttacking = false;

	//PlayIdleAnim();
}

// Called when the game starts or when spawned
void AMeleeEnemy::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMeleeEnemy::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	if (mAttacking)
	{
		mAttackAnimTimer -= DeltaTime;

		if (mAttackAnimTimer <= 0)
		{
			mAttacking = false;
			mAttackAnimTimer = ATTACK_ANIM_MAX;
			Attack();
			Die();
		}
	}

	switch (mCurrentState)
	{
	case E_EnemyState::E_FIGHTING:
		//WALK TOWARDS ENEMY
		if (mPlayerRef->IsValidLowLevel())
		{
			_playerLoc = mPlayerRef->GetActorLocation();
			_enemyLoc = GetActorLocation();
			_p2EDir = _playerLoc - _enemyLoc;
			_p2EDir.Normalize();

			_lookRot = FRotationMatrix::MakeFromX(_p2EDir).Rotator();
			_facePlayerRot = FRotator::ZeroRotator;
			_facePlayerRot.Yaw = _lookRot.Yaw;

			SetActorRotation(_facePlayerRot);
			//mPawnMovement->AddInputVector(_p2EDir * MOVE_INPUT_SCALE, true);
			AddMovementInput(_p2EDir, MOVE_INPUT_SCALE, true);
		}
		else print_text("No player reference found!");
		break;
	case E_EnemyState::E_IDLE:
		//ENEMY IDLE
		break;
	case E_EnemyState::E_PATHING:
		//FOLLOW SET PATH
		mCurrentState = E_EnemyState::E_FIGHTING;

		// Continue following path, if we've reached the end, call OnPathFinished();
		break;
	}
}

void AMeleeEnemy::OnPathFinished()
{
	// play voice line for spawning
	PlaySpawnVoiceLine();

	// set visibility for targeting arrows
	SetTargeterVisibility();

	mCurrentState = E_EnemyState::E_FIGHTING;
}

void AMeleeEnemy::SetTargeterVisibility()
{
	mCurrentState = E_EnemyState::E_FIGHTING;

	for (int i = 0; i < mSelectionKeys.Num(); i++)
	{
		switch (mSelectionKeys[i])
		{
		case(E_RefKey::E_LEFT) :
			// toggle component visibility
			mArrowLeft->SetVisibility(true);
			break;
		case(E_RefKey::E_RIGHT) :
			// toggle component visibility
			mArrowRight->SetVisibility(true);
			break;
		case(E_RefKey::E_UP) :
			// toggle component visibility
			mArrowUp->SetVisibility(true);
			break;
		case(E_RefKey::E_DOWN) :
			// toggle component visibility
			mArrowDown->SetVisibility(true);
			break;
		}
	}
}

void AMeleeEnemy::OverlapEvent(AActor* other)
{
	if (other->GetClass() == mPlayerRef->GetClass())
	{
		switch (mCurrentState)
		{
		case(E_EnemyState::E_FIGHTING) :
			// attack the player
			PlayAttackAnim(); // play attack animation
			mAttacking = true; // start attack timer
			break;
		case(E_EnemyState::E_IDLE) :
			// Do nothing for now
			break;
		case(E_EnemyState::E_PATHING) :
			// check current location along path, if on last node call OnPathFinished()
			break;
		}
	}
}

//void AMeleeEnemy::OnBeginOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
//{
//	print_text("[ENEMY] Base class OverlapBegin");
//}

//void AMeleeEnemy::OnEndOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
//{
//	print_text("[ENEMY] Base class OverlapEnd");
//}

/* Virtual functions from parent
void AMeleeEnemy::StartPath()
{

}

void AMeleeEnemy::Attack()
{

}

void AMeleeEnemy::PlaySpawnVoiceLine()
{

}

void AMeleeEnemy::PlayKillVoiceLine()
{

}

void AMeleeEnemy::OnAttack()
{

}

void AMeleeEnemy::OnTargeted()
{

}
*/

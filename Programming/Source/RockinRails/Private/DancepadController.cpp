// Fill out your copyright notice in the Description page of Project Settings.

#include "RockinRails.h"
#include "DancepadController.h"


// Sets default values
ADancepadController::ADancepadController()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
}

// Called when the game starts or when spawned
void ADancepadController::BeginPlay()
{
	Super::BeginPlay();
	UE_LOG(LogTemp, Error, TEXT("Dance Pad Constructed"));

}

// Called every frame
void ADancepadController::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void ADancepadController::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
	InputComponent->BindAction("D-Pad Up", IE_Pressed, this, &ADancepadController::UpPressedEvent);
	InputComponent->BindAction("D-Pad Up", IE_Released, this, &ADancepadController::UpReleasedEvent);
	InputComponent->BindAction("D-Pad Down", IE_Pressed, this, &ADancepadController::DownPressedEvent);
	InputComponent->BindAction("D-Pad Down", IE_Released, this, &ADancepadController::DownReleasedEvent);
	InputComponent->BindAction("D-Pad Left", IE_Pressed, this, &ADancepadController::LeftPressedEvent);
	InputComponent->BindAction("D-Pad Left", IE_Released, this, &ADancepadController::LeftReleasedEvent);
	InputComponent->BindAction("D-Pad Right", IE_Pressed, this, &ADancepadController::RightPressedEvent);
	InputComponent->BindAction("D-Pad Right", IE_Released, this, &ADancepadController::RightReleasedEvent);

}

void ADancepadController::UpPressedEvent()
{
	m_UpHold = true;
	m_InputHandler->HandleInputEvent(EInputEnum::PAD_UP);
	UE_LOG(LogTemp, Warning, TEXT("Up Pressed"));

}

void ADancepadController::DownPressedEvent()
{
	m_DownHold = true;
	m_InputHandler->HandleInputEvent(EInputEnum::PAD_DOWN);

}

void ADancepadController::LeftPressedEvent()
{
	m_LeftHold = true;
	m_InputHandler->HandleInputEvent(EInputEnum::PAD_LEFT);

}

void ADancepadController::RightPressedEvent()
{
	m_RightHold = true;
	m_InputHandler->HandleInputEvent(EInputEnum::PAD_RIGHT);

}

void ADancepadController::UpReleasedEvent()
{
	m_UpHold = false;
	m_InputHandler->HandleInputEvent(EInputEnum::PAD_UP_RELEASE);

}

void ADancepadController::DownReleasedEvent()
{
	m_DownHold = false;
	m_InputHandler->HandleInputEvent(EInputEnum::PAD_DOWN_RELEASE);

}

void ADancepadController::LeftReleasedEvent()
{
	m_LeftHold = false;
	m_InputHandler->HandleInputEvent(EInputEnum::PAD_LEFT_RELEASE);

}

void ADancepadController::RightReleasedEvent()
{
	m_RightHold = false;
	m_InputHandler->HandleInputEvent(EInputEnum::PAD_RIGHT_RELEASE);

}


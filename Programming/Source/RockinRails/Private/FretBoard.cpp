// Fill out your copyright notice in the Description page of Project Settings.

#include "RockinRails.h"
#include "FretBoard.h"
#include "FretboardNote.h"
#include "InputHandler.h"
#include "DanShredderGameMode.h"
#include "ScoreKeeper.h"
#include "Reciever.h"
#include "PlayerGuitarController.h"
#include "Enemy.h"

#define GREEN_COLOR FVector(0.0f,1.0f,0.0f)
#define RED_COLOR FVector(1.0f,0.0f,0.0f)
#define YELLOW_COLOR FVector(1.0f,1.0f,0.0f)
#define BLUE_COLOR FVector(0.0f,0.0f,1.0f)
#define ORANGE_COLOR FVector(1.0f,0.05f,0.0f)

// Sets default values
AFretBoard::AFretBoard()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	mFretBoard = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Fretboard"));
	RootComponent = mFretBoard;

	mFretBoardMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FretboardMesh"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> FretboardMesh(TEXT("StaticMesh'/Game/HUD/Fret_Bar.Fret_Bar'"));
	if (FretboardMesh.Succeeded())
	{
		mFretBoardMesh->SetStaticMesh(FretboardMesh.Object);
	}
	mFretBoardMesh->SetRelativeLocation(FVector(-80.0f, 0.0f, -10.0f));
	mFretBoardMesh->SetWorldScale3D(FVector(1));
	mFretBoardMesh->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f));
	
	// Create the Receivers
	mGreenReciever = CreateDefaultSubobject<UChildActorComponent>(TEXT("Green Reciever"));
	mGreenReciever->SetRelativeScale3D(FVector(1, 1, 1));
	mGreenReciever->SetWorldTransform(FTransform(FVector(-473.0f, -80.0f, 5.0f)));

	mRedReciever = CreateDefaultSubobject<UChildActorComponent>(TEXT("Red Reciever"));
	mRedReciever->SetRelativeScale3D(FVector(1, 1, 1));
	mRedReciever->SetWorldTransform(FTransform(FVector(-473, -40.0f, 5.0f)));

	mYellowReciever = CreateDefaultSubobject<UChildActorComponent>(TEXT("Yellow Reciever"));
	mYellowReciever->SetRelativeScale3D(FVector(1, 1, 1));
	mYellowReciever->SetWorldTransform(FTransform(FVector(-473, 0.0f, 5.0f)));

	mBlueReciever = CreateDefaultSubobject<UChildActorComponent>(TEXT("Blue Reciever"));
	mBlueReciever->SetRelativeScale3D(FVector(1, 1, 1));
	mBlueReciever->SetWorldTransform(FTransform(FVector(-473, 40.0f, 5.0f)));

	mOrangeReciever = CreateDefaultSubobject<UChildActorComponent>(TEXT("Orange Reciever"));
	mOrangeReciever->SetRelativeScale3D(FVector(1, 1, 1));
	mOrangeReciever->SetWorldTransform(FTransform(FVector(-473, 80.0f, 5.0f)));

	// Attach Receivers to the Fretboard
	mFretBoardMesh->AttachTo(RootComponent);
	mGreenReciever->AttachTo(RootComponent);
	mRedReciever->AttachTo(RootComponent);
	mYellowReciever->AttachTo(RootComponent);
	mBlueReciever->AttachTo(RootComponent);
	mOrangeReciever->AttachTo(RootComponent);

	// Create the point lights
	mPointLight1 = CreateDefaultSubobject<UPointLightComponent>(TEXT("Pointlight 1"));
	mPointLight1->AttachTo(RootComponent);
	mPointLight1->SetIntensity(25);
	mPointLight1->SetLightColor(FLinearColor(1.0f, 0.627f, 0.558f));
	mPointLight1->SetAttenuationRadius(75.f);
	mPointLight1->SetRelativeLocation(FVector(-369.8f, 0.0f, 300.0f));

	mPointLight2 = CreateDefaultSubobject<UPointLightComponent>(TEXT("Pointlight 2"));
	mPointLight2->AttachTo(RootComponent);
	mPointLight2->SetIntensity(25);
	mPointLight2->SetLightColor(FLinearColor(1.0f, 0.627f, 0.558f));
	mPointLight2->SetAttenuationRadius(75.f);
	mPointLight2->SetRelativeLocation(FVector(-196, 0.0f, 300.0f));

	mPointLight3 = CreateDefaultSubobject<UPointLightComponent>(TEXT("Pointlight 3"));
	mPointLight3->AttachTo(RootComponent);
	mPointLight3->SetIntensity(25);
	mPointLight3->SetLightColor(FLinearColor(1.0f, 0.627f, 0.558f));
	mPointLight3->SetAttenuationRadius(75.f);
	mPointLight3->SetRelativeLocation(FVector(-42, 0.0f, 300.0f));


	// Create Riff Audio Component
	RiffAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("Audio Riff Component"));
	RiffAudioComponent->AttachTo(RootComponent);

	m_NoteConsumer = CreateDefaultSubobject<UChildActorComponent>(TEXT("Note Consumer"));
	m_NoteConsumer->AttachTo(RootComponent);

	m_Delay = 1000;
	m_NoteSpeed= 650.f;
	m_SyncVal = 0.0f;

	// Create Lightning Attack Components
	mLightningAttackLeft = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Lighting Attack Left"));
	mLightningAttackLeft->AttachTo(RootComponent);

	mLightningAttackRight = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Lighting Attack Right"));
	mLightningAttackRight->AttachTo(RootComponent);

	mSparksEnemy = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Sparks Enemy"));
	mSparksEnemy->AttachTo(RootComponent);

	mLaserSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Laser Sound"));
	mLaserSound->AttachTo(RootComponent);
}

void AFretBoard::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	//AReciever(mGreenReciever->GetChildComponent(0)->SetColor(FVector::ZeroVector);
}

void AFretBoard::OnConstruction(const FTransform& Transform)
{
	//Wait until PostInitializeComponents so that the Engine can guarantee that these classes are recognized and registered
	mGreenReciever->SetChildActorClass(AReciever::StaticClass());
	mRedReciever->SetChildActorClass(AReciever::StaticClass());
	mYellowReciever->SetChildActorClass(AReciever::StaticClass());
	mBlueReciever->SetChildActorClass(AReciever::StaticClass());
	mOrangeReciever->SetChildActorClass(AReciever::StaticClass());

	m_NoteConsumer->SetChildActorClass(ANoteConsumer::StaticClass());

	//Have Unreal instantiate instances of the Receiver class
	mGreenReciever->CreateChildActor();
	mRedReciever->CreateChildActor();
	mYellowReciever->CreateChildActor();
	mBlueReciever->CreateChildActor();
	mOrangeReciever->CreateChildActor();

	m_NoteConsumer->CreateChildActor();
	m_NoteConsumer->ChildActor->SetActorScale3D(FVector(0.1f, 2.0f, 0.1f));
	AReciever* recG = Cast<AReciever>(mGreenReciever->ChildActor);
	recG->SetColor(GREEN_COLOR);
	AReciever* recR = Cast<AReciever>(mRedReciever->ChildActor);
	recR->SetColor(RED_COLOR);
	AReciever* recY = Cast<AReciever>(mYellowReciever->ChildActor);
	recY->SetColor(YELLOW_COLOR);
	AReciever* recB = Cast<AReciever>(mBlueReciever->ChildActor);
	recB->SetColor(BLUE_COLOR);
	AReciever* recO = Cast<AReciever>(mOrangeReciever->ChildActor);
	recO->SetColor(ORANGE_COLOR);

	m_NoteConsumer->SetRelativeLocation(FVector(-520.0f, 0, 0));
	m_NoteConsumer->SetRelativeTransform(FTransform(FVector(-900.f, 0.f, 0.f)));
	
}

// Called when the game starts or when spawned
void AFretBoard::BeginPlay()
{
	Super::BeginPlay();
	PlaySong();
	//GetWorld()->GetTimerManager().SetTimer(m_SongTimerHandle, this, &AFretBoard::PlaySong, 2.0f, false);
}

// Called every frame
void AFretBoard::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

void AFretBoard::InterpretFretStates(int32 fretInput)
{
	switch (fretInput)
	{
		case EInputEnum::GUITAR_GREEN:
		{
			SetRecieverDepressed(mGreenReciever);
			break;
		}
		case EInputEnum::GUITAR_RED:
		{
			SetRecieverDepressed(mRedReciever);
			break;
		}
		case EInputEnum::GUITAR_YELLOW:
		{
			SetRecieverDepressed(mYellowReciever);
			break;
		}
		case EInputEnum::GUITAR_BLUE:
		{
			SetRecieverDepressed(mBlueReciever);
			break;
		}
		case EInputEnum::GUITAR_ORANGE:
		{
			SetRecieverDepressed(mOrangeReciever);
			break;
		}
	}

}

void AFretBoard::ClearSong()
{
	TArray<AActor*> scorekeeper;
	UGameplayStatics::GetAllActorsOfClass(this, AScoreKeeper::StaticClass(), scorekeeper);

	if (scorekeeper.Num() == 1)
	{
		AScoreKeeper* scoreKeep = Cast<AScoreKeeper>(scorekeeper[0]);


		auto setToZero = [](AActor* rec) 
		{		
			AReciever* reciever = Cast<AReciever>(rec);
			reciever->SetHitNoteCount(0); 
		};

		setToZero(mGreenReciever->ChildActor);
		setToZero(mRedReciever->ChildActor);
		setToZero(mBlueReciever->ChildActor);
		setToZero(mYellowReciever->ChildActor);
		setToZero(mOrangeReciever->ChildActor);
		m_HitCount = 0;
		m_HitPercent = 0;
		m_TotalNoteCount = 0;
		m_MissedCount = 0;

		for (auto note : m_Notes)
		{
			if (note->IsValidLowLevel())
			{
				note->Destroy();
			}

		}

		RiffAudioComponent->Stop();
		GetWorldTimerManager().ClearTimer(m_SongTimerHandle);
	}
	else
	{
		print_text(TEXT("NO SCOREKEEPER IN SCENE"));
	}
}

void AFretBoard::PlaySong()
{
	ADanShredderGameMode * gameMode = Cast<ADanShredderGameMode>(GetGameInstance());
	
	auto track = m_RiffBank->GetRandomTrackAsArray();
		ClearSong();

		m_CurrentWaveForm = track.m_SoundWave;
		
		SpawnNotes(track.m_Notes);
		GetWorldTimerManager().SetTimer(m_SongTimerHandle, this, &AFretBoard::PlayAudio, m_Delay / 1000.0f, false);

		m_TotalNoteCount = track.m_Notes.Num();
}

void AFretBoard::PlayAudio()
{
	if (RiffAudioComponent != nullptr)
	{
		RiffAudioComponent->Stop();
		RiffAudioComponent->SetSound(m_CurrentWaveForm);
		RiffAudioComponent->Play();
	}
}

void AFretBoard::SetState(EFretBoardState state)
{	
	m_State = state;
}

void AFretBoard::PointBeamAtEnemy(AEnemy* a_Enemy)
{
	//Work in progress. Switching to work on pause menu then will finish this
	AEnemy* targetEnemy = a_Enemy;
	AActor* targetActor = targetEnemy->GetChildActorComponent()->ChildActor;

	if (targetEnemy != nullptr)
	{
		mLightningAttackLeft->Activate();
		mLightningAttackRight->Activate();
		if (!mLaserSound->IsPlaying())
		{
			mLaserSound->Play();
		}
		mLightningAttackRight->SetActorParameter("BeamTarget", targetActor);
		mLightningAttackLeft->SetActorParameter("BeamTarget", targetActor);
		mSparksEnemy->Activate();
		mSparksEnemy->SetWorldLocation(targetEnemy->GetChildActorComponent()->GetComponentLocation());
	}
	else
	{
		mLightningAttackLeft->Deactivate();
		mLightningAttackRight->Deactivate();
		mSparksEnemy->Deactivate();
		mLaserSound->Stop();
	}
}

void AFretBoard::SetRecieverDepressed(UChildActorComponent * recieverComp)
{
	AReciever * rec = Cast<AReciever>(recieverComp->ChildActor);

	if (rec != nullptr)
		rec->SetDepressed(!(rec->GetDepressed()));

}

void AFretBoard::SpawnNotes(TArray<FNoteMetaData> notes)
{
	m_LowerBound = INT32_MAX;
	m_UpperBound = -1;
	for (FNoteMetaData note : notes)
	{
		if (note.m_Note < m_LowerBound)
		{
			m_LowerBound = note.m_Note;
		}
		if (note.m_Note > m_UpperBound)
		{
			m_UpperBound = note.m_Note;
		}
	}

	for (FNoteMetaData note : notes)
	{
		m_CurrentNote = note.m_Note - m_LowerBound;
		FVector BaseLocation;
		switch (m_CurrentNote)
		{
		case 0:
		{
			m_NewNoteColor = GREEN_COLOR;

			FTransform greenNoteTransform = mGreenReciever->GetRelativeTransform();
			 BaseLocation = greenNoteTransform.GetLocation() + FVector(0, 0, -2);
			float wat = ((((float)note.m_StartTime + m_Delay) / 1000.0f) * m_NoteSpeed) - m_SyncVal;
			BaseLocation += (FVector(1.0f, 0.0f, 0.0f) * wat);
			break;
		}
		case 1:
		{
			m_NewNoteColor = RED_COLOR;

			FTransform      RedNoteTransform = mRedReciever->GetRelativeTransform();
			BaseLocation = RedNoteTransform.GetLocation() + FVector(0, 0, -2);
			float wat = ((((float)note.m_StartTime + m_Delay) / 1000.0f) * m_NoteSpeed) - m_SyncVal;
			BaseLocation += (FVector(1.0f, 0.0f, 0.0f) * wat);
			break;
		}

		case 2:
		{
			m_NewNoteColor =   YELLOW_COLOR;

			FTransform		YellowNoteTransform = mYellowReciever->GetRelativeTransform();
			BaseLocation = YellowNoteTransform.GetLocation() + FVector(0, 0, -2);
			float wat = ((((float)note.m_StartTime + m_Delay) / 1000.0f) * m_NoteSpeed) - m_SyncVal;
			BaseLocation += (FVector(1.0f, 0.0f, 0.0f) * wat);
			break;
		}
		case 3:
		{
			m_NewNoteColor =  BLUE_COLOR;

			FTransform		BlueNoteTransform = mBlueReciever->GetRelativeTransform();
			BaseLocation = BlueNoteTransform.GetLocation() + FVector(0, 0, -2);
			float wat = ((((float)note.m_StartTime + m_Delay) / 1000.0f) * m_NoteSpeed) - m_SyncVal;
			BaseLocation += (FVector(1.0f, 0.0f, 0.0f) * wat);
			break;
		}
		case 4:
		{
			m_NewNoteColor = ORANGE_COLOR;

			FTransform		OrangeNoteTransform = mOrangeReciever->GetRelativeTransform();
			BaseLocation = OrangeNoteTransform.GetLocation() + FVector(0, 0, -2);
			float wat = ((((float)note.m_StartTime + m_Delay) / 1000.0f) * m_NoteSpeed) - m_SyncVal;
			BaseLocation += (FVector(1.0f, 0.0f, 0.0f) * wat);
			break;
		}
		}

		FTransform trans = FTransform();
		trans.SetScale3D(FVector(1.0f, 1.0f, 1.0f));
		AFretboardNote* newNoteActor = (AFretboardNote*)(this->GetWorld()->SpawnActor(AFretboardNote::StaticClass()));

		AFretboardNote* newNote = Cast<AFretboardNote>(newNoteActor);
		newNote->SetActorRelativeTransform(trans);
		newNote->AttachRootComponentToActor(this);


		FTransform newRelativeTrans = FTransform();
		newRelativeTrans.SetLocation(BaseLocation);
		newRelativeTrans.SetScale3D(FVector(1.0f, 1.0f, 1.0f));

		newNote->SetActorRelativeTransform(newRelativeTrans);
		m_Notes.Add(newNote);

		newNote->SetColor(m_NewNoteColor);
		newNote->SetNoteSpeed(m_NoteSpeed);
		
	}
}

void AFretBoard::SetGreenReceiverActive(bool active)
{
	AReciever* rec = Cast<AReciever>(mGreenReciever->ChildActor);
	rec->SetIsActive(active);
}

void AFretBoard::SetRedReceiverActive(bool active)
{
	Cast<AReciever>(mRedReciever->ChildActor)->SetIsActive(active);
}

void AFretBoard::SetYellowReceiverActive(bool active)
{
	Cast<AReciever>(mYellowReciever->ChildActor)->SetIsActive(active);
}

void AFretBoard::SetBlueReceiverActive(bool active)
{
	Cast<AReciever>(mBlueReciever->ChildActor)->SetIsActive(active);
}

void AFretBoard::SetOrangeReceiverActive(bool active)
{
	Cast<AReciever>(mOrangeReciever->ChildActor)->SetIsActive(active);
}

bool AFretBoard::GetIsHyperDriveActive()
{
	return m_IsHyperDriveActive;
}

void AFretBoard::SetIsHyperDriveActive(bool state)
{

	m_IsHyperDriveActive = state;
}

float AFretBoard::GenerateHyperdriveAmount()
{
	return m_HitPercent * m_MaxHyperdrivePercent * FMath::FRandRange(0.0f, 1.0f) * (FMath::RandRange(0, 2) - 1);
}


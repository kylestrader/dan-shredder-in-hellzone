// Fill out your copyright notice in the Description page of Project Settings.

#include "RockinRails.h"
#include "RangedEnemy.h"
#include "Utilities.h"


// Sets default values
ARangedEnemy::ARangedEnemy()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ARangedEnemy::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ARangedEnemy::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	switch (mCurrentState)
	{
	case E_EnemyState::E_FIGHTING:
		//CALL ATTACK AFTER SET TIMER (EXPOSED THROUGH BLUEPRINTS)
		// ATTACK MAY BE VERY DIFFERENT FOR EACH RANGED ENEMY
		break;
	case E_EnemyState::E_IDLE:
		//ENEMY IDLE
		break;
	case E_EnemyState::E_PATHING:
		mCurrentState = E_EnemyState::E_FIGHTING;
		break;
	}
}

void ARangedEnemy::OnOverlap(AActor* other)
{
	switch (mCurrentState)
	{
	case(E_EnemyState::E_FIGHTING) :
		// somehow the player walked into us
		if (other->GetClass() == mPlayerRef->GetClass())
		{
			Die();
		}
		break;
	case(E_EnemyState::E_IDLE) :
		// Do nothing for now
		break;
	case(E_EnemyState::E_PATHING) :
		// check current location along path, if on last node call OnPathFinished()
		break;
	}
}

void ARangedEnemy::Attack()
{
	print_text("[ENEMY RANGED] Attack called");
}

void ARangedEnemy::OnPathFinished()
{
	print_text("[ENEMY RANGED] OnPathFinished called");
}


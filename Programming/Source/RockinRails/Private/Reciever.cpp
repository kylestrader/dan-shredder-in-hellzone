// Fill out your copyright notice in the Description page of Project Settings.

#include "RockinRails.h"
#include "Reciever.h"
#include "Utilities.h"
#include "ScoreKeeper.h"
#include "FretboardNote.h"

// Sets default values
AReciever::AReciever()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	mColor = FVector(0.0f, 0.0f, 0.0f);

	//Create the Root Component
	USphereComponent* sphere = CreateDefaultSubobject<USphereComponent>(TEXT("Root"));
	RootComponent = sphere;
	sphere->InitSphereRadius(1.0f);
	sphere->SetCollisionProfileName(TEXT("None"));

	//Create the component that will represent the collision boundaries
	mBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Colission"));
	mBox->AttachTo(RootComponent);
	mBox->SetRelativeLocation(FVector(15.0f, 0.0f, 0.0f));
	mBox->SetRelativeScale3D(FVector(0.75f, 0.5625f, 0.421875f));
	mBox->bGenerateOverlapEvents = true;
	mBox->SetCollisionProfileName(TEXT("OverlapAllDynamic"));
	mBox->OnComponentBeginOverlap.AddDynamic(this, &AReciever::OnComponentBeginOverlap);

	//Create the Static Mesh Component
	mRecieverMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Reciever"));
	mRecieverMesh->AttachTo(RootComponent);	
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ReceiverMesh(TEXT("StaticMesh'/Game/HUD/Fret_Bar_Assets/Note.Note'"));
	if (ReceiverMesh.Succeeded())
	{
		mRecieverMesh->SetStaticMesh(ReceiverMesh.Object);
	}

	// Set the ReceiverMesh's material
	static ConstructorHelpers::FObjectFinder<UMaterialInstance> ReceiverMaterial(TEXT("MaterialInstanceConstant'/Game/HUD/Fret_Bar_Textures/Note_Instance_Yellow.Note_Instance_Yellow'"));
	if (ReceiverMaterial.Succeeded())
	{
		UMaterialInterface* mat = (UMaterialInstance*)ReceiverMaterial.Object;
		mRecieverMesh->SetMaterial(0, mat);
	}

	// Create Particle System
	mParticleSystem = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Particle System"));
	mParticleSystem->AttachTo(RootComponent);
	mParticleSystem->bAutoActivate = false;
	mParticleSystem->SetRelativeLocation(FVector::ZeroVector);
	// Find asset and set the result to the particle system's template

	static ConstructorHelpers::FObjectFinder<UParticleSystem> ReceiverParticleSystem(TEXT("ParticleSystem'/Game/Particles/Note_Explosion.Note_Explosion'"));
	if (ReceiverParticleSystem.Succeeded())
	{
		mParticleSystem->SetTemplate(ReceiverParticleSystem.Object);
	}

	SetColor(mColor);
	FretDepressOffSet = -4;
}

void AReciever::OnConstruction(const FTransform& Transform)
{
	mRecieverMesh->SetRelativeLocation(FVector(0.0f, 0.0f, 5.0f));
	mRecieverMesh->SetRelativeScale3D(FVector(1));
}

// Called when the game starts or when spawned
void AReciever::BeginPlay()
{
	Super::BeginPlay();


	FVector newLocation = mRecieverMesh->GetRelativeTransform().GetLocation();

	m_BazeZPosition = newLocation.Z;
}

// Called every frame
void AReciever::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	FVector newLoc;
	FVector oldLoc = mRecieverMesh->GetRelativeTransform().GetLocation();

	float ZLerpValue = m_isDepressed ? FretDepressOffSet : m_BazeZPosition;

	newLoc = FVector(oldLoc.X, oldLoc.Y, ZLerpValue);


	mBox->SetRelativeLocation(FMath::Lerp(oldLoc, newLoc, .5));
	mRecieverMesh->SetRelativeLocation(FMath::Lerp(oldLoc, newLoc, .5));
}

void AReciever::OnComponentBeginOverlap(AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (OtherActor->GetClass() == AFretboardNote::StaticClass())
	{
		m_ContainObj = OtherActor;
		m_CollideNote = true;
		HandleNoteHit();
	}
	else
	{
		m_CollideNote = false;
	}

}

void AReciever::OnComponentEndOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	m_CollideNote = false;
}

void AReciever::SetColor(FVector color)
{
	mColor = color;
	mMID = mRecieverMesh->CreateDynamicMaterialInstance(0);

	if (mMID != nullptr)
	{
		mMID->SetVectorParameterValue("Note_Base_Color", FLinearColor(color));
		mRecieverMesh->SetMaterial(0, mMID);
	}
}

void AReciever::SetGlow(float glow)
{
	mGlow = glow;
	mMID = mRecieverMesh->CreateDynamicMaterialInstance(0);
	if (mMID != nullptr)
	{
		mMID->SetScalarParameterValue("Glow", glow);
		mRecieverMesh->SetMaterial(0, mMID);
	}
}

void AReciever::SetIsActive(bool active)
{
	m_isActive = active;
}

void AReciever::HandleNoteHit()
{
	if (m_isActive)
	{
		if (m_ContainObj->IsValidLowLevel() && !m_ContainObj->IsPendingKill())
		{
			mParticleSystem->Activate(true);
			m_isActive = false;
			m_ContainObj->Destroy();
			SetHitNoteCount(GetHitNoteCount() + 1);
		}
	}
}

void AReciever::SetDepressed(bool depressed)
{
	//print_text(TEXT("Pressed Or Released Button"));
 m_isDepressed = depressed; 
}

bool AReciever::GetDepressed()
{
	return m_isDepressed;
}

void AReciever::SetHitNoteCount(int32 amount)
{
	m_HitNoteCount = amount;
}

int32 AReciever::GetHitNoteCount()
{
	return m_HitNoteCount;
}


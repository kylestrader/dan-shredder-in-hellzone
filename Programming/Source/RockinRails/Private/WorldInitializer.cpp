// Fill out your copyright notice in the Description page of Project Settings.

#include "RockinRails.h"
#include "WorldInitializer.h"


// Sets default values
AWorldInitializer::AWorldInitializer()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AWorldInitializer::BeginPlay()
{
	Super::BeginPlay();
	GetWorld()->GetGameViewport()->SetDisableSplitscreenOverride(true);
}

// Called every frame
void AWorldInitializer::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}


//Maya ASCII 2016 scene
//Name: Boss_anim_Idle_v04.ma
//Last modified: Thu, Mar 31, 2016 01:50:24 AM
//Codeset: 1252
file -rdi 1 -ns "Boss_rig_reference" -rfn "Boss_rig_referenceRN" -op "v=0;" 
		-typ "mayaAscii" "E:/Mary's Files/school_Spring 2016/DanShredder/Repo/capstone-2015-16-t7-git/Trunk/RockinRails/Art/Enemies/Boss//scenes/Boss_rig_reference.ma";
file -r -ns "Boss_rig_reference" -dr 1 -rfn "Boss_rig_referenceRN" -op "v=0;" -typ
		 "mayaAscii" "E:/Mary's Files/school_Spring 2016/DanShredder/Repo/capstone-2015-16-t7-git/Trunk/RockinRails/Art/Enemies/Boss//scenes/Boss_rig_reference.ma";
requires maya "2016";
requires -nodeType "ikSpringSolver" "ikSpringSolver" "1.0";
requires -nodeType "gameFbxExporter" "gameFbxExporter" "1.0";
requires "stereoCamera" "10.0";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Microsoft Windows 8 Home Premium Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "26C8EA44-4112-A8B8-E79D-ABA5F381D7E6";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -92.71486176060904 507.24501040574825 1831.0480360323136 ;
	setAttr ".r" -type "double3" -4.5383527296277544 356.99999999998516 2.488218361681121e-017 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "CBD6AF25-4C0B-475D-C991-A3842D24DECA";
	setAttr -k off ".v" no;
	setAttr ".pze" yes;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 1778.2973339611153;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -8.6847697589359996 383.53966768816304 47.249111632086993 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "9BB5A585-4A46-DE5A-DD90-C4A25C6EAB36";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "9680E2F9-4AF0-E22C-7FE5-1DAB48E5FD1C";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "2811C5B8-4DE4-A496-12EA-A6AB975135AD";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "203CA8DF-4088-E107-7531-CFA0BC0E02B1";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "3275C4CF-4869-B25E-4EC8-9AA73CC42F35";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "8F917F6E-47F3-2FDF-E8C1-4296B597C4F1";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "8498F06E-48E1-1447-9886-2786353E01DE";
	setAttr -s 8 ".lnk";
	setAttr -s 8 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "F61A3BBD-442F-D226-ADEB-9CB50BA5481F";
createNode displayLayer -n "defaultLayer";
	rename -uid "6E0B1049-4F9F-719B-586E-57853A5F7446";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "5C6F44D4-4E21-6B4E-9DFE-6CBF3A3986FF";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "F47B02FF-4801-EB61-BFC9-7384475651F0";
	setAttr ".g" yes;
createNode reference -n "Boss_rig_referenceRN";
	rename -uid "E9C72C8A-41FF-9DE5-B569-4484630E34C4";
	setAttr -s 391 ".phl";
	setAttr ".phl[1]" 0;
	setAttr ".phl[2]" 0;
	setAttr ".phl[3]" 0;
	setAttr ".phl[4]" 0;
	setAttr ".phl[5]" 0;
	setAttr ".phl[6]" 0;
	setAttr ".phl[7]" 0;
	setAttr ".phl[8]" 0;
	setAttr ".phl[9]" 0;
	setAttr ".phl[10]" 0;
	setAttr ".phl[11]" 0;
	setAttr ".phl[12]" 0;
	setAttr ".phl[13]" 0;
	setAttr ".phl[14]" 0;
	setAttr ".phl[15]" 0;
	setAttr ".phl[16]" 0;
	setAttr ".phl[17]" 0;
	setAttr ".phl[18]" 0;
	setAttr ".phl[19]" 0;
	setAttr ".phl[20]" 0;
	setAttr ".phl[21]" 0;
	setAttr ".phl[22]" 0;
	setAttr ".phl[23]" 0;
	setAttr ".phl[24]" 0;
	setAttr ".phl[25]" 0;
	setAttr ".phl[26]" 0;
	setAttr ".phl[27]" 0;
	setAttr ".phl[28]" 0;
	setAttr ".phl[29]" 0;
	setAttr ".phl[30]" 0;
	setAttr ".phl[31]" 0;
	setAttr ".phl[32]" 0;
	setAttr ".phl[33]" 0;
	setAttr ".phl[34]" 0;
	setAttr ".phl[35]" 0;
	setAttr ".phl[36]" 0;
	setAttr ".phl[37]" 0;
	setAttr ".phl[38]" 0;
	setAttr ".phl[39]" 0;
	setAttr ".phl[40]" 0;
	setAttr ".phl[41]" 0;
	setAttr ".phl[42]" 0;
	setAttr ".phl[43]" 0;
	setAttr ".phl[44]" 0;
	setAttr ".phl[45]" 0;
	setAttr ".phl[46]" 0;
	setAttr ".phl[47]" 0;
	setAttr ".phl[48]" 0;
	setAttr ".phl[49]" 0;
	setAttr ".phl[50]" 0;
	setAttr ".phl[51]" 0;
	setAttr ".phl[52]" 0;
	setAttr ".phl[53]" 0;
	setAttr ".phl[54]" 0;
	setAttr ".phl[55]" 0;
	setAttr ".phl[56]" 0;
	setAttr ".phl[57]" 0;
	setAttr ".phl[58]" 0;
	setAttr ".phl[59]" 0;
	setAttr ".phl[60]" 0;
	setAttr ".phl[61]" 0;
	setAttr ".phl[62]" 0;
	setAttr ".phl[63]" 0;
	setAttr ".phl[64]" 0;
	setAttr ".phl[65]" 0;
	setAttr ".phl[66]" 0;
	setAttr ".phl[67]" 0;
	setAttr ".phl[68]" 0;
	setAttr ".phl[69]" 0;
	setAttr ".phl[70]" 0;
	setAttr ".phl[71]" 0;
	setAttr ".phl[72]" 0;
	setAttr ".phl[73]" 0;
	setAttr ".phl[74]" 0;
	setAttr ".phl[75]" 0;
	setAttr ".phl[76]" 0;
	setAttr ".phl[77]" 0;
	setAttr ".phl[78]" 0;
	setAttr ".phl[79]" 0;
	setAttr ".phl[80]" 0;
	setAttr ".phl[81]" 0;
	setAttr ".phl[82]" 0;
	setAttr ".phl[83]" 0;
	setAttr ".phl[84]" 0;
	setAttr ".phl[85]" 0;
	setAttr ".phl[86]" 0;
	setAttr ".phl[87]" 0;
	setAttr ".phl[88]" 0;
	setAttr ".phl[89]" 0;
	setAttr ".phl[90]" 0;
	setAttr ".phl[91]" 0;
	setAttr ".phl[92]" 0;
	setAttr ".phl[93]" 0;
	setAttr ".phl[94]" 0;
	setAttr ".phl[95]" 0;
	setAttr ".phl[96]" 0;
	setAttr ".phl[97]" 0;
	setAttr ".phl[98]" 0;
	setAttr ".phl[99]" 0;
	setAttr ".phl[100]" 0;
	setAttr ".phl[101]" 0;
	setAttr ".phl[102]" 0;
	setAttr ".phl[103]" 0;
	setAttr ".phl[104]" 0;
	setAttr ".phl[105]" 0;
	setAttr ".phl[106]" 0;
	setAttr ".phl[107]" 0;
	setAttr ".phl[108]" 0;
	setAttr ".phl[109]" 0;
	setAttr ".phl[110]" 0;
	setAttr ".phl[111]" 0;
	setAttr ".phl[112]" 0;
	setAttr ".phl[113]" 0;
	setAttr ".phl[114]" 0;
	setAttr ".phl[115]" 0;
	setAttr ".phl[116]" 0;
	setAttr ".phl[117]" 0;
	setAttr ".phl[118]" 0;
	setAttr ".phl[119]" 0;
	setAttr ".phl[120]" 0;
	setAttr ".phl[121]" 0;
	setAttr ".phl[122]" 0;
	setAttr ".phl[123]" 0;
	setAttr ".phl[124]" 0;
	setAttr ".phl[125]" 0;
	setAttr ".phl[126]" 0;
	setAttr ".phl[127]" 0;
	setAttr ".phl[128]" 0;
	setAttr ".phl[129]" 0;
	setAttr ".phl[130]" 0;
	setAttr ".phl[131]" 0;
	setAttr ".phl[132]" 0;
	setAttr ".phl[133]" 0;
	setAttr ".phl[134]" 0;
	setAttr ".phl[135]" 0;
	setAttr ".phl[136]" 0;
	setAttr ".phl[137]" 0;
	setAttr ".phl[138]" 0;
	setAttr ".phl[139]" 0;
	setAttr ".phl[140]" 0;
	setAttr ".phl[141]" 0;
	setAttr ".phl[142]" 0;
	setAttr ".phl[143]" 0;
	setAttr ".phl[144]" 0;
	setAttr ".phl[145]" 0;
	setAttr ".phl[146]" 0;
	setAttr ".phl[147]" 0;
	setAttr ".phl[148]" 0;
	setAttr ".phl[149]" 0;
	setAttr ".phl[150]" 0;
	setAttr ".phl[151]" 0;
	setAttr ".phl[152]" 0;
	setAttr ".phl[153]" 0;
	setAttr ".phl[154]" 0;
	setAttr ".phl[155]" 0;
	setAttr ".phl[156]" 0;
	setAttr ".phl[157]" 0;
	setAttr ".phl[158]" 0;
	setAttr ".phl[159]" 0;
	setAttr ".phl[160]" 0;
	setAttr ".phl[161]" 0;
	setAttr ".phl[162]" 0;
	setAttr ".phl[163]" 0;
	setAttr ".phl[164]" 0;
	setAttr ".phl[165]" 0;
	setAttr ".phl[166]" 0;
	setAttr ".phl[167]" 0;
	setAttr ".phl[168]" 0;
	setAttr ".phl[169]" 0;
	setAttr ".phl[170]" 0;
	setAttr ".phl[171]" 0;
	setAttr ".phl[172]" 0;
	setAttr ".phl[173]" 0;
	setAttr ".phl[174]" 0;
	setAttr ".phl[175]" 0;
	setAttr ".phl[176]" 0;
	setAttr ".phl[177]" 0;
	setAttr ".phl[178]" 0;
	setAttr ".phl[179]" 0;
	setAttr ".phl[180]" 0;
	setAttr ".phl[181]" 0;
	setAttr ".phl[182]" 0;
	setAttr ".phl[183]" 0;
	setAttr ".phl[184]" 0;
	setAttr ".phl[185]" 0;
	setAttr ".phl[186]" 0;
	setAttr ".phl[187]" 0;
	setAttr ".phl[188]" 0;
	setAttr ".phl[189]" 0;
	setAttr ".phl[190]" 0;
	setAttr ".phl[191]" 0;
	setAttr ".phl[192]" 0;
	setAttr ".phl[193]" 0;
	setAttr ".phl[194]" 0;
	setAttr ".phl[195]" 0;
	setAttr ".phl[196]" 0;
	setAttr ".phl[197]" 0;
	setAttr ".phl[198]" 0;
	setAttr ".phl[199]" 0;
	setAttr ".phl[200]" 0;
	setAttr ".phl[201]" 0;
	setAttr ".phl[202]" 0;
	setAttr ".phl[203]" 0;
	setAttr ".phl[204]" 0;
	setAttr ".phl[205]" 0;
	setAttr ".phl[206]" 0;
	setAttr ".phl[207]" 0;
	setAttr ".phl[208]" 0;
	setAttr ".phl[209]" 0;
	setAttr ".phl[210]" 0;
	setAttr ".phl[211]" 0;
	setAttr ".phl[212]" 0;
	setAttr ".phl[213]" 0;
	setAttr ".phl[214]" 0;
	setAttr ".phl[215]" 0;
	setAttr ".phl[216]" 0;
	setAttr ".phl[217]" 0;
	setAttr ".phl[218]" 0;
	setAttr ".phl[219]" 0;
	setAttr ".phl[220]" 0;
	setAttr ".phl[221]" 0;
	setAttr ".phl[222]" 0;
	setAttr ".phl[223]" 0;
	setAttr ".phl[224]" 0;
	setAttr ".phl[225]" 0;
	setAttr ".phl[226]" 0;
	setAttr ".phl[227]" 0;
	setAttr ".phl[228]" 0;
	setAttr ".phl[229]" 0;
	setAttr ".phl[230]" 0;
	setAttr ".phl[231]" 0;
	setAttr ".phl[232]" 0;
	setAttr ".phl[233]" 0;
	setAttr ".phl[234]" 0;
	setAttr ".phl[235]" 0;
	setAttr ".phl[236]" 0;
	setAttr ".phl[237]" 0;
	setAttr ".phl[238]" 0;
	setAttr ".phl[239]" 0;
	setAttr ".phl[240]" 0;
	setAttr ".phl[241]" 0;
	setAttr ".phl[242]" 0;
	setAttr ".phl[243]" 0;
	setAttr ".phl[244]" 0;
	setAttr ".phl[245]" 0;
	setAttr ".phl[246]" 0;
	setAttr ".phl[247]" 0;
	setAttr ".phl[248]" 0;
	setAttr ".phl[249]" 0;
	setAttr ".phl[250]" 0;
	setAttr ".phl[251]" 0;
	setAttr ".phl[252]" 0;
	setAttr ".phl[253]" 0;
	setAttr ".phl[254]" 0;
	setAttr ".phl[255]" 0;
	setAttr ".phl[256]" 0;
	setAttr ".phl[257]" 0;
	setAttr ".phl[258]" 0;
	setAttr ".phl[259]" 0;
	setAttr ".phl[260]" 0;
	setAttr ".phl[261]" 0;
	setAttr ".phl[262]" 0;
	setAttr ".phl[263]" 0;
	setAttr ".phl[264]" 0;
	setAttr ".phl[265]" 0;
	setAttr ".phl[266]" 0;
	setAttr ".phl[267]" 0;
	setAttr ".phl[268]" 0;
	setAttr ".phl[269]" 0;
	setAttr ".phl[270]" 0;
	setAttr ".phl[271]" 0;
	setAttr ".phl[272]" 0;
	setAttr ".phl[273]" 0;
	setAttr ".phl[274]" 0;
	setAttr ".phl[275]" 0;
	setAttr ".phl[276]" 0;
	setAttr ".phl[277]" 0;
	setAttr ".phl[278]" 0;
	setAttr ".phl[279]" 0;
	setAttr ".phl[280]" 0;
	setAttr ".phl[281]" 0;
	setAttr ".phl[282]" 0;
	setAttr ".phl[283]" 0;
	setAttr ".phl[284]" 0;
	setAttr ".phl[285]" 0;
	setAttr ".phl[286]" 0;
	setAttr ".phl[287]" 0;
	setAttr ".phl[288]" 0;
	setAttr ".phl[289]" 0;
	setAttr ".phl[290]" 0;
	setAttr ".phl[291]" 0;
	setAttr ".phl[292]" 0;
	setAttr ".phl[293]" 0;
	setAttr ".phl[294]" 0;
	setAttr ".phl[295]" 0;
	setAttr ".phl[296]" 0;
	setAttr ".phl[297]" 0;
	setAttr ".phl[298]" 0;
	setAttr ".phl[299]" 0;
	setAttr ".phl[300]" 0;
	setAttr ".phl[301]" 0;
	setAttr ".phl[302]" 0;
	setAttr ".phl[303]" 0;
	setAttr ".phl[304]" 0;
	setAttr ".phl[305]" 0;
	setAttr ".phl[306]" 0;
	setAttr ".phl[307]" 0;
	setAttr ".phl[308]" 0;
	setAttr ".phl[309]" 0;
	setAttr ".phl[310]" 0;
	setAttr ".phl[311]" 0;
	setAttr ".phl[312]" 0;
	setAttr ".phl[313]" 0;
	setAttr ".phl[314]" 0;
	setAttr ".phl[315]" 0;
	setAttr ".phl[316]" 0;
	setAttr ".phl[317]" 0;
	setAttr ".phl[318]" 0;
	setAttr ".phl[319]" 0;
	setAttr ".phl[320]" 0;
	setAttr ".phl[321]" 0;
	setAttr ".phl[322]" 0;
	setAttr ".phl[323]" 0;
	setAttr ".phl[324]" 0;
	setAttr ".phl[325]" 0;
	setAttr ".phl[326]" 0;
	setAttr ".phl[327]" 0;
	setAttr ".phl[328]" 0;
	setAttr ".phl[329]" 0;
	setAttr ".phl[330]" 0;
	setAttr ".phl[331]" 0;
	setAttr ".phl[332]" 0;
	setAttr ".phl[333]" 0;
	setAttr ".phl[334]" 0;
	setAttr ".phl[335]" 0;
	setAttr ".phl[336]" 0;
	setAttr ".phl[337]" 0;
	setAttr ".phl[338]" 0;
	setAttr ".phl[339]" 0;
	setAttr ".phl[340]" 0;
	setAttr ".phl[341]" 0;
	setAttr ".phl[342]" 0;
	setAttr ".phl[343]" 0;
	setAttr ".phl[344]" 0;
	setAttr ".phl[345]" 0;
	setAttr ".phl[346]" 0;
	setAttr ".phl[347]" 0;
	setAttr ".phl[348]" 0;
	setAttr ".phl[349]" 0;
	setAttr ".phl[350]" 0;
	setAttr ".phl[351]" 0;
	setAttr ".phl[352]" 0;
	setAttr ".phl[353]" 0;
	setAttr ".phl[354]" 0;
	setAttr ".phl[355]" 0;
	setAttr ".phl[356]" 0;
	setAttr ".phl[357]" 0;
	setAttr ".phl[358]" 0;
	setAttr ".phl[359]" 0;
	setAttr ".phl[360]" 0;
	setAttr ".phl[361]" 0;
	setAttr ".phl[362]" 0;
	setAttr ".phl[363]" 0;
	setAttr ".phl[364]" 0;
	setAttr ".phl[365]" 0;
	setAttr ".phl[366]" 0;
	setAttr ".phl[367]" 0;
	setAttr ".phl[368]" 0;
	setAttr ".phl[369]" 0;
	setAttr ".phl[370]" 0;
	setAttr ".phl[371]" 0;
	setAttr ".phl[372]" 0;
	setAttr ".phl[373]" 0;
	setAttr ".phl[374]" 0;
	setAttr ".phl[375]" 0;
	setAttr ".phl[376]" 0;
	setAttr ".phl[377]" 0;
	setAttr ".phl[378]" 0;
	setAttr ".phl[379]" 0;
	setAttr ".phl[380]" 0;
	setAttr ".phl[381]" 0;
	setAttr ".phl[382]" 0;
	setAttr ".phl[383]" 0;
	setAttr ".phl[384]" 0;
	setAttr ".phl[385]" 0;
	setAttr ".phl[386]" 0;
	setAttr ".phl[387]" 0;
	setAttr ".phl[388]" 0;
	setAttr ".phl[389]" 0;
	setAttr ".phl[390]" 0;
	setAttr ".phl[391]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"Boss_rig_referenceRN"
		"Boss_rig_referenceRN" 2
		2 "|Boss_rig_reference:geo_bass" "translate" " -type \"double3\" 5.8703891615874966 53.083824525660077 -5.9809107083466984"
		
		2 "|Boss_rig_reference:geo_bass" "rotate" " -type \"double3\" 0 0 -6.3907459353932499"
		
		"Boss_rig_referenceRN" 725
		2 "|Boss_rig_reference:geo_LP_body|Boss_rig_reference:geo_LP_bodyShape" "visibility" 
		" -k 0 1"
		2 "|Boss_rig_reference:geo_LP_body|Boss_rig_reference:geo_LP_bodyShape" "uvPivot" 
		" -type \"double2\" 0.49314749240875244 0.50709009263664484"
		2 "|Boss_rig_reference:geo_LP_strap|Boss_rig_reference:geo_LP_strapShape" 
		"visibility" " -k 0 1"
		2 "|Boss_rig_reference:geo_LP_strap|Boss_rig_reference:geo_LP_strapShape" 
		"uvPivot" " -type \"double2\" 0.50260746479034424 0.50837397575378418"
		2 "|Boss_rig_reference:geo_LP_shoulderpad_R|Boss_rig_reference:geo_LP_shoulderpad_RShape" 
		"visibility" " -k 0 1"
		2 "|Boss_rig_reference:geo_LP_shoulderpad_R|Boss_rig_reference:geo_LP_shoulderpad_RShape" 
		"uvPivot" " -type \"double2\" 0.48943412303924561 0.50184249877929688"
		2 "|Boss_rig_reference:geo_LP_shoulderpad_L|Boss_rig_reference:geo_LP_shoulderpad_LShape" 
		"visibility" " -k 0 1"
		2 "|Boss_rig_reference:geo_LP_shoulderpad_L|Boss_rig_reference:geo_LP_shoulderpad_LShape" 
		"uvPivot" " -type \"double2\" 0.48943412303924561 0.50184249877929688"
		2 "|Boss_rig_reference:geo_LP_horn|Boss_rig_reference:geo_LP_hornShape" "visibility" 
		" -k 0 1"
		2 "|Boss_rig_reference:geo_LP_horn|Boss_rig_reference:geo_LP_hornShape" "uvPivot" 
		" -type \"double2\" 0.50330281257629395 0.49672225117683411"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_L" "translate" 
		" -type \"double3\" -52.849844449308826 69.339125556781227 73.897548455626634"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_L" "translateX" 
		" -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_L" "translateY" 
		" -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_L" "translateZ" 
		" -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_L" "rotate" 
		" -type \"double3\" 60.185829218205939 16.767104062783073 -2.8011808701874688"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_L" "rotateX" 
		" -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_L" "rotateY" 
		" -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_L" "rotateZ" 
		" -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_R" "translate" 
		" -type \"double3\" 21.155856363666327 131.18775219243346 0.49448701104448389"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_R" "translateX" 
		" -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_R" "translateY" 
		" -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_R" "translateZ" 
		" -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_R" "rotate" 
		" -type \"double3\" 69.778383013076876 -23.008864726018615 17.366663964748604"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_R" "rotateX" 
		" -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_R" "rotateY" 
		" -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_R" "rotateZ" 
		" -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:grp_ctrl_leg_L_upperTwist|Boss_rig_reference:ctrl_leg_L_upperTwist" 
		"translate" " -type \"double3\" -6.4114456407220661 66.398209013856956 11.966575821794946"
		
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:grp_ctrl_leg_L_upperTwist|Boss_rig_reference:ctrl_leg_L_upperTwist" 
		"translateX" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:grp_ctrl_leg_L_upperTwist|Boss_rig_reference:ctrl_leg_L_upperTwist" 
		"translateY" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:grp_ctrl_leg_L_upperTwist|Boss_rig_reference:ctrl_leg_L_upperTwist" 
		"translateZ" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:grp_ctrl_leg_R_upperTwist|Boss_rig_reference:ctrl_leg_R_upperTwist" 
		"translate" " -type \"double3\" 5.2810434810581732 68.484766285981436 0.52897069799018415"
		
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:grp_ctrl_leg_R_upperTwist|Boss_rig_reference:ctrl_leg_R_upperTwist" 
		"translateX" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:grp_ctrl_leg_R_upperTwist|Boss_rig_reference:ctrl_leg_R_upperTwist" 
		"translateY" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:grp_ctrl_leg_R_upperTwist|Boss_rig_reference:ctrl_leg_R_upperTwist" 
		"translateZ" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans" "translate" 
		" -type \"double3\" 0 68.84927161710803 14.234683291224488"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans" "translateX" 
		" -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans" "translateY" 
		" -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans" "translateZ" 
		" -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans" "rotate" 
		" -type \"double3\" 2.5430542412882273 0 0"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans" "rotateX" 
		" -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hips" 
		"rotate" " -type \"double3\" 3.8522664194439771 -14.013794474357418 -26.465639012531128"
		
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hips" 
		"rotateX" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hips" 
		"rotateY" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hips" 
		"rotateZ" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2" 
		"rotate" " -type \"double3\" 6.6451736595316868 -0.3859851625041924 1.6284227661224833"
		
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2" 
		"rotateX" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2" 
		"rotateY" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2" 
		"rotateZ" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_Shape2" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_Shape2" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_Shape2" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_Shape2" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3" 
		"rotate" " -type \"double3\" 6.6451736595316868 -0.3859851625041924 1.6284227661224833"
		
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3" 
		"rotateX" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3" 
		"rotateY" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3" 
		"rotateZ" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_Shape3" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_Shape3" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_Shape3" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_Shape3" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4" 
		"rotate" " -type \"double3\" 6.6451736595316868 -0.3859851625041924 1.6284227661224833"
		
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4" 
		"rotateX" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4" 
		"rotateY" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4" 
		"rotateZ" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_spine_Shape4" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_spine_Shape4" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_spine_Shape4" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_spine_Shape4" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulder_L_FK|Boss_rig_reference:ctrl_shoulder_L_FKShape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulder_L_FK|Boss_rig_reference:ctrl_shoulder_L_FKShape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulder_L_FK|Boss_rig_reference:ctrl_shoulder_L_FKShape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulder_L_FK|Boss_rig_reference:ctrl_shoulder_L_FKShape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulder_L_FK|Boss_rig_reference:ctrl_elbow_L_FK|Boss_rig_reference:ctrl_elbow_L_FKShape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulder_L_FK|Boss_rig_reference:ctrl_elbow_L_FK|Boss_rig_reference:ctrl_elbow_L_FKShape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulder_L_FK|Boss_rig_reference:ctrl_elbow_L_FK|Boss_rig_reference:ctrl_elbow_L_FKShape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulder_L_FK|Boss_rig_reference:ctrl_elbow_L_FK|Boss_rig_reference:ctrl_elbow_L_FKShape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulder_L_FK|Boss_rig_reference:ctrl_elbow_L_FK|Boss_rig_reference:ctrl_wrist_L_FK|Boss_rig_reference:ctrl_wrist_L_FKShape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulder_L_FK|Boss_rig_reference:ctrl_elbow_L_FK|Boss_rig_reference:ctrl_wrist_L_FK|Boss_rig_reference:ctrl_wrist_L_FKShape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulder_L_FK|Boss_rig_reference:ctrl_elbow_L_FK|Boss_rig_reference:ctrl_wrist_L_FK|Boss_rig_reference:ctrl_wrist_L_FKShape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulder_L_FK|Boss_rig_reference:ctrl_elbow_L_FK|Boss_rig_reference:ctrl_wrist_L_FK|Boss_rig_reference:ctrl_wrist_L_FKShape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulder_R_FK|Boss_rig_reference:ctrl_shoulder_R_FKShape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulder_R_FK|Boss_rig_reference:ctrl_shoulder_R_FKShape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulder_R_FK|Boss_rig_reference:ctrl_shoulder_R_FKShape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulder_R_FK|Boss_rig_reference:ctrl_shoulder_R_FKShape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulder_R_FK|Boss_rig_reference:ctrl_elbow_R_FK|Boss_rig_reference:ctrl_elbow_R_FKShape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulder_R_FK|Boss_rig_reference:ctrl_elbow_R_FK|Boss_rig_reference:ctrl_elbow_R_FKShape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulder_R_FK|Boss_rig_reference:ctrl_elbow_R_FK|Boss_rig_reference:ctrl_elbow_R_FKShape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulder_R_FK|Boss_rig_reference:ctrl_elbow_R_FK|Boss_rig_reference:ctrl_elbow_R_FKShape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulder_R_FK|Boss_rig_reference:ctrl_elbow_R_FK|Boss_rig_reference:ctrl_wrist_R_FK|Boss_rig_reference:ctrl_wrist_R_FKShape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulder_R_FK|Boss_rig_reference:ctrl_elbow_R_FK|Boss_rig_reference:ctrl_wrist_R_FK|Boss_rig_reference:ctrl_wrist_R_FKShape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulder_R_FK|Boss_rig_reference:ctrl_elbow_R_FK|Boss_rig_reference:ctrl_wrist_R_FK|Boss_rig_reference:ctrl_wrist_R_FKShape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulder_R_FK|Boss_rig_reference:ctrl_elbow_R_FK|Boss_rig_reference:ctrl_wrist_R_FK|Boss_rig_reference:ctrl_wrist_R_FKShape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_R_IK" 
		"translate" " -type \"double3\" 104.40168242949909 -86.362023918913607 90.628164110443592"
		
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_R_IK" 
		"translateY" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_R_IK" 
		"translateZ" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_R_IK|Boss_rig_reference:ctrl_wrist_R_IKShape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_R_IK|Boss_rig_reference:ctrl_wrist_R_IKShape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_R_IK|Boss_rig_reference:ctrl_wrist_R_IKShape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_R_IK|Boss_rig_reference:ctrl_wrist_R_IKShape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_L_IK" 
		"translate" " -type \"double3\" -115.59432515744597 -76.55388124776583 31.54946975874223"
		
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_L_IK" 
		"translateY" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_L_IK" 
		"translateZ" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_L_IK" 
		"rotate" " -type \"double3\" 140.28565122860005 -79.987278806373382 -281.47435111255731"
		
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_L_IK" 
		"rotateX" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_L_IK" 
		"rotateY" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_L_IK" 
		"rotateZ" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_L_IK|Boss_rig_reference:ctrl_wrist_L_IKShape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_L_IK|Boss_rig_reference:ctrl_wrist_L_IKShape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_L_IK|Boss_rig_reference:ctrl_wrist_L_IKShape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_L_IK|Boss_rig_reference:ctrl_wrist_L_IKShape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_Shape1" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_Shape1" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_Shape1" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_Shape1" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_neck_Shape2" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_neck_Shape2" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_neck_Shape2" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_neck_Shape2" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head" 
		"rotate" " -type \"double3\" 23.373242083163639 0.27468449773261883 -10.002149679016332"
		
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head" 
		"rotateX" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head" 
		"rotateY" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head" 
		"rotateZ" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_R_1" 
		"rotate" " -type \"double3\" 0 15.016104577353925 0"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_R_1" 
		"rotateY" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_L_1" 
		"rotate" " -type \"double3\" 0 -13.900995547177947 0"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_L_1" 
		"rotateY" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_fingers_LShape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_fingers_LShape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_fingers_LShape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_fingers_LShape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1" 
		"rotateZ" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1" 
		"rotateX" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1" 
		"rotateY" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1|Boss_rig_reference:ctrl_pointerF_L_1Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1|Boss_rig_reference:ctrl_pointerF_L_1Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1|Boss_rig_reference:ctrl_pointerF_L_1Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1|Boss_rig_reference:ctrl_pointerF_L_1Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1|Boss_rig_reference:ctrl_pointerF_L_2" 
		"rotate" " -type \"double3\" 0 0 -100.11645005533711"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1|Boss_rig_reference:ctrl_pointerF_L_2" 
		"rotateZ" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1|Boss_rig_reference:ctrl_pointerF_L_2" 
		"rotateX" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1|Boss_rig_reference:ctrl_pointerF_L_2" 
		"rotateY" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1|Boss_rig_reference:ctrl_pointerF_L_2|Boss_rig_reference:ctrl_pointerF_L_2Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1|Boss_rig_reference:ctrl_pointerF_L_2|Boss_rig_reference:ctrl_pointerF_L_2Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1|Boss_rig_reference:ctrl_pointerF_L_2|Boss_rig_reference:ctrl_pointerF_L_2Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1|Boss_rig_reference:ctrl_pointerF_L_2|Boss_rig_reference:ctrl_pointerF_L_2Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1|Boss_rig_reference:ctrl_pointerF_L_2|Boss_rig_reference:ctrl_pointerF_L_3|Boss_rig_reference:ctrl_pointerF_L_3Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1|Boss_rig_reference:ctrl_pointerF_L_2|Boss_rig_reference:ctrl_pointerF_L_3|Boss_rig_reference:ctrl_pointerF_L_3Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1|Boss_rig_reference:ctrl_pointerF_L_2|Boss_rig_reference:ctrl_pointerF_L_3|Boss_rig_reference:ctrl_pointerF_L_3Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1|Boss_rig_reference:ctrl_pointerF_L_2|Boss_rig_reference:ctrl_pointerF_L_3|Boss_rig_reference:ctrl_pointerF_L_3Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1" 
		"rotateZ" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1" 
		"rotateX" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1" 
		"rotateY" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1|Boss_rig_reference:ctrl_middleF_L_1Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1|Boss_rig_reference:ctrl_middleF_L_1Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1|Boss_rig_reference:ctrl_middleF_L_1Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1|Boss_rig_reference:ctrl_middleF_L_1Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1|Boss_rig_reference:ctrl_middleF_L_2" 
		"rotate" " -type \"double3\" 0 0 -100.11645005533711"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1|Boss_rig_reference:ctrl_middleF_L_2" 
		"rotateZ" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1|Boss_rig_reference:ctrl_middleF_L_2" 
		"rotateX" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1|Boss_rig_reference:ctrl_middleF_L_2" 
		"rotateY" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1|Boss_rig_reference:ctrl_middleF_L_2|Boss_rig_reference:ctrl_middleF_L_2Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1|Boss_rig_reference:ctrl_middleF_L_2|Boss_rig_reference:ctrl_middleF_L_2Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1|Boss_rig_reference:ctrl_middleF_L_2|Boss_rig_reference:ctrl_middleF_L_2Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1|Boss_rig_reference:ctrl_middleF_L_2|Boss_rig_reference:ctrl_middleF_L_2Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1|Boss_rig_reference:ctrl_middleF_L_2|Boss_rig_reference:ctrl_middleF_L_3|Boss_rig_reference:ctrl_middleF_L_3Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1|Boss_rig_reference:ctrl_middleF_L_2|Boss_rig_reference:ctrl_middleF_L_3|Boss_rig_reference:ctrl_middleF_L_3Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1|Boss_rig_reference:ctrl_middleF_L_2|Boss_rig_reference:ctrl_middleF_L_3|Boss_rig_reference:ctrl_middleF_L_3Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1|Boss_rig_reference:ctrl_middleF_L_2|Boss_rig_reference:ctrl_middleF_L_3|Boss_rig_reference:ctrl_middleF_L_3Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1" 
		"rotateZ" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1" 
		"rotateX" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1" 
		"rotateY" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1|Boss_rig_reference:ctrl_ringF_L_1Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1|Boss_rig_reference:ctrl_ringF_L_1Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1|Boss_rig_reference:ctrl_ringF_L_1Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1|Boss_rig_reference:ctrl_ringF_L_1Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1|Boss_rig_reference:ctrl_ringF_L_2" 
		"rotate" " -type \"double3\" 0 0 -100.11645005533711"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1|Boss_rig_reference:ctrl_ringF_L_2" 
		"rotateZ" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1|Boss_rig_reference:ctrl_ringF_L_2" 
		"rotateX" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1|Boss_rig_reference:ctrl_ringF_L_2" 
		"rotateY" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1|Boss_rig_reference:ctrl_ringF_L_2|Boss_rig_reference:ctrl_ringF_L_2Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1|Boss_rig_reference:ctrl_ringF_L_2|Boss_rig_reference:ctrl_ringF_L_2Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1|Boss_rig_reference:ctrl_ringF_L_2|Boss_rig_reference:ctrl_ringF_L_2Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1|Boss_rig_reference:ctrl_ringF_L_2|Boss_rig_reference:ctrl_ringF_L_2Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1|Boss_rig_reference:ctrl_ringF_L_2|Boss_rig_reference:ctrl_ringF_L_3|Boss_rig_reference:ctrl_ringF_L_3Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1|Boss_rig_reference:ctrl_ringF_L_2|Boss_rig_reference:ctrl_ringF_L_3|Boss_rig_reference:ctrl_ringF_L_3Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1|Boss_rig_reference:ctrl_ringF_L_2|Boss_rig_reference:ctrl_ringF_L_3|Boss_rig_reference:ctrl_ringF_L_3Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1|Boss_rig_reference:ctrl_ringF_L_2|Boss_rig_reference:ctrl_ringF_L_3|Boss_rig_reference:ctrl_ringF_L_3Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1" 
		"rotateZ" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1" 
		"rotateX" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1" 
		"rotateY" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1|Boss_rig_reference:ctrl_pinkyF_L_1Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1|Boss_rig_reference:ctrl_pinkyF_L_1Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1|Boss_rig_reference:ctrl_pinkyF_L_1Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1|Boss_rig_reference:ctrl_pinkyF_L_1Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1|Boss_rig_reference:ctrl_pinkyF_L_2" 
		"rotate" " -type \"double3\" 0 0 -100.11645005533711"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1|Boss_rig_reference:ctrl_pinkyF_L_2" 
		"rotateZ" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1|Boss_rig_reference:ctrl_pinkyF_L_2" 
		"rotateX" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1|Boss_rig_reference:ctrl_pinkyF_L_2" 
		"rotateY" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1|Boss_rig_reference:ctrl_pinkyF_L_2|Boss_rig_reference:ctrl_pinkyF_L_2Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1|Boss_rig_reference:ctrl_pinkyF_L_2|Boss_rig_reference:ctrl_pinkyF_L_2Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1|Boss_rig_reference:ctrl_pinkyF_L_2|Boss_rig_reference:ctrl_pinkyF_L_2Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1|Boss_rig_reference:ctrl_pinkyF_L_2|Boss_rig_reference:ctrl_pinkyF_L_2Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1|Boss_rig_reference:ctrl_pinkyF_L_2|Boss_rig_reference:ctrl_pinkyF_L_3|Boss_rig_reference:ctrl_pinkyF_L_3Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1|Boss_rig_reference:ctrl_pinkyF_L_2|Boss_rig_reference:ctrl_pinkyF_L_3|Boss_rig_reference:ctrl_pinkyF_L_3Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1|Boss_rig_reference:ctrl_pinkyF_L_2|Boss_rig_reference:ctrl_pinkyF_L_3|Boss_rig_reference:ctrl_pinkyF_L_3Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1|Boss_rig_reference:ctrl_pinkyF_L_2|Boss_rig_reference:ctrl_pinkyF_L_3|Boss_rig_reference:ctrl_pinkyF_L_3Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1" 
		"rotate" " -type \"double3\" 27.460070411320089 3.4761467708811158 -15.373291132242127"
		
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1" 
		"rotateX" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1" 
		"rotateY" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1" 
		"rotateZ" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1|Boss_rig_reference:ctrl_thumb_L_1Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1|Boss_rig_reference:ctrl_thumb_L_1Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1|Boss_rig_reference:ctrl_thumb_L_1Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1|Boss_rig_reference:ctrl_thumb_L_1Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1|Boss_rig_reference:ctrl_thumb_L_2" 
		"rotate" " -type \"double3\" 0 -17.479632362345406 0"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1|Boss_rig_reference:ctrl_thumb_L_2" 
		"rotateX" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1|Boss_rig_reference:ctrl_thumb_L_2" 
		"rotateY" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1|Boss_rig_reference:ctrl_thumb_L_2" 
		"rotateZ" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1|Boss_rig_reference:ctrl_thumb_L_2|Boss_rig_reference:ctrl_thumb_L_2Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1|Boss_rig_reference:ctrl_thumb_L_2|Boss_rig_reference:ctrl_thumb_L_2Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1|Boss_rig_reference:ctrl_thumb_L_2|Boss_rig_reference:ctrl_thumb_L_2Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1|Boss_rig_reference:ctrl_thumb_L_2|Boss_rig_reference:ctrl_thumb_L_2Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1|Boss_rig_reference:ctrl_thumb_L_2|Boss_rig_reference:ctrl_thumb_L_3" 
		"rotate" " -type \"double3\" 0 -29.02054027323927 0"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1|Boss_rig_reference:ctrl_thumb_L_2|Boss_rig_reference:ctrl_thumb_L_3" 
		"rotateX" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1|Boss_rig_reference:ctrl_thumb_L_2|Boss_rig_reference:ctrl_thumb_L_3" 
		"rotateY" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1|Boss_rig_reference:ctrl_thumb_L_2|Boss_rig_reference:ctrl_thumb_L_3" 
		"rotateZ" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1|Boss_rig_reference:ctrl_thumb_L_2|Boss_rig_reference:ctrl_thumb_L_3|Boss_rig_reference:ctrl_thumb_L_3Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1|Boss_rig_reference:ctrl_thumb_L_2|Boss_rig_reference:ctrl_thumb_L_3|Boss_rig_reference:ctrl_thumb_L_3Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1|Boss_rig_reference:ctrl_thumb_L_2|Boss_rig_reference:ctrl_thumb_L_3|Boss_rig_reference:ctrl_thumb_L_3Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1|Boss_rig_reference:ctrl_thumb_L_2|Boss_rig_reference:ctrl_thumb_L_3|Boss_rig_reference:ctrl_thumb_L_3Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R" 
		"fingerCurl" " -av -k 1 2.6"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_fingers_RShape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_fingers_RShape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_fingers_RShape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_fingers_RShape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1" 
		"rotate" " -type \"double3\" 0 0 9.5026343393438175"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1" 
		"rotateZ" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1|Boss_rig_reference:ctrl_pointerF_R_1Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1|Boss_rig_reference:ctrl_pointerF_R_1Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1|Boss_rig_reference:ctrl_pointerF_R_1Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1|Boss_rig_reference:ctrl_pointerF_R_1Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1|Boss_rig_reference:ctrl_pointerF_R_2|Boss_rig_reference:ctrl_pointerF_R_2Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1|Boss_rig_reference:ctrl_pointerF_R_2|Boss_rig_reference:ctrl_pointerF_R_2Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1|Boss_rig_reference:ctrl_pointerF_R_2|Boss_rig_reference:ctrl_pointerF_R_2Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1|Boss_rig_reference:ctrl_pointerF_R_2|Boss_rig_reference:ctrl_pointerF_R_2Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1|Boss_rig_reference:ctrl_pointerF_R_2|Boss_rig_reference:ctrl_pointerF_R_3|Boss_rig_reference:ctrl_pointerF_R_3Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1|Boss_rig_reference:ctrl_pointerF_R_2|Boss_rig_reference:ctrl_pointerF_R_3|Boss_rig_reference:ctrl_pointerF_R_3Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1|Boss_rig_reference:ctrl_pointerF_R_2|Boss_rig_reference:ctrl_pointerF_R_3|Boss_rig_reference:ctrl_pointerF_R_3Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1|Boss_rig_reference:ctrl_pointerF_R_2|Boss_rig_reference:ctrl_pointerF_R_3|Boss_rig_reference:ctrl_pointerF_R_3Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1" 
		"rotate" " -type \"double3\" 0 0 9.5026343393438175"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1" 
		"rotateZ" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1|Boss_rig_reference:ctrl_middleF_R_1Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1|Boss_rig_reference:ctrl_middleF_R_1Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1|Boss_rig_reference:ctrl_middleF_R_1Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1|Boss_rig_reference:ctrl_middleF_R_1Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1|Boss_rig_reference:ctrl_middleF_R_2|Boss_rig_reference:ctrl_middleF_R_2Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1|Boss_rig_reference:ctrl_middleF_R_2|Boss_rig_reference:ctrl_middleF_R_2Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1|Boss_rig_reference:ctrl_middleF_R_2|Boss_rig_reference:ctrl_middleF_R_2Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1|Boss_rig_reference:ctrl_middleF_R_2|Boss_rig_reference:ctrl_middleF_R_2Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1|Boss_rig_reference:ctrl_middleF_R_2|Boss_rig_reference:ctrl_middleF_R_3|Boss_rig_reference:ctrl_middleF_R_3Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1|Boss_rig_reference:ctrl_middleF_R_2|Boss_rig_reference:ctrl_middleF_R_3|Boss_rig_reference:ctrl_middleF_R_3Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1|Boss_rig_reference:ctrl_middleF_R_2|Boss_rig_reference:ctrl_middleF_R_3|Boss_rig_reference:ctrl_middleF_R_3Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1|Boss_rig_reference:ctrl_middleF_R_2|Boss_rig_reference:ctrl_middleF_R_3|Boss_rig_reference:ctrl_middleF_R_3Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1" 
		"rotate" " -type \"double3\" 0 0 9.5026343393438175"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1" 
		"rotateZ" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1|Boss_rig_reference:ctrl_ringF_R_1Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1|Boss_rig_reference:ctrl_ringF_R_1Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1|Boss_rig_reference:ctrl_ringF_R_1Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1|Boss_rig_reference:ctrl_ringF_R_1Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1|Boss_rig_reference:ctrl_ringF_R_2|Boss_rig_reference:ctrl_ringF_R_2Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1|Boss_rig_reference:ctrl_ringF_R_2|Boss_rig_reference:ctrl_ringF_R_2Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1|Boss_rig_reference:ctrl_ringF_R_2|Boss_rig_reference:ctrl_ringF_R_2Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1|Boss_rig_reference:ctrl_ringF_R_2|Boss_rig_reference:ctrl_ringF_R_2Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1|Boss_rig_reference:ctrl_ringF_R_2|Boss_rig_reference:ctrl_ringF_R_3|Boss_rig_reference:ctrl_ringF_R_3Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1|Boss_rig_reference:ctrl_ringF_R_2|Boss_rig_reference:ctrl_ringF_R_3|Boss_rig_reference:ctrl_ringF_R_3Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1|Boss_rig_reference:ctrl_ringF_R_2|Boss_rig_reference:ctrl_ringF_R_3|Boss_rig_reference:ctrl_ringF_R_3Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1|Boss_rig_reference:ctrl_ringF_R_2|Boss_rig_reference:ctrl_ringF_R_3|Boss_rig_reference:ctrl_ringF_R_3Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1" 
		"rotate" " -type \"double3\" 0 0 9.5026343393438175"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1" 
		"rotateZ" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1|Boss_rig_reference:ctrl_pinkyF_R_1Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1|Boss_rig_reference:ctrl_pinkyF_R_1Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1|Boss_rig_reference:ctrl_pinkyF_R_1Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1|Boss_rig_reference:ctrl_pinkyF_R_1Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1|Boss_rig_reference:ctrl_pinkyF_R_2|Boss_rig_reference:ctrl_pinkyF_R_2Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1|Boss_rig_reference:ctrl_pinkyF_R_2|Boss_rig_reference:ctrl_pinkyF_R_2Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1|Boss_rig_reference:ctrl_pinkyF_R_2|Boss_rig_reference:ctrl_pinkyF_R_2Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1|Boss_rig_reference:ctrl_pinkyF_R_2|Boss_rig_reference:ctrl_pinkyF_R_2Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1|Boss_rig_reference:ctrl_pinkyF_R_2|Boss_rig_reference:ctrl_pinkyF_R_3|Boss_rig_reference:ctrl_pinkyF_R_3Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1|Boss_rig_reference:ctrl_pinkyF_R_2|Boss_rig_reference:ctrl_pinkyF_R_3|Boss_rig_reference:ctrl_pinkyF_R_3Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1|Boss_rig_reference:ctrl_pinkyF_R_2|Boss_rig_reference:ctrl_pinkyF_R_3|Boss_rig_reference:ctrl_pinkyF_R_3Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1|Boss_rig_reference:ctrl_pinkyF_R_2|Boss_rig_reference:ctrl_pinkyF_R_3|Boss_rig_reference:ctrl_pinkyF_R_3Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_thumb_R_1|Boss_rig_reference:ctrl_thumb_R_1Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_thumb_R_1|Boss_rig_reference:ctrl_thumb_R_1Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_thumb_R_1|Boss_rig_reference:ctrl_thumb_R_1Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_thumb_R_1|Boss_rig_reference:ctrl_thumb_R_1Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_thumb_R_1|Boss_rig_reference:ctrl_thumb_R_2|Boss_rig_reference:ctrl_thumb_R_2Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_thumb_R_1|Boss_rig_reference:ctrl_thumb_R_2|Boss_rig_reference:ctrl_thumb_R_2Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_thumb_R_1|Boss_rig_reference:ctrl_thumb_R_2|Boss_rig_reference:ctrl_thumb_R_2Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_thumb_R_1|Boss_rig_reference:ctrl_thumb_R_2|Boss_rig_reference:ctrl_thumb_R_2Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_thumb_R_1|Boss_rig_reference:ctrl_thumb_R_2|Boss_rig_reference:ctrl_thumb_R_3|Boss_rig_reference:ctrl_thumb_R_3Shape" 
		"divisionsU" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_thumb_R_1|Boss_rig_reference:ctrl_thumb_R_2|Boss_rig_reference:ctrl_thumb_R_3|Boss_rig_reference:ctrl_thumb_R_3Shape" 
		"divisionsV" " 1"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_thumb_R_1|Boss_rig_reference:ctrl_thumb_R_2|Boss_rig_reference:ctrl_thumb_R_3|Boss_rig_reference:ctrl_thumb_R_3Shape" 
		"curvePrecision" " 7"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_thumb_R_1|Boss_rig_reference:ctrl_thumb_R_2|Boss_rig_reference:ctrl_thumb_R_3|Boss_rig_reference:ctrl_thumb_R_3Shape" 
		"curvePrecisionShaded" " 2"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulderpad_R" 
		"rotate" " -type \"double3\" 7.3645292364626931 0 0"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulderpad_R" 
		"rotateX" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulderpad_L" 
		"rotate" " -type \"double3\" 7.4008237533732668 0 0"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulderpad_L" 
		"rotateX" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_guitar" 
		"translate" " -type \"double3\" 14.210481695334991 12.890215015634155 -0.79159065583651866"
		
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_guitar" 
		"translateY" " -av"
		2 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_guitar" 
		"translateZ" " -av"
		2 "|Boss_rig_reference:geo_bass|Boss_rig_reference:geo_bassShape" "visibility" 
		" -k 0 1"
		2 "|Boss_rig_reference:geo_bass|Boss_rig_reference:geo_bassShape" "uvPivot" 
		" -type \"double2\" 0.5 0.5"
		2 "Boss_rig_reference:lyr_LP_shoulderpad" "displayType" " 2"
		2 "Boss_rig_reference:lyr_LP_strap" "displayType" " 2"
		2 "Boss_rig_reference:lyr_LP_body" "displayType" " 2"
		2 "Boss_rig_reference:lyr_LP_horn" "displayType" " 2"
		2 "Boss_rig_reference:lyr_bass" "displayType" " 2"
		2 "Boss_rig_reference:lyr_bass" "visibility" " 1"
		3 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_fingers_L_parentConstraint1.constraintTranslateX" 
		"|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L.translateX" 
		""
		3 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_fingers_L_parentConstraint1.constraintTranslateY" 
		"|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L.translateY" 
		""
		3 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_fingers_L_parentConstraint1.constraintTranslateZ" 
		"|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L.translateZ" 
		""
		3 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_fingers_L_parentConstraint1.constraintRotateX" 
		"|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L.rotateX" 
		""
		3 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_fingers_L_parentConstraint1.constraintRotateY" 
		"|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L.rotateY" 
		""
		3 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_fingers_L_parentConstraint1.constraintRotateZ" 
		"|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L.rotateZ" 
		""
		3 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_fingers_R_parentConstraint1.constraintTranslateX" 
		"|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R.translateX" 
		""
		3 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_fingers_R_parentConstraint1.constraintTranslateY" 
		"|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R.translateY" 
		""
		3 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_fingers_R_parentConstraint1.constraintTranslateZ" 
		"|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R.translateZ" 
		""
		3 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_fingers_R_parentConstraint1.constraintRotateX" 
		"|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R.rotateX" 
		""
		3 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_fingers_R_parentConstraint1.constraintRotateY" 
		"|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R.rotateY" 
		""
		3 "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_fingers_R_parentConstraint1.constraintRotateZ" 
		"|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R.rotateZ" 
		""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root.translateX" 
		"Boss_rig_referenceRN.placeHolderList[1]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root.translateY" 
		"Boss_rig_referenceRN.placeHolderList[2]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[3]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root.rotateX" "Boss_rig_referenceRN.placeHolderList[4]" 
		""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root.rotateY" "Boss_rig_referenceRN.placeHolderList[5]" 
		""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root.rotateZ" "Boss_rig_referenceRN.placeHolderList[6]" 
		""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root.scaleX" "Boss_rig_referenceRN.placeHolderList[7]" 
		""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root.scaleY" "Boss_rig_referenceRN.placeHolderList[8]" 
		""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root.scaleZ" "Boss_rig_referenceRN.placeHolderList[9]" 
		""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_L.LowerLegTwist" 
		"Boss_rig_referenceRN.placeHolderList[10]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_L.toeLift" 
		"Boss_rig_referenceRN.placeHolderList[11]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_L.translateX" 
		"Boss_rig_referenceRN.placeHolderList[12]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_L.translateY" 
		"Boss_rig_referenceRN.placeHolderList[13]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_L.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[14]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_L.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[15]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_L.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[16]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_L.rotateZ" 
		"Boss_rig_referenceRN.placeHolderList[17]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_L|Boss_rig_reference:ctrl_leg_L_PV_SPRING.translateX" 
		"Boss_rig_referenceRN.placeHolderList[18]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_L|Boss_rig_reference:ctrl_leg_L_PV_SPRING.translateY" 
		"Boss_rig_referenceRN.placeHolderList[19]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_L|Boss_rig_reference:ctrl_leg_L_PV_SPRING.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[20]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_L|Boss_rig_reference:ctrl_leg_L_PV_SPRING.visibility" 
		"Boss_rig_referenceRN.placeHolderList[21]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_L|Boss_rig_reference:ctrl_leg_L_PV_SPRING.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[22]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_L|Boss_rig_reference:ctrl_leg_L_PV_SPRING.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[23]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_L|Boss_rig_reference:ctrl_leg_L_PV_SPRING.rotateZ" 
		"Boss_rig_referenceRN.placeHolderList[24]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_R.lowerLegTwist" 
		"Boss_rig_referenceRN.placeHolderList[25]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_R.toeLift" 
		"Boss_rig_referenceRN.placeHolderList[26]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_R.translateX" 
		"Boss_rig_referenceRN.placeHolderList[27]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_R.translateY" 
		"Boss_rig_referenceRN.placeHolderList[28]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_R.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[29]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_R.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[30]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_R.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[31]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_R.rotateZ" 
		"Boss_rig_referenceRN.placeHolderList[32]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_R|Boss_rig_reference:ctrl_leg_R_PV_SPRING.translateX" 
		"Boss_rig_referenceRN.placeHolderList[33]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_R|Boss_rig_reference:ctrl_leg_R_PV_SPRING.translateY" 
		"Boss_rig_referenceRN.placeHolderList[34]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_R|Boss_rig_reference:ctrl_leg_R_PV_SPRING.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[35]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_R|Boss_rig_reference:ctrl_leg_R_PV_SPRING.visibility" 
		"Boss_rig_referenceRN.placeHolderList[36]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_R|Boss_rig_reference:ctrl_leg_R_PV_SPRING.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[37]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_R|Boss_rig_reference:ctrl_leg_R_PV_SPRING.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[38]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_foot_R|Boss_rig_reference:ctrl_leg_R_PV_SPRING.rotateZ" 
		"Boss_rig_referenceRN.placeHolderList[39]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:grp_ctrl_leg_L_upperTwist|Boss_rig_reference:ctrl_leg_L_upperTwist.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[40]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:grp_ctrl_leg_L_upperTwist|Boss_rig_reference:ctrl_leg_L_upperTwist.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[41]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:grp_ctrl_leg_L_upperTwist|Boss_rig_reference:ctrl_leg_L_upperTwist.rotateZ" 
		"Boss_rig_referenceRN.placeHolderList[42]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:grp_ctrl_leg_L_upperTwist|Boss_rig_reference:ctrl_leg_L_upperTwist.translateX" 
		"Boss_rig_referenceRN.placeHolderList[43]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:grp_ctrl_leg_L_upperTwist|Boss_rig_reference:ctrl_leg_L_upperTwist.translateY" 
		"Boss_rig_referenceRN.placeHolderList[44]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:grp_ctrl_leg_L_upperTwist|Boss_rig_reference:ctrl_leg_L_upperTwist.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[45]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:grp_ctrl_leg_L_upperTwist|Boss_rig_reference:ctrl_leg_L_upperTwist.visibility" 
		"Boss_rig_referenceRN.placeHolderList[46]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:grp_ctrl_leg_R_upperTwist|Boss_rig_reference:ctrl_leg_R_upperTwist.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[47]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:grp_ctrl_leg_R_upperTwist|Boss_rig_reference:ctrl_leg_R_upperTwist.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[48]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:grp_ctrl_leg_R_upperTwist|Boss_rig_reference:ctrl_leg_R_upperTwist.rotateZ" 
		"Boss_rig_referenceRN.placeHolderList[49]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:grp_ctrl_leg_R_upperTwist|Boss_rig_reference:ctrl_leg_R_upperTwist.translateX" 
		"Boss_rig_referenceRN.placeHolderList[50]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:grp_ctrl_leg_R_upperTwist|Boss_rig_reference:ctrl_leg_R_upperTwist.translateY" 
		"Boss_rig_referenceRN.placeHolderList[51]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:grp_ctrl_leg_R_upperTwist|Boss_rig_reference:ctrl_leg_R_upperTwist.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[52]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:grp_ctrl_leg_R_upperTwist|Boss_rig_reference:ctrl_leg_R_upperTwist.visibility" 
		"Boss_rig_referenceRN.placeHolderList[53]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans.translateX" 
		"Boss_rig_referenceRN.placeHolderList[54]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans.translateY" 
		"Boss_rig_referenceRN.placeHolderList[55]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[56]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[57]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[58]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans.rotateZ" 
		"Boss_rig_referenceRN.placeHolderList[59]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans.visibility" 
		"Boss_rig_referenceRN.placeHolderList[60]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hips.translateX" 
		"Boss_rig_referenceRN.placeHolderList[61]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hips.translateY" 
		"Boss_rig_referenceRN.placeHolderList[62]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hips.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[63]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hips.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[64]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hips.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[65]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hips.rotateZ" 
		"Boss_rig_referenceRN.placeHolderList[66]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hips.visibility" 
		"Boss_rig_referenceRN.placeHolderList[67]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up.translateX" 
		"Boss_rig_referenceRN.placeHolderList[68]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up.translateY" 
		"Boss_rig_referenceRN.placeHolderList[69]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[70]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[71]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[72]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up.rotateZ" 
		"Boss_rig_referenceRN.placeHolderList[73]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up.visibility" 
		"Boss_rig_referenceRN.placeHolderList[74]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1.translateX" 
		"Boss_rig_referenceRN.placeHolderList[75]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1.translateY" 
		"Boss_rig_referenceRN.placeHolderList[76]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[77]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[78]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[79]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1.rotateZ" 
		"Boss_rig_referenceRN.placeHolderList[80]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1.visibility" 
		"Boss_rig_referenceRN.placeHolderList[81]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2.translateX" 
		"Boss_rig_referenceRN.placeHolderList[82]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2.translateY" 
		"Boss_rig_referenceRN.placeHolderList[83]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[84]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[85]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[86]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2.rotateZ" 
		"Boss_rig_referenceRN.placeHolderList[87]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2.visibility" 
		"Boss_rig_referenceRN.placeHolderList[88]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3.translateX" 
		"Boss_rig_referenceRN.placeHolderList[89]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3.translateY" 
		"Boss_rig_referenceRN.placeHolderList[90]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[91]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[92]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[93]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3.rotateZ" 
		"Boss_rig_referenceRN.placeHolderList[94]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3.visibility" 
		"Boss_rig_referenceRN.placeHolderList[95]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4.translateX" 
		"Boss_rig_referenceRN.placeHolderList[96]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4.translateY" 
		"Boss_rig_referenceRN.placeHolderList[97]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[98]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[99]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[100]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4.rotateZ" 
		"Boss_rig_referenceRN.placeHolderList[101]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4.visibility" 
		"Boss_rig_referenceRN.placeHolderList[102]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_R_IK.translateX" 
		"Boss_rig_referenceRN.placeHolderList[103]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_R_IK.translateY" 
		"Boss_rig_referenceRN.placeHolderList[104]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_R_IK.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[105]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_R_IK.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[106]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_R_IK.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[107]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_R_IK.rotateZ" 
		"Boss_rig_referenceRN.placeHolderList[108]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_L_IK.translateX" 
		"Boss_rig_referenceRN.placeHolderList[109]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_L_IK.translateY" 
		"Boss_rig_referenceRN.placeHolderList[110]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_L_IK.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[111]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_L_IK.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[112]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_L_IK.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[113]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_wrist_L_IK.rotateZ" 
		"Boss_rig_referenceRN.placeHolderList[114]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_arm_L_ikfk.ikfk" 
		"Boss_rig_referenceRN.placeHolderList[115]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_arm_R_ikfk.ikfk" 
		"Boss_rig_referenceRN.placeHolderList[116]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1.translateX" 
		"Boss_rig_referenceRN.placeHolderList[117]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1.translateY" 
		"Boss_rig_referenceRN.placeHolderList[118]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[119]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[120]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[121]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1.rotateZ" 
		"Boss_rig_referenceRN.placeHolderList[122]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1.visibility" 
		"Boss_rig_referenceRN.placeHolderList[123]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2.translateX" 
		"Boss_rig_referenceRN.placeHolderList[124]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2.translateY" 
		"Boss_rig_referenceRN.placeHolderList[125]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[126]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[127]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[128]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2.rotateZ" 
		"Boss_rig_referenceRN.placeHolderList[129]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2.visibility" 
		"Boss_rig_referenceRN.placeHolderList[130]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head.translateX" 
		"Boss_rig_referenceRN.placeHolderList[131]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head.translateY" 
		"Boss_rig_referenceRN.placeHolderList[132]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[133]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[134]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[135]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head.rotateZ" 
		"Boss_rig_referenceRN.placeHolderList[136]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head.visibility" 
		"Boss_rig_referenceRN.placeHolderList[137]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_R_1.translateX" 
		"Boss_rig_referenceRN.placeHolderList[138]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_R_1.translateY" 
		"Boss_rig_referenceRN.placeHolderList[139]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_R_1.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[140]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_R_1.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[141]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_R_1.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[142]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_R_1.rotateZ" 
		"Boss_rig_referenceRN.placeHolderList[143]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_R_1.scaleX" 
		"Boss_rig_referenceRN.placeHolderList[144]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_R_1.scaleY" 
		"Boss_rig_referenceRN.placeHolderList[145]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_R_1.scaleZ" 
		"Boss_rig_referenceRN.placeHolderList[146]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_R_1.visibility" 
		"Boss_rig_referenceRN.placeHolderList[147]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_R_1|Boss_rig_reference:ctrl_ear_R_2.translateX" 
		"Boss_rig_referenceRN.placeHolderList[148]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_R_1|Boss_rig_reference:ctrl_ear_R_2.translateY" 
		"Boss_rig_referenceRN.placeHolderList[149]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_R_1|Boss_rig_reference:ctrl_ear_R_2.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[150]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_R_1|Boss_rig_reference:ctrl_ear_R_2.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[151]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_R_1|Boss_rig_reference:ctrl_ear_R_2.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[152]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_R_1|Boss_rig_reference:ctrl_ear_R_2.rotateZ" 
		"Boss_rig_referenceRN.placeHolderList[153]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_R_1|Boss_rig_reference:ctrl_ear_R_2.scaleX" 
		"Boss_rig_referenceRN.placeHolderList[154]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_R_1|Boss_rig_reference:ctrl_ear_R_2.scaleY" 
		"Boss_rig_referenceRN.placeHolderList[155]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_R_1|Boss_rig_reference:ctrl_ear_R_2.scaleZ" 
		"Boss_rig_referenceRN.placeHolderList[156]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_R_1|Boss_rig_reference:ctrl_ear_R_2.visibility" 
		"Boss_rig_referenceRN.placeHolderList[157]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_L_1.translateX" 
		"Boss_rig_referenceRN.placeHolderList[158]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_L_1.translateY" 
		"Boss_rig_referenceRN.placeHolderList[159]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_L_1.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[160]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_L_1.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[161]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_L_1.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[162]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_L_1.rotateZ" 
		"Boss_rig_referenceRN.placeHolderList[163]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_L_1.scaleX" 
		"Boss_rig_referenceRN.placeHolderList[164]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_L_1.scaleY" 
		"Boss_rig_referenceRN.placeHolderList[165]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_L_1.scaleZ" 
		"Boss_rig_referenceRN.placeHolderList[166]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_L_1.visibility" 
		"Boss_rig_referenceRN.placeHolderList[167]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_L_1|Boss_rig_reference:ctrl_ear_L_2.translateX" 
		"Boss_rig_referenceRN.placeHolderList[168]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_L_1|Boss_rig_reference:ctrl_ear_L_2.translateY" 
		"Boss_rig_referenceRN.placeHolderList[169]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_L_1|Boss_rig_reference:ctrl_ear_L_2.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[170]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_L_1|Boss_rig_reference:ctrl_ear_L_2.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[171]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_L_1|Boss_rig_reference:ctrl_ear_L_2.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[172]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_L_1|Boss_rig_reference:ctrl_ear_L_2.rotateZ" 
		"Boss_rig_referenceRN.placeHolderList[173]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_L_1|Boss_rig_reference:ctrl_ear_L_2.scaleX" 
		"Boss_rig_referenceRN.placeHolderList[174]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_L_1|Boss_rig_reference:ctrl_ear_L_2.scaleY" 
		"Boss_rig_referenceRN.placeHolderList[175]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_L_1|Boss_rig_reference:ctrl_ear_L_2.scaleZ" 
		"Boss_rig_referenceRN.placeHolderList[176]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_neck_1|Boss_rig_reference:ctrl_neck_2|Boss_rig_reference:ctrl_head|Boss_rig_reference:ctrl_ear_L_1|Boss_rig_reference:ctrl_ear_L_2.visibility" 
		"Boss_rig_referenceRN.placeHolderList[177]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L.figerCurl" 
		"Boss_rig_referenceRN.placeHolderList[178]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L.translateX" 
		"Boss_rig_referenceRN.placeHolderList[179]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L.translateY" 
		"Boss_rig_referenceRN.placeHolderList[180]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[181]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[182]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[183]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L.rotateZ" 
		"Boss_rig_referenceRN.placeHolderList[184]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L.blendParent1" 
		"Boss_rig_referenceRN.placeHolderList[185]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1.translateX" 
		"Boss_rig_referenceRN.placeHolderList[186]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1.translateY" 
		"Boss_rig_referenceRN.placeHolderList[187]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[188]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1.visibility" 
		"Boss_rig_referenceRN.placeHolderList[189]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1|Boss_rig_reference:ctrl_pointerF_L_2.translateX" 
		"Boss_rig_referenceRN.placeHolderList[190]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1|Boss_rig_reference:ctrl_pointerF_L_2.translateY" 
		"Boss_rig_referenceRN.placeHolderList[191]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1|Boss_rig_reference:ctrl_pointerF_L_2.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[192]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1|Boss_rig_reference:ctrl_pointerF_L_2.visibility" 
		"Boss_rig_referenceRN.placeHolderList[193]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1|Boss_rig_reference:ctrl_pointerF_L_2|Boss_rig_reference:ctrl_pointerF_L_3.translateX" 
		"Boss_rig_referenceRN.placeHolderList[194]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1|Boss_rig_reference:ctrl_pointerF_L_2|Boss_rig_reference:ctrl_pointerF_L_3.translateY" 
		"Boss_rig_referenceRN.placeHolderList[195]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1|Boss_rig_reference:ctrl_pointerF_L_2|Boss_rig_reference:ctrl_pointerF_L_3.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[196]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pointerF_L_1|Boss_rig_reference:ctrl_pointerF_L_2|Boss_rig_reference:ctrl_pointerF_L_3.visibility" 
		"Boss_rig_referenceRN.placeHolderList[197]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1.translateX" 
		"Boss_rig_referenceRN.placeHolderList[198]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1.translateY" 
		"Boss_rig_referenceRN.placeHolderList[199]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[200]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1.visibility" 
		"Boss_rig_referenceRN.placeHolderList[201]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1|Boss_rig_reference:ctrl_middleF_L_2.translateX" 
		"Boss_rig_referenceRN.placeHolderList[202]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1|Boss_rig_reference:ctrl_middleF_L_2.translateY" 
		"Boss_rig_referenceRN.placeHolderList[203]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1|Boss_rig_reference:ctrl_middleF_L_2.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[204]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1|Boss_rig_reference:ctrl_middleF_L_2.visibility" 
		"Boss_rig_referenceRN.placeHolderList[205]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1|Boss_rig_reference:ctrl_middleF_L_2|Boss_rig_reference:ctrl_middleF_L_3.translateX" 
		"Boss_rig_referenceRN.placeHolderList[206]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1|Boss_rig_reference:ctrl_middleF_L_2|Boss_rig_reference:ctrl_middleF_L_3.translateY" 
		"Boss_rig_referenceRN.placeHolderList[207]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1|Boss_rig_reference:ctrl_middleF_L_2|Boss_rig_reference:ctrl_middleF_L_3.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[208]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_middleF_L_1|Boss_rig_reference:ctrl_middleF_L_2|Boss_rig_reference:ctrl_middleF_L_3.visibility" 
		"Boss_rig_referenceRN.placeHolderList[209]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1.translateX" 
		"Boss_rig_referenceRN.placeHolderList[210]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1.translateY" 
		"Boss_rig_referenceRN.placeHolderList[211]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[212]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1.visibility" 
		"Boss_rig_referenceRN.placeHolderList[213]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1|Boss_rig_reference:ctrl_ringF_L_2.translateX" 
		"Boss_rig_referenceRN.placeHolderList[214]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1|Boss_rig_reference:ctrl_ringF_L_2.translateY" 
		"Boss_rig_referenceRN.placeHolderList[215]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1|Boss_rig_reference:ctrl_ringF_L_2.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[216]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1|Boss_rig_reference:ctrl_ringF_L_2.visibility" 
		"Boss_rig_referenceRN.placeHolderList[217]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1|Boss_rig_reference:ctrl_ringF_L_2|Boss_rig_reference:ctrl_ringF_L_3.translateX" 
		"Boss_rig_referenceRN.placeHolderList[218]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1|Boss_rig_reference:ctrl_ringF_L_2|Boss_rig_reference:ctrl_ringF_L_3.translateY" 
		"Boss_rig_referenceRN.placeHolderList[219]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1|Boss_rig_reference:ctrl_ringF_L_2|Boss_rig_reference:ctrl_ringF_L_3.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[220]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_ringF_L_1|Boss_rig_reference:ctrl_ringF_L_2|Boss_rig_reference:ctrl_ringF_L_3.visibility" 
		"Boss_rig_referenceRN.placeHolderList[221]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1.translateX" 
		"Boss_rig_referenceRN.placeHolderList[222]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1.translateY" 
		"Boss_rig_referenceRN.placeHolderList[223]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[224]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1.visibility" 
		"Boss_rig_referenceRN.placeHolderList[225]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1|Boss_rig_reference:ctrl_pinkyF_L_2.translateX" 
		"Boss_rig_referenceRN.placeHolderList[226]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1|Boss_rig_reference:ctrl_pinkyF_L_2.translateY" 
		"Boss_rig_referenceRN.placeHolderList[227]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1|Boss_rig_reference:ctrl_pinkyF_L_2.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[228]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1|Boss_rig_reference:ctrl_pinkyF_L_2.visibility" 
		"Boss_rig_referenceRN.placeHolderList[229]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1|Boss_rig_reference:ctrl_pinkyF_L_2|Boss_rig_reference:ctrl_pinkyF_L_3.translateX" 
		"Boss_rig_referenceRN.placeHolderList[230]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1|Boss_rig_reference:ctrl_pinkyF_L_2|Boss_rig_reference:ctrl_pinkyF_L_3.translateY" 
		"Boss_rig_referenceRN.placeHolderList[231]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1|Boss_rig_reference:ctrl_pinkyF_L_2|Boss_rig_reference:ctrl_pinkyF_L_3.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[232]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_pinkyF_L_1|Boss_rig_reference:ctrl_pinkyF_L_2|Boss_rig_reference:ctrl_pinkyF_L_3.visibility" 
		"Boss_rig_referenceRN.placeHolderList[233]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1.translateX" 
		"Boss_rig_referenceRN.placeHolderList[234]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1.translateY" 
		"Boss_rig_referenceRN.placeHolderList[235]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[236]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1.visibility" 
		"Boss_rig_referenceRN.placeHolderList[237]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1|Boss_rig_reference:ctrl_thumb_L_2.translateX" 
		"Boss_rig_referenceRN.placeHolderList[238]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1|Boss_rig_reference:ctrl_thumb_L_2.translateY" 
		"Boss_rig_referenceRN.placeHolderList[239]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1|Boss_rig_reference:ctrl_thumb_L_2.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[240]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1|Boss_rig_reference:ctrl_thumb_L_2.visibility" 
		"Boss_rig_referenceRN.placeHolderList[241]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1|Boss_rig_reference:ctrl_thumb_L_2|Boss_rig_reference:ctrl_thumb_L_3.translateX" 
		"Boss_rig_referenceRN.placeHolderList[242]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1|Boss_rig_reference:ctrl_thumb_L_2|Boss_rig_reference:ctrl_thumb_L_3.translateY" 
		"Boss_rig_referenceRN.placeHolderList[243]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1|Boss_rig_reference:ctrl_thumb_L_2|Boss_rig_reference:ctrl_thumb_L_3.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[244]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_thumb_L_1|Boss_rig_reference:ctrl_thumb_L_2|Boss_rig_reference:ctrl_thumb_L_3.visibility" 
		"Boss_rig_referenceRN.placeHolderList[245]" ""
		5 3 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_fingers_L_parentConstraint1.constraintTranslateX" 
		"Boss_rig_referenceRN.placeHolderList[246]" "Boss_rig_reference:ctrl_fingers_L.tx"
		
		5 3 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_fingers_L_parentConstraint1.constraintTranslateY" 
		"Boss_rig_referenceRN.placeHolderList[247]" "Boss_rig_reference:ctrl_fingers_L.ty"
		
		5 3 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_fingers_L_parentConstraint1.constraintTranslateZ" 
		"Boss_rig_referenceRN.placeHolderList[248]" "Boss_rig_reference:ctrl_fingers_L.tz"
		
		5 3 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_fingers_L_parentConstraint1.constraintRotateX" 
		"Boss_rig_referenceRN.placeHolderList[249]" "Boss_rig_reference:ctrl_fingers_L.rx"
		
		5 3 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_fingers_L_parentConstraint1.constraintRotateY" 
		"Boss_rig_referenceRN.placeHolderList[250]" "Boss_rig_reference:ctrl_fingers_L.ry"
		
		5 3 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_L|Boss_rig_reference:ctrl_fingers_L_parentConstraint1.constraintRotateZ" 
		"Boss_rig_referenceRN.placeHolderList[251]" "Boss_rig_reference:ctrl_fingers_L.rz"
		
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R.fingerCurl" 
		"Boss_rig_referenceRN.placeHolderList[252]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R.translateX" 
		"Boss_rig_referenceRN.placeHolderList[253]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R.translateY" 
		"Boss_rig_referenceRN.placeHolderList[254]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[255]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[256]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[257]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R.rotateZ" 
		"Boss_rig_referenceRN.placeHolderList[258]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R.blendParent1" 
		"Boss_rig_referenceRN.placeHolderList[259]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[260]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[261]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1.translateX" 
		"Boss_rig_referenceRN.placeHolderList[262]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1.translateY" 
		"Boss_rig_referenceRN.placeHolderList[263]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[264]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1.visibility" 
		"Boss_rig_referenceRN.placeHolderList[265]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1|Boss_rig_reference:ctrl_pointerF_R_2.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[266]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1|Boss_rig_reference:ctrl_pointerF_R_2.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[267]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1|Boss_rig_reference:ctrl_pointerF_R_2.translateX" 
		"Boss_rig_referenceRN.placeHolderList[268]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1|Boss_rig_reference:ctrl_pointerF_R_2.translateY" 
		"Boss_rig_referenceRN.placeHolderList[269]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1|Boss_rig_reference:ctrl_pointerF_R_2.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[270]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1|Boss_rig_reference:ctrl_pointerF_R_2.visibility" 
		"Boss_rig_referenceRN.placeHolderList[271]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1|Boss_rig_reference:ctrl_pointerF_R_2|Boss_rig_reference:ctrl_pointerF_R_3.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[272]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1|Boss_rig_reference:ctrl_pointerF_R_2|Boss_rig_reference:ctrl_pointerF_R_3.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[273]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1|Boss_rig_reference:ctrl_pointerF_R_2|Boss_rig_reference:ctrl_pointerF_R_3.translateX" 
		"Boss_rig_referenceRN.placeHolderList[274]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1|Boss_rig_reference:ctrl_pointerF_R_2|Boss_rig_reference:ctrl_pointerF_R_3.translateY" 
		"Boss_rig_referenceRN.placeHolderList[275]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1|Boss_rig_reference:ctrl_pointerF_R_2|Boss_rig_reference:ctrl_pointerF_R_3.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[276]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pointerF_R_1|Boss_rig_reference:ctrl_pointerF_R_2|Boss_rig_reference:ctrl_pointerF_R_3.visibility" 
		"Boss_rig_referenceRN.placeHolderList[277]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[278]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[279]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1.translateX" 
		"Boss_rig_referenceRN.placeHolderList[280]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1.translateY" 
		"Boss_rig_referenceRN.placeHolderList[281]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[282]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1.visibility" 
		"Boss_rig_referenceRN.placeHolderList[283]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1|Boss_rig_reference:ctrl_middleF_R_2.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[284]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1|Boss_rig_reference:ctrl_middleF_R_2.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[285]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1|Boss_rig_reference:ctrl_middleF_R_2.translateX" 
		"Boss_rig_referenceRN.placeHolderList[286]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1|Boss_rig_reference:ctrl_middleF_R_2.translateY" 
		"Boss_rig_referenceRN.placeHolderList[287]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1|Boss_rig_reference:ctrl_middleF_R_2.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[288]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1|Boss_rig_reference:ctrl_middleF_R_2.visibility" 
		"Boss_rig_referenceRN.placeHolderList[289]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1|Boss_rig_reference:ctrl_middleF_R_2|Boss_rig_reference:ctrl_middleF_R_3.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[290]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1|Boss_rig_reference:ctrl_middleF_R_2|Boss_rig_reference:ctrl_middleF_R_3.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[291]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1|Boss_rig_reference:ctrl_middleF_R_2|Boss_rig_reference:ctrl_middleF_R_3.translateX" 
		"Boss_rig_referenceRN.placeHolderList[292]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1|Boss_rig_reference:ctrl_middleF_R_2|Boss_rig_reference:ctrl_middleF_R_3.translateY" 
		"Boss_rig_referenceRN.placeHolderList[293]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1|Boss_rig_reference:ctrl_middleF_R_2|Boss_rig_reference:ctrl_middleF_R_3.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[294]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_middleF_R_1|Boss_rig_reference:ctrl_middleF_R_2|Boss_rig_reference:ctrl_middleF_R_3.visibility" 
		"Boss_rig_referenceRN.placeHolderList[295]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[296]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[297]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1.translateX" 
		"Boss_rig_referenceRN.placeHolderList[298]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1.translateY" 
		"Boss_rig_referenceRN.placeHolderList[299]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[300]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1.visibility" 
		"Boss_rig_referenceRN.placeHolderList[301]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1|Boss_rig_reference:ctrl_ringF_R_2.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[302]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1|Boss_rig_reference:ctrl_ringF_R_2.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[303]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1|Boss_rig_reference:ctrl_ringF_R_2.translateX" 
		"Boss_rig_referenceRN.placeHolderList[304]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1|Boss_rig_reference:ctrl_ringF_R_2.translateY" 
		"Boss_rig_referenceRN.placeHolderList[305]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1|Boss_rig_reference:ctrl_ringF_R_2.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[306]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1|Boss_rig_reference:ctrl_ringF_R_2.visibility" 
		"Boss_rig_referenceRN.placeHolderList[307]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1|Boss_rig_reference:ctrl_ringF_R_2|Boss_rig_reference:ctrl_ringF_R_3.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[308]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1|Boss_rig_reference:ctrl_ringF_R_2|Boss_rig_reference:ctrl_ringF_R_3.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[309]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1|Boss_rig_reference:ctrl_ringF_R_2|Boss_rig_reference:ctrl_ringF_R_3.translateX" 
		"Boss_rig_referenceRN.placeHolderList[310]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1|Boss_rig_reference:ctrl_ringF_R_2|Boss_rig_reference:ctrl_ringF_R_3.translateY" 
		"Boss_rig_referenceRN.placeHolderList[311]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1|Boss_rig_reference:ctrl_ringF_R_2|Boss_rig_reference:ctrl_ringF_R_3.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[312]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_ringF_R_1|Boss_rig_reference:ctrl_ringF_R_2|Boss_rig_reference:ctrl_ringF_R_3.visibility" 
		"Boss_rig_referenceRN.placeHolderList[313]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[314]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[315]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1.translateX" 
		"Boss_rig_referenceRN.placeHolderList[316]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1.translateY" 
		"Boss_rig_referenceRN.placeHolderList[317]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[318]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1.visibility" 
		"Boss_rig_referenceRN.placeHolderList[319]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1|Boss_rig_reference:ctrl_pinkyF_R_2.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[320]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1|Boss_rig_reference:ctrl_pinkyF_R_2.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[321]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1|Boss_rig_reference:ctrl_pinkyF_R_2.translateX" 
		"Boss_rig_referenceRN.placeHolderList[322]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1|Boss_rig_reference:ctrl_pinkyF_R_2.translateY" 
		"Boss_rig_referenceRN.placeHolderList[323]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1|Boss_rig_reference:ctrl_pinkyF_R_2.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[324]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1|Boss_rig_reference:ctrl_pinkyF_R_2.visibility" 
		"Boss_rig_referenceRN.placeHolderList[325]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1|Boss_rig_reference:ctrl_pinkyF_R_2|Boss_rig_reference:ctrl_pinkyF_R_3.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[326]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1|Boss_rig_reference:ctrl_pinkyF_R_2|Boss_rig_reference:ctrl_pinkyF_R_3.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[327]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1|Boss_rig_reference:ctrl_pinkyF_R_2|Boss_rig_reference:ctrl_pinkyF_R_3.translateX" 
		"Boss_rig_referenceRN.placeHolderList[328]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1|Boss_rig_reference:ctrl_pinkyF_R_2|Boss_rig_reference:ctrl_pinkyF_R_3.translateY" 
		"Boss_rig_referenceRN.placeHolderList[329]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1|Boss_rig_reference:ctrl_pinkyF_R_2|Boss_rig_reference:ctrl_pinkyF_R_3.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[330]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_pinkyF_R_1|Boss_rig_reference:ctrl_pinkyF_R_2|Boss_rig_reference:ctrl_pinkyF_R_3.visibility" 
		"Boss_rig_referenceRN.placeHolderList[331]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_thumb_R_1.translateX" 
		"Boss_rig_referenceRN.placeHolderList[332]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_thumb_R_1.translateY" 
		"Boss_rig_referenceRN.placeHolderList[333]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_thumb_R_1.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[334]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_thumb_R_1.visibility" 
		"Boss_rig_referenceRN.placeHolderList[335]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_thumb_R_1|Boss_rig_reference:ctrl_thumb_R_2.translateX" 
		"Boss_rig_referenceRN.placeHolderList[336]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_thumb_R_1|Boss_rig_reference:ctrl_thumb_R_2.translateY" 
		"Boss_rig_referenceRN.placeHolderList[337]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_thumb_R_1|Boss_rig_reference:ctrl_thumb_R_2.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[338]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_thumb_R_1|Boss_rig_reference:ctrl_thumb_R_2.visibility" 
		"Boss_rig_referenceRN.placeHolderList[339]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_thumb_R_1|Boss_rig_reference:ctrl_thumb_R_2|Boss_rig_reference:ctrl_thumb_R_3.translateX" 
		"Boss_rig_referenceRN.placeHolderList[340]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_thumb_R_1|Boss_rig_reference:ctrl_thumb_R_2|Boss_rig_reference:ctrl_thumb_R_3.translateY" 
		"Boss_rig_referenceRN.placeHolderList[341]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_thumb_R_1|Boss_rig_reference:ctrl_thumb_R_2|Boss_rig_reference:ctrl_thumb_R_3.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[342]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_thumb_R_1|Boss_rig_reference:ctrl_thumb_R_2|Boss_rig_reference:ctrl_thumb_R_3.visibility" 
		"Boss_rig_referenceRN.placeHolderList[343]" ""
		5 3 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_fingers_R_parentConstraint1.constraintTranslateX" 
		"Boss_rig_referenceRN.placeHolderList[344]" "Boss_rig_reference:ctrl_fingers_R.tx"
		
		5 3 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_fingers_R_parentConstraint1.constraintTranslateY" 
		"Boss_rig_referenceRN.placeHolderList[345]" "Boss_rig_reference:ctrl_fingers_R.ty"
		
		5 3 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_fingers_R_parentConstraint1.constraintTranslateZ" 
		"Boss_rig_referenceRN.placeHolderList[346]" "Boss_rig_reference:ctrl_fingers_R.tz"
		
		5 3 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_fingers_R_parentConstraint1.constraintRotateX" 
		"Boss_rig_referenceRN.placeHolderList[347]" "Boss_rig_reference:ctrl_fingers_R.rx"
		
		5 3 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_fingers_R_parentConstraint1.constraintRotateY" 
		"Boss_rig_referenceRN.placeHolderList[348]" "Boss_rig_reference:ctrl_fingers_R.ry"
		
		5 3 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_fingers_R|Boss_rig_reference:ctrl_fingers_R_parentConstraint1.constraintRotateZ" 
		"Boss_rig_referenceRN.placeHolderList[349]" "Boss_rig_reference:ctrl_fingers_R.rz"
		
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulderpad_R.translateX" 
		"Boss_rig_referenceRN.placeHolderList[350]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulderpad_R.translateY" 
		"Boss_rig_referenceRN.placeHolderList[351]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulderpad_R.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[352]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulderpad_R.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[353]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulderpad_R.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[354]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulderpad_R.rotateZ" 
		"Boss_rig_referenceRN.placeHolderList[355]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulderpad_R.visibility" 
		"Boss_rig_referenceRN.placeHolderList[356]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulderpad_L.translateX" 
		"Boss_rig_referenceRN.placeHolderList[357]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulderpad_L.translateY" 
		"Boss_rig_referenceRN.placeHolderList[358]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulderpad_L.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[359]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulderpad_L.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[360]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulderpad_L.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[361]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulderpad_L.rotateZ" 
		"Boss_rig_referenceRN.placeHolderList[362]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_shoulderpad_L.visibility" 
		"Boss_rig_referenceRN.placeHolderList[363]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_arm_L_IK_PV.translateX" 
		"Boss_rig_referenceRN.placeHolderList[364]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_arm_L_IK_PV.translateY" 
		"Boss_rig_referenceRN.placeHolderList[365]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_arm_L_IK_PV.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[366]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_arm_L_IK_PV.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[367]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_arm_L_IK_PV.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[368]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_arm_L_IK_PV.rotateZ" 
		"Boss_rig_referenceRN.placeHolderList[369]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_arm_L_IK_PV.scaleX" 
		"Boss_rig_referenceRN.placeHolderList[370]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_arm_L_IK_PV.scaleY" 
		"Boss_rig_referenceRN.placeHolderList[371]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_arm_L_IK_PV.scaleZ" 
		"Boss_rig_referenceRN.placeHolderList[372]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_arm_R_IK_PV.translateX" 
		"Boss_rig_referenceRN.placeHolderList[373]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_arm_R_IK_PV.translateY" 
		"Boss_rig_referenceRN.placeHolderList[374]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_arm_R_IK_PV.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[375]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_arm_R_IK_PV.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[376]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_arm_R_IK_PV.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[377]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_arm_R_IK_PV.rotateZ" 
		"Boss_rig_referenceRN.placeHolderList[378]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_arm_R_IK_PV.scaleX" 
		"Boss_rig_referenceRN.placeHolderList[379]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_arm_R_IK_PV.scaleY" 
		"Boss_rig_referenceRN.placeHolderList[380]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_arm_R_IK_PV.scaleZ" 
		"Boss_rig_referenceRN.placeHolderList[381]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_guitar.translateX" 
		"Boss_rig_referenceRN.placeHolderList[382]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_guitar.translateY" 
		"Boss_rig_referenceRN.placeHolderList[383]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_guitar.translateZ" 
		"Boss_rig_referenceRN.placeHolderList[384]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_guitar.rotateX" 
		"Boss_rig_referenceRN.placeHolderList[385]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_guitar.rotateY" 
		"Boss_rig_referenceRN.placeHolderList[386]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_guitar.rotateZ" 
		"Boss_rig_referenceRN.placeHolderList[387]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_guitar.scaleX" 
		"Boss_rig_referenceRN.placeHolderList[388]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_guitar.scaleY" 
		"Boss_rig_referenceRN.placeHolderList[389]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_guitar.scaleZ" 
		"Boss_rig_referenceRN.placeHolderList[390]" ""
		5 4 "Boss_rig_referenceRN" "|Boss_rig_reference:ctrl_root|Boss_rig_reference:ctrl_hips_trans|Boss_rig_reference:ctrl_hip_up|Boss_rig_reference:ctrl_spine_1|Boss_rig_reference:ctrl_spine_2|Boss_rig_reference:ctrl_spine_3|Boss_rig_reference:ctrl_spine_4|Boss_rig_reference:ctrl_guitar.visibility" 
		"Boss_rig_referenceRN.placeHolderList[391]" "";
	setAttr ".ptag" -type "string" "";
lockNode -l 1 ;
createNode ikSpringSolver -s -n "ikSpringSolver";
	rename -uid "731464BE-4676-B305-9D3B-92853BFCE359";
createNode script -n "uiConfigurationScriptNode";
	rename -uid "1BAB1AA6-41E2-FA2F-D05F-F9B077CB6201";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n"
		+ "            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n"
		+ "        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 0\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 0\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 0\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 0\n                -imagePlane 1\n                -joints 0\n"
		+ "                -ikHandles 0\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 0\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 0\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1306\n                -height 550\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 0\n            -imagePlane 1\n            -joints 0\n            -ikHandles 0\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 0\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 0\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1306\n            -height 550\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n"
		+ "            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 1\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -clipTime \"on\" \n                -stackedCurves 0\n"
		+ "                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n"
		+ "                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 1\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -clipTime \"on\" \n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\tif ($useSceneConfig) {\n\t\tscriptedPanel -e -to $panelName;\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n"
		+ "                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n"
		+ "                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n"
		+ "                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n"
		+ "                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n"
		+ "                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n"
		+ "                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n"
		+ "                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n"
		+ "                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n"
		+ "\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n"
		+ "\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n"
		+ "\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"Stereo\" -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels `;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -editorChanged \"updateModelPanelBar\" \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n"
		+ "                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n"
		+ "                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -editorChanged \"updateModelPanelBar\" \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n"
		+ "                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n"
		+ "                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n"
		+ "                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n"
		+ "                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n"
		+ "                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n"
		+ "\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -imagePlane 1\\n    -joints 0\\n    -ikHandles 0\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 0\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 0\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1306\\n    -height 550\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -imagePlane 1\\n    -joints 0\\n    -ikHandles 0\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 0\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 0\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1306\\n    -height 550\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "F8DD359D-4B24-8CE0-7A90-F5B8FF5D6922";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 29 -ast 0 -aet 30 ";
	setAttr ".st" 6;
createNode animCurveTL -n "ctrl_root_translateX";
	rename -uid "E11CC51E-4B72-3181-CF92-1FA814A31CED";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_root_translateY";
	rename -uid "960BE826-4877-3CAF-B0BE-40AA4E1E63DA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_root_translateZ";
	rename -uid "8565BBEC-4FD6-694B-F705-C18589E973C0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_root_rotateX";
	rename -uid "8537952A-4EEF-3C9A-C978-E1BD0B7DD268";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_root_rotateY";
	rename -uid "B791DC6E-46C9-CED1-3B30-CF8CE176A2DD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_root_rotateZ";
	rename -uid "CF8AA7D5-486B-1DC9-1808-6DA6801EC81C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_root_scaleX";
	rename -uid "76670168-460E-1A6F-7D72-35B1FA19C387";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_root_scaleY";
	rename -uid "7FD280A5-41ED-5153-FEE2-02B854773C21";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_root_scaleZ";
	rename -uid "12298E14-43B3-2BAC-A145-D1892248CCC9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_foot_L_translateX";
	rename -uid "B5D88E24-4856-A205-C892-859B74F0B6DB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 -37.663342570219193 9 -53.389231718473866
		 22 -21.361273212098311 30 -37.663342570219193;
	setAttr -s 4 ".kit[0:3]"  1 18 18 1;
	setAttr -s 4 ".kot[0:3]"  1 18 18 1;
	setAttr -s 4 ".kix[0:3]"  0.014209890738129616 1 1 0.014851838350296021;
	setAttr -s 4 ".kiy[0:3]"  -0.99989908933639526 0 0 -0.99988967180252075;
	setAttr -s 4 ".kox[0:3]"  0.014209876768290997 1 1 0.014851835556328297;
	setAttr -s 4 ".koy[0:3]"  -0.99989908933639526 0 0 -0.99988967180252075;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_foot_L_translateY";
	rename -uid "3AEC0CCA-4E6A-49D4-5DC7-DDA203BBC686";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 59.36786430051005 4 54.811530728306394
		 18 91.722385791209874 30 59.36786430051005;
	setAttr -s 4 ".kit[0:3]"  1 18 18 1;
	setAttr -s 4 ".kot[0:3]"  1 18 18 1;
	setAttr -s 4 ".kix[0:3]"  0.015932882204651833 1 1 0.014042317867279053;
	setAttr -s 4 ".kiy[0:3]"  -0.99987304210662842 0 0 -0.9999014139175415;
	setAttr -s 4 ".kox[0:3]"  0.015932882204651833 1 1 0.014042329043149948;
	setAttr -s 4 ".koy[0:3]"  -0.99987304210662842 0 0 -0.9999014139175415;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_foot_L_translateZ";
	rename -uid "75A21D8B-4F29-E332-2F25-56B036139820";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 73.897548455626634 30 73.897548455626634;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_foot_L_rotateX";
	rename -uid "49B2BD0C-4919-C3DC-E99E-A0A735FD7406";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  0 43.778993984530459 5 54.962773113422102
		 9 60.484159868565641 24 37.068661870661259 30 43.778993984530459;
	setAttr -s 5 ".kit[2:4]"  18 18 1;
	setAttr -s 5 ".kot[2:4]"  18 18 1;
	setAttr -s 5 ".kix[0:4]"  0.70417499542236328 0.72982692718505859 
		1 1 0.78515100479125977;
	setAttr -s 5 ".kiy[0:4]"  0.71002650260925293 0.68363189697265625 
		0 0 0.619304358959198;
	setAttr -s 5 ".kox[0:4]"  0.70417505502700806 0.72982656955718994 
		1 1 0.78515100479125977;
	setAttr -s 5 ".koy[0:4]"  0.71002644300460815 0.68363219499588013 
		0 0 0.61930441856384277;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_foot_L_rotateY";
	rename -uid "D1D7BBFA-4CD9-8F61-1D69-ABBA41774FB2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 16.767104062783073 30 16.767104062783073;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_foot_L_rotateZ";
	rename -uid "67CCC23C-4640-85B8-8E43-A28B3CE13483";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -2.8011808701874688 30 -2.8011808701874688;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_foot_L_LowerLegTwist";
	rename -uid "C3C909A7-4B06-2927-8E84-D18EA8BE69D9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_foot_L_toeLift";
	rename -uid "8328D94E-4067-E6A8-AC98-AF8F95C96549";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_leg_L_PV_SPRING_visibility";
	rename -uid "9F6C8087-4C15-BEA4-8682-759A6534EC9A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_leg_L_PV_SPRING_translateX";
	rename -uid "841E46ED-4A66-0AB8-607D-D48F3CD04307";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_leg_L_PV_SPRING_translateY";
	rename -uid "084BA233-4FC3-7E40-2934-16857AFB7D1F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_leg_L_PV_SPRING_translateZ";
	rename -uid "114A4E65-4E70-2B63-F63F-CAB91A1A34D2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_leg_L_PV_SPRING_rotateX";
	rename -uid "C25C917B-47F2-6D55-375C-3397D06B4C0D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_leg_L_PV_SPRING_rotateY";
	rename -uid "FDED5559-490E-58DE-CCB2-9982EA5F2ADD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_leg_L_PV_SPRING_rotateZ";
	rename -uid "58DA633A-4673-5A0F-0B72-4386E1A7BCB3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_foot_R_translateX";
	rename -uid "98A537F8-469C-9510-9177-718AC2939654";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 24.33029150516041 4 27.340263175297579
		 18 11.627287547237687 30 24.33029150516041;
	setAttr -s 4 ".kit[0:3]"  1 18 18 1;
	setAttr -s 4 ".kot[0:3]"  1 18 18 1;
	setAttr -s 4 ".kix[0:3]"  0.042076781392097473 1 1 0.037713687866926193;
	setAttr -s 4 ".kiy[0:3]"  0.99911439418792725 0 0 0.99928861856460571;
	setAttr -s 4 ".kox[0:3]"  0.042076759040355682 1 1 0.037713725119829178;
	setAttr -s 4 ".koy[0:3]"  0.99911439418792725 0 0 0.99928861856460571;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_foot_R_translateY";
	rename -uid "150EDCCF-4142-62C2-5D65-B2BE300F5BA0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 110.05356156812243 2 107.52025404266776
		 17 150.5578652645662 30 110.05356156812243;
	setAttr -s 4 ".kit[0:3]"  1 18 18 1;
	setAttr -s 4 ".kot[0:3]"  1 18 18 1;
	setAttr -s 4 ".kix[0:3]"  0.026761174201965332 1 1 0.014416302554309368;
	setAttr -s 4 ".kiy[0:3]"  -0.99964189529418945 0 0 -0.99989604949951172;
	setAttr -s 4 ".kox[0:3]"  0.026761174201965332 1 1 0.014416302554309368;
	setAttr -s 4 ".koy[0:3]"  -0.99964189529418945 0 0 -0.99989604949951172;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_foot_R_translateZ";
	rename -uid "8C1AC891-430D-6779-7E5B-7A8455DA96B2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0.49448701104448389 30 0.49448701104448389;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_foot_R_rotateX";
	rename -uid "05221573-4AFF-23C3-D93D-A59F912F13D7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 70.615388242255207 4 74.876646272254732
		 23 53.290850342604976 30 70.615388242255207;
	setAttr -s 4 ".kit[0:3]"  1 18 18 1;
	setAttr -s 4 ".kot[0:3]"  1 18 18 1;
	setAttr -s 4 ".kix[0:3]"  0.75531113147735596 1 1 0.71389055252075195;
	setAttr -s 4 ".kiy[0:3]"  0.65536642074584961 0 0 0.70025736093521118;
	setAttr -s 4 ".kox[0:3]"  0.75531113147735596 1 1 0.7138904333114624;
	setAttr -s 4 ".koy[0:3]"  0.65536642074584961 0 0 0.70025736093521118;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_foot_R_rotateY";
	rename -uid "493AC0F8-4BFE-9E55-6B2C-C0B4037FFE78";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -23.008864726018615 30 -23.008864726018615;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_foot_R_rotateZ";
	rename -uid "E8968DC2-4340-76A2-A1F4-B3AAF74E7DBA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 17.366663964748604 30 17.366663964748604;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_foot_R_lowerLegTwist";
	rename -uid "01044FD8-40BF-9F58-D65E-FE917CF7120B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 20.5 30 20.5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_foot_R_toeLift";
	rename -uid "818B0A2E-4043-8794-C246-0FBE23F2C2C4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_leg_R_PV_SPRING_visibility";
	rename -uid "1D63656B-4DC7-9EEF-8769-C396753BA129";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_leg_R_PV_SPRING_translateX";
	rename -uid "98F2C68D-403A-1113-F914-9BB6C1A7F177";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_leg_R_PV_SPRING_translateY";
	rename -uid "655AFC28-4C5E-30F7-4F0D-31B2B0468E0D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_leg_R_PV_SPRING_translateZ";
	rename -uid "219E80E0-48D2-E461-AECB-2AA9AAF4CAF8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_leg_R_PV_SPRING_rotateX";
	rename -uid "2EACB4C6-4442-3A3E-E7C9-629072B32E41";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_leg_R_PV_SPRING_rotateY";
	rename -uid "CF3963DC-45F0-486F-A5E5-5DB6F0EB0DCD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_leg_R_PV_SPRING_rotateZ";
	rename -uid "74A78836-475E-DA3C-295D-EBBF1A8D8074";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_leg_L_upperTwist_visibility";
	rename -uid "B5A09799-4B62-6888-A168-A2836BEE2030";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_leg_L_upperTwist_translateX";
	rename -uid "FF04145C-4A8E-0984-FE58-749B106B5B78";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 15 -8.655451862609258 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_leg_L_upperTwist_translateY";
	rename -uid "86C5C7EB-43AC-33D8-0EF9-3CBDC0116E74";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 53.420412565629249 15 70.940438271988612
		 30 53.420412565629249;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_leg_L_upperTwist_translateZ";
	rename -uid "F2CE0B4B-4DD8-C425-E6D3-66B9E1FB4B70";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 15 16.154877821617944 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_leg_L_upperTwist_rotateX";
	rename -uid "B2C4B2F6-47EC-43FE-07F3-57B2969C859C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1.2526607225856179 30 1.2526607225856179;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_leg_L_upperTwist_rotateY";
	rename -uid "F8FE15AD-4259-19DF-82B1-CA913BA6C379";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_leg_L_upperTwist_rotateZ";
	rename -uid "F1798BD3-4B6E-9064-A9F1-F3B80B2B1414";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_leg_R_upperTwist_visibility";
	rename -uid "93BD1720-4650-26E5-287B-50953D586FAF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_leg_R_upperTwist_translateX";
	rename -uid "630C58FF-44D8-6FA8-6B4A-48A116C6BFCF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 15 7.1294089034025605 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_leg_R_upperTwist_translateY";
	rename -uid "3A5A451D-47A0-58B1-BCC7-4C87C6EB1ACF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 53.420412565629249 15 73.757290669947466
		 30 53.420412565629249;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_leg_R_upperTwist_translateZ";
	rename -uid "29F95EFF-4673-557D-DDD4-F38C2D6E76E6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 15 0.71411046271761303 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_leg_R_upperTwist_rotateX";
	rename -uid "EB900BB4-4628-BEA0-AD44-84BDDC70B1D3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 3.714386252016681 30 3.714386252016681;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_leg_R_upperTwist_rotateY";
	rename -uid "5D9B8623-4FB6-9103-34B0-778629391ADB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_leg_R_upperTwist_rotateZ";
	rename -uid "7CBCFC6E-40DC-A228-30C9-188A5792E880";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_hips_trans_visibility";
	rename -uid "1ECE8A64-49AB-58E5-0D57-1A8409C5A522";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_hips_trans_translateX";
	rename -uid "83BAFA32-4942-C536-4FF8-749D39F7B77E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_hips_trans_translateY";
	rename -uid "C06D3636-4587-9E8D-8765-559B3963A93A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 53.420412565629249 15 74.249372881046938
		 30 53.420412565629249;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_hips_trans_translateZ";
	rename -uid "E3128047-4790-9EE8-9743-93A3FF57A466";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 0 9 14.863885408573566 21 -17.114401708412462
		 30 0;
	setAttr -s 4 ".kit[0:3]"  1 18 18 1;
	setAttr -s 4 ".kot[0:3]"  1 18 18 1;
	setAttr -s 4 ".kix[0:3]"  0.013166814111173153 1 1 0.014382890425622463;
	setAttr -s 4 ".kiy[0:3]"  0.99991333484649658 0 0 0.9998965859413147;
	setAttr -s 4 ".kox[0:3]"  0.013166807591915131 1 1 0.014382892288267612;
	setAttr -s 4 ".koy[0:3]"  0.99991333484649658 0 0 0.9998965859413147;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_hips_trans_rotateX";
	rename -uid "E184A58F-4375-050A-839D-A7A16FB673F6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 0 6 3.5535227811259205 24 -4.4533429332117924
		 30 0;
	setAttr -s 4 ".kit[0:3]"  1 18 18 1;
	setAttr -s 4 ".kot[0:3]"  1 18 18 1;
	setAttr -s 4 ".kix[0:3]"  0.94569993019104004 1 1 0.93663537502288818;
	setAttr -s 4 ".kiy[0:3]"  0.32504108548164368 0 0 0.3503059446811676;
	setAttr -s 4 ".kox[0:3]"  0.94569998979568481 1 1 0.93663537502288818;
	setAttr -s 4 ".koy[0:3]"  0.32504090666770935 0 0 0.35030597448348999;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_hips_trans_rotateY";
	rename -uid "AA1A9D16-4EF2-200D-2B6C-1EB97C954E47";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_hips_trans_rotateZ";
	rename -uid "FD366330-4539-BEDA-DE69-67836781C302";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_hips_visibility";
	rename -uid "0B5D2A3E-41D9-7FEA-E209-1BB4E5E999A1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_hips_translateX";
	rename -uid "0CF22373-4260-A0CF-1273-5195CF937AD0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_hips_translateY";
	rename -uid "202268C7-47F4-3C11-194A-6C8C4041C571";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_hips_translateZ";
	rename -uid "EF5977D4-4D7A-6CAF-8AD8-48A02099DC2E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_hips_rotateX";
	rename -uid "55C08199-40E0-2504-F42B-398EA08A46CD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 3.8522664194439771 30 3.8522664194439771;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_hips_rotateY";
	rename -uid "B164398C-499F-584E-17C0-B389D1E64625";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -14.013794474357418 30 -14.013794474357418;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_hips_rotateZ";
	rename -uid "ACC28F8C-49FF-C7F3-D099-CAB5D46C129D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -26.465639012531128 30 -26.465639012531128;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_hip_up_visibility";
	rename -uid "FB5006FB-4EEC-37AC-A451-FEB527667C17";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_hip_up_translateX";
	rename -uid "784837B2-4EAB-C985-19F6-628538DE7D9D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_hip_up_translateY";
	rename -uid "22D011FE-45A8-DEC4-6809-A4869A8F3FE7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_hip_up_translateZ";
	rename -uid "EC3C1BFE-4A41-9980-FD51-70AFBDC8695A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_hip_up_rotateX";
	rename -uid "1D198082-465E-FB38-1699-198A7D15CAD9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -2.8151579249343839 30 -2.8151579249343839;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_hip_up_rotateY";
	rename -uid "73B415D3-4E9B-1590-7FDB-5593CFB8DD45";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_hip_up_rotateZ";
	rename -uid "5DF77CE2-49AA-D352-0D1A-48B6259D9F78";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_spine_1_visibility";
	rename -uid "5DF41086-4DE2-EF3D-474E-5F9F86592496";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_spine_1_translateX";
	rename -uid "7071D2CF-47FF-D96A-864B-5F8CBE0A89C3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_spine_1_translateY";
	rename -uid "3EABB806-43A0-44A2-98F1-E6B8D76EC9A7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_spine_1_translateZ";
	rename -uid "3D219A08-4E89-40BB-9FB8-09895350E5B0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_spine_1_rotateX";
	rename -uid "6652B7F3-4968-F70D-3F59-A69463F7A1D9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -2.8151579249343839 30 -2.8151579249343839;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_spine_1_rotateY";
	rename -uid "ECE2C62C-49C3-ED19-5492-6BB1C7351491";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_spine_1_rotateZ";
	rename -uid "9BE96B6B-4D9B-4FA7-0585-7B8957A0C7DD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_spine_2_visibility";
	rename -uid "9E64BCBF-4A5E-2E2A-4626-F4B0E6AD50C7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_spine_2_translateX";
	rename -uid "38153375-4674-D2BE-AA02-4FBE2FCA07E0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_spine_2_translateY";
	rename -uid "EFC3404A-46E6-3E1F-EC04-9DA0ED7DE31D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_spine_2_translateZ";
	rename -uid "327ED373-482E-E876-E648-4593FAE9EF0F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_spine_2_rotateX";
	rename -uid "CF27C415-48F6-2AD0-9838-4A986B108D11";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 5.6684312619152708 10 6.6451736595316868
		 22 4.6060004112101796 30 5.6684312619152708;
	setAttr -s 4 ".kit[0:3]"  1 18 18 1;
	setAttr -s 4 ".kot[0:3]"  1 18 18 1;
	setAttr -s 4 ".kix[0:3]"  0.99860769510269165 1 1 0.99738675355911255;
	setAttr -s 4 ".kiy[0:3]"  0.052750889211893082 0 0 0.072246775031089783;
	setAttr -s 4 ".kox[0:3]"  0.99860769510269165 1 1 0.99738675355911255;
	setAttr -s 4 ".koy[0:3]"  0.052750889211893082 0 0 0.072246775031089783;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_spine_2_rotateY";
	rename -uid "3BFA7AFB-4266-DF60-513A-859ACB392EB0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 -0.21587020214597938 10 -0.3859851625041924
		 22 -0.071125766206907373 30 -0.21587020214597938;
	setAttr -s 4 ".kit[0:3]"  1 18 18 1;
	setAttr -s 4 ".kot[0:3]"  1 18 18 1;
	setAttr -s 4 ".kix[0:3]"  0.99997085332870483 1 1 0.9999462366104126;
	setAttr -s 4 ".kiy[0:3]"  -0.0076338062062859535 0 0 -0.010368496179580688;
	setAttr -s 4 ".kox[0:3]"  0.99997085332870483 1 1 0.9999462366104126;
	setAttr -s 4 ".koy[0:3]"  -0.0076338122598826885 0 0 -0.010368501767516136;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_spine_2_rotateZ";
	rename -uid "B30159C0-4C6B-2A37-7A1E-E49D24D6D0B1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 -0.0017008035910356933 10 1.6284227661224833
		 22 -0.62300927360313763 30 -0.0017008035910356933;
	setAttr -s 4 ".kit[0:3]"  1 18 18 1;
	setAttr -s 4 ".kot[0:3]"  1 18 18 1;
	setAttr -s 4 ".kix[0:3]"  0.99589234590530396 1 1 0.99589234590530396;
	setAttr -s 4 ".kiy[0:3]"  0.090545192360877991 0 0 0.090545132756233215;
	setAttr -s 4 ".kox[0:3]"  0.99589234590530396 1 1 0.99589234590530396;
	setAttr -s 4 ".koy[0:3]"  0.090545132756233215 0 0 0.090545132756233215;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_spine_3_visibility";
	rename -uid "8CC63DB0-4ACC-97D9-0EC0-A89DD978F163";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_spine_3_translateX";
	rename -uid "EED19FFC-46D6-5158-C2F5-AC879A647B8D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_spine_3_translateY";
	rename -uid "6BC1C061-4F11-A80D-D0C3-7BAFC3459338";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_spine_3_translateZ";
	rename -uid "46495D72-4A42-F590-96B7-0DBCAB5DA4B9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_spine_3_rotateX";
	rename -uid "086B1263-47A5-448A-2989-E8813746D1F9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 5.6684312619152708 10 6.6451736595316868
		 22 4.6060004112101796 30 5.6684312619152708;
	setAttr -s 4 ".kit[0:3]"  1 18 18 1;
	setAttr -s 4 ".kot[0:3]"  1 18 18 1;
	setAttr -s 4 ".kix[0:3]"  0.99860769510269165 1 1 0.99738675355911255;
	setAttr -s 4 ".kiy[0:3]"  0.052750889211893082 0 0 0.072246775031089783;
	setAttr -s 4 ".kox[0:3]"  0.99860769510269165 1 1 0.99738675355911255;
	setAttr -s 4 ".koy[0:3]"  0.052750889211893082 0 0 0.072246775031089783;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_spine_3_rotateY";
	rename -uid "F5089823-438F-D2A1-E08C-E7A3A210A543";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 -0.21587020214597938 10 -0.3859851625041924
		 22 -0.071125766206907373 30 -0.21587020214597938;
	setAttr -s 4 ".kit[0:3]"  1 18 18 1;
	setAttr -s 4 ".kot[0:3]"  1 18 18 1;
	setAttr -s 4 ".kix[0:3]"  0.99997085332870483 1 1 0.9999462366104126;
	setAttr -s 4 ".kiy[0:3]"  -0.0076338062062859535 0 0 -0.010368496179580688;
	setAttr -s 4 ".kox[0:3]"  0.99997085332870483 1 1 0.9999462366104126;
	setAttr -s 4 ".koy[0:3]"  -0.0076338122598826885 0 0 -0.010368501767516136;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_spine_3_rotateZ";
	rename -uid "8C911522-4AC8-02B9-9A0E-6D99EC0D128D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 -0.0017008035910356933 10 1.6284227661224833
		 22 -0.62300927360313763 30 -0.0017008035910356933;
	setAttr -s 4 ".kit[0:3]"  1 18 18 1;
	setAttr -s 4 ".kot[0:3]"  1 18 18 1;
	setAttr -s 4 ".kix[0:3]"  0.99589234590530396 1 1 0.99589234590530396;
	setAttr -s 4 ".kiy[0:3]"  0.090545192360877991 0 0 0.090545132756233215;
	setAttr -s 4 ".kox[0:3]"  0.99589234590530396 1 1 0.99589234590530396;
	setAttr -s 4 ".koy[0:3]"  0.090545132756233215 0 0 0.090545132756233215;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_spine_4_visibility";
	rename -uid "D4CFDA20-499E-88CE-8D2A-A982AFAB709C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_spine_4_translateX";
	rename -uid "D7FE52E0-4682-341E-555A-90963E10F51D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_spine_4_translateY";
	rename -uid "A0572CC7-4738-9CCC-94BE-73AD66824359";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_spine_4_translateZ";
	rename -uid "15469147-4E30-DA52-9F79-F6AE50DA682C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_spine_4_rotateX";
	rename -uid "4577683E-4FC7-2E8B-129C-D5BF361E1CB9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 5.6684312619152708 10 6.6451736595316868
		 22 4.6060004112101796 30 5.6684312619152708;
	setAttr -s 4 ".kit[0:3]"  1 18 18 1;
	setAttr -s 4 ".kot[0:3]"  1 18 18 1;
	setAttr -s 4 ".kix[0:3]"  0.99860769510269165 1 1 0.99738675355911255;
	setAttr -s 4 ".kiy[0:3]"  0.052750889211893082 0 0 0.072246775031089783;
	setAttr -s 4 ".kox[0:3]"  0.99860769510269165 1 1 0.99738675355911255;
	setAttr -s 4 ".koy[0:3]"  0.052750889211893082 0 0 0.072246775031089783;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_spine_4_rotateY";
	rename -uid "211855A8-4DB7-95D6-7B6E-9383B1446433";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 -0.21587020214597938 10 -0.3859851625041924
		 22 -0.071125766206907373 30 -0.21587020214597938;
	setAttr -s 4 ".kit[0:3]"  1 18 18 1;
	setAttr -s 4 ".kot[0:3]"  1 18 18 1;
	setAttr -s 4 ".kix[0:3]"  0.99997085332870483 1 1 0.9999462366104126;
	setAttr -s 4 ".kiy[0:3]"  -0.0076338062062859535 0 0 -0.010368496179580688;
	setAttr -s 4 ".kox[0:3]"  0.99997085332870483 1 1 0.9999462366104126;
	setAttr -s 4 ".koy[0:3]"  -0.0076338122598826885 0 0 -0.010368501767516136;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_spine_4_rotateZ";
	rename -uid "F8DFE8F0-43EF-723C-AED6-ECBF344D980C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 -0.0017008035910356933 10 1.6284227661224833
		 22 -0.62300927360313763 30 -0.0017008035910356933;
	setAttr -s 4 ".kit[0:3]"  1 18 18 1;
	setAttr -s 4 ".kot[0:3]"  1 18 18 1;
	setAttr -s 4 ".kix[0:3]"  0.99589234590530396 1 1 0.99589234590530396;
	setAttr -s 4 ".kiy[0:3]"  0.090545192360877991 0 0 0.090545132756233215;
	setAttr -s 4 ".kox[0:3]"  0.99589234590530396 1 1 0.99589234590530396;
	setAttr -s 4 ".koy[0:3]"  0.090545132756233215 0 0 0.090545132756233215;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_wrist_R_IK_translateX";
	rename -uid "12BFF7D9-4CC3-6F76-C158-1285675875F0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 104.40168242949909 30 104.40168242949909;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_wrist_R_IK_translateY";
	rename -uid "7B5D3ABE-4507-EB21-B210-798E551D1F02";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 -84.791893632553993 7 -87.287286046923953
		 21 -79.451115187265913 30 -84.791893632553993;
	setAttr -s 4 ".kit[0:3]"  1 18 18 1;
	setAttr -s 4 ".kot[0:3]"  1 18 18 1;
	setAttr -s 4 ".kix[0:3]"  0.056519161909818649 1 1 0.051377620548009872;
	setAttr -s 4 ".kiy[0:3]"  -0.99840152263641357 0 0 -0.99867933988571167;
	setAttr -s 4 ".kox[0:3]"  0.056519180536270142 1 1 0.051377624273300171;
	setAttr -s 4 ".koy[0:3]"  -0.99840152263641357 0 0 -0.99867933988571167;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_wrist_R_IK_translateZ";
	rename -uid "366E4D01-49D9-9B55-6F9D-3B9FFB0FE178";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 89.921915781874219 7 91.01248055685852
		 21 87.757652279427575 30 89.921915781874219;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_wrist_R_IK_rotateX";
	rename -uid "02C8FF9B-48AE-9246-8B3E-8B80C6BA70B1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 106.92173356690202 30 106.92173356690202;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_wrist_R_IK_rotateY";
	rename -uid "8D4C1ECE-4075-FC11-07B6-A6A9B125FF3E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 26.66303385363091 30 26.66303385363091;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_wrist_R_IK_rotateZ";
	rename -uid "DB70B997-4400-53ED-BC43-5AA8C668D285";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 120.00638091442532 30 120.00638091442532;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_wrist_L_IK_translateX";
	rename -uid "51C9A83F-4D34-96C9-6C2F-1D93A37B97C1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -115.59432515744597 30 -115.59432515744597;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_wrist_L_IK_translateY";
	rename -uid "C7B96DD3-44EE-2E70-4BDA-919ED4281419";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 -74.983750961406216 7 -77.479143375776175
		 21 -69.642972516118135 30 -74.983750961406216;
	setAttr -s 4 ".kit[0:3]"  1 18 18 1;
	setAttr -s 4 ".kot[0:3]"  1 18 18 1;
	setAttr -s 4 ".kix[0:3]"  0.067668229341506958 1 1 0.067668229341506958;
	setAttr -s 4 ".kiy[0:3]"  -0.99770790338516235 0 0 -0.99770790338516235;
	setAttr -s 4 ".kox[0:3]"  0.067668139934539795 1 1 0.067668139934539795;
	setAttr -s 4 ".koy[0:3]"  -0.99770790338516235 0 0 -0.99770790338516235;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_wrist_L_IK_translateZ";
	rename -uid "3E5DA7F8-4403-2A0C-C5CA-EBB6ABA74237";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 30.843221430172875 7 31.933786205157158
		 21 28.678957927726227 30 30.843221430172875;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_wrist_L_IK_rotateX";
	rename -uid "77A59CB1-4699-2AD8-C368-60B0DAA97DD2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 140.28565122860005 30 140.28565122860005;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_wrist_L_IK_rotateY";
	rename -uid "CEB484A9-4699-8BC1-3530-59AD48EDD86B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -79.987278806373382 30 -79.987278806373382;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_wrist_L_IK_rotateZ";
	rename -uid "E7A018DB-4E59-AAF6-EA5D-3E98C752DB74";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -281.47435111255731 30 -281.47435111255731;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_arm_L_ikfk_ikfk";
	rename -uid "B6168EA1-435D-6F69-F805-BFAD68E4C9A1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_arm_R_ikfk_ikfk";
	rename -uid "7ECC2BB4-4717-551C-0493-F4B4FAF6F322";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_neck_1_visibility";
	rename -uid "CC204F26-428E-B30C-BF09-FFA3FBF4C189";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_neck_1_translateX";
	rename -uid "B24FC304-4EF9-7833-8AE0-E9B3056C6B3C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_neck_1_translateY";
	rename -uid "3F29E633-468B-A188-1EB7-3EACB5745043";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_neck_1_translateZ";
	rename -uid "EFFD3CA3-4F00-FF8F-A12B-70AE64AB81C5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_neck_1_rotateX";
	rename -uid "F8EE3DD3-4B51-5BE4-E3DC-DFADD9D8E4D0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_neck_1_rotateY";
	rename -uid "9C47621B-400A-A65C-788B-699C10ADECD6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_neck_1_rotateZ";
	rename -uid "8B9E42DE-4718-B690-0125-3B9145EBD681";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_neck_2_visibility";
	rename -uid "5D7B5E42-4C96-7122-FB40-ABBC37D5B017";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_neck_2_translateX";
	rename -uid "C46DC7A7-43E8-AA09-6B53-D0B68A803B0D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_neck_2_translateY";
	rename -uid "1B82BAB3-47AB-86B0-3B4B-A3AFCA2B4E51";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_neck_2_translateZ";
	rename -uid "EDB8A7AC-496E-4FEB-5DF3-36B00275AA17";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_neck_2_rotateX";
	rename -uid "A4180E48-4769-074F-94FB-FBA3891D7463";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_neck_2_rotateY";
	rename -uid "1BB1B285-4B21-30A5-0BB1-4B9C624A3B29";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_neck_2_rotateZ";
	rename -uid "5A0DA8B3-4ED0-D07B-AED7-52B3FEEF56A1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_head_visibility";
	rename -uid "8EC200B7-4B9E-BC7E-1247-61B0B0E8127E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_head_translateX";
	rename -uid "4CAAFD05-497E-5C03-F5D8-43B99C10EA62";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_head_translateY";
	rename -uid "3C9CCF5E-47E9-470F-8C60-D3AC2F50D475";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_head_translateZ";
	rename -uid "CDD0DE51-4042-6BED-D4BE-A0A0B45E167A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_head_rotateX";
	rename -uid "5AE14190-42B7-230A-0AC1-27872DCE1623";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 16.448277250355861 15 24.786752894802031
		 27 15.583329398645882 30 16.448277250355861;
	setAttr -s 4 ".kit[0:3]"  1 18 18 1;
	setAttr -s 4 ".kot[0:3]"  1 18 18 1;
	setAttr -s 4 ".kix[0:3]"  0.96243530511856079 1 1 0.98085242509841919;
	setAttr -s 4 ".kiy[0:3]"  0.27151128649711609 0 0 0.19475239515304565;
	setAttr -s 4 ".kox[0:3]"  0.96243530511856079 1 1 0.98085242509841919;
	setAttr -s 4 ".koy[0:3]"  0.27151128649711609 0 0 0.19475239515304565;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_head_rotateY";
	rename -uid "11C18DC7-4A41-03D4-49CD-ACAC62E45C5E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 4.9575894694815732 15 -0.64520312490877174
		 27 5.6649646764535841 30 4.9575894694815732;
	setAttr -s 4 ".kit[0:3]"  1 18 18 1;
	setAttr -s 4 ".kot[0:3]"  1 18 18 1;
	setAttr -s 4 ".kix[0:3]"  0.98042601346969604 1 1 0.98321992158889771;
	setAttr -s 4 ".kiy[0:3]"  -0.19688794016838074 0 0 -0.18242448568344116;
	setAttr -s 4 ".kox[0:3]"  0.98042601346969604 1 1 0.98321992158889771;
	setAttr -s 4 ".koy[0:3]"  -0.19688794016838074 0 0 -0.18242457509040833;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_head_rotateZ";
	rename -uid "4DA55389-4FA6-C765-C1B6-D0A48C10E21E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 -3.9350252225456219 15 -11.468088293060525
		 27 -3.1186307080057918 30 -3.9350252225456219;
	setAttr -s 4 ".kit[0:3]"  1 18 18 1;
	setAttr -s 4 ".kot[0:3]"  1 18 18 1;
	setAttr -s 4 ".kix[0:3]"  0.98355585336685181 1 1 0.98476237058639526;
	setAttr -s 4 ".kiy[0:3]"  -0.180604487657547 0 0 -0.17390522360801697;
	setAttr -s 4 ".kox[0:3]"  0.98355585336685181 1 1 0.98476237058639526;
	setAttr -s 4 ".koy[0:3]"  -0.180604487657547 0 0 -0.17390522360801697;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_ear_R_1_visibility";
	rename -uid "4A1718D0-4EBA-E343-847E-31B1BE06C02A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ear_R_1_translateX";
	rename -uid "81B2E472-4288-9D98-06E9-AFB7171EBA4A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ear_R_1_translateY";
	rename -uid "74C7373E-47F6-157A-13F4-5C9A81AA1BB3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ear_R_1_translateZ";
	rename -uid "FE5F0FAE-4227-86EE-B160-4981AB987FC0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_ear_R_1_rotateX";
	rename -uid "B1D21696-4325-CEAB-4299-FB93F48FA647";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_ear_R_1_rotateY";
	rename -uid "A53AD952-4B7E-FCA8-B97A-07821EEE3775";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 13 17.363359814733069 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_ear_R_1_rotateZ";
	rename -uid "AC02426B-44F4-6DAC-7725-3E8E87EBBD68";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_ear_R_1_scaleX";
	rename -uid "21E55531-4535-E683-2759-C9B66609A6BE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1.739017693937654 30 1.739017693937654;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_ear_R_1_scaleY";
	rename -uid "584A6D28-4204-DA61-FD82-47A4FD7DBE5D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 2.0927609399682505 30 2.0927609399682505;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_ear_R_1_scaleZ";
	rename -uid "817C1D1B-4DEF-3C15-07A4-4DBC353BDD7D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1.739017693937654 30 1.739017693937654;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_ear_R_2_visibility";
	rename -uid "1093522B-437F-F7A3-3475-38B35CF35985";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ear_R_2_translateX";
	rename -uid "84C2CE5E-4C63-FE79-4CBC-44A2970C775B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ear_R_2_translateY";
	rename -uid "215CF10D-49DE-74AA-258B-C291087DB622";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ear_R_2_translateZ";
	rename -uid "8A60940D-48F7-E9C4-A473-E08DAA497A94";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_ear_R_2_rotateX";
	rename -uid "29A09412-457E-0F8A-67F8-49B68C4EA090";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_ear_R_2_rotateY";
	rename -uid "DE2B636B-4B27-5FD2-FDDB-01A1F7FC8561";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_ear_R_2_rotateZ";
	rename -uid "DEA9C245-4CFC-3E63-57A5-58915CDE90E3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_ear_R_2_scaleX";
	rename -uid "E29257D7-474A-F64C-B54B-01A36E166414";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_ear_R_2_scaleY";
	rename -uid "3428769D-4FB4-C9A1-6D93-4CB13BADE812";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_ear_R_2_scaleZ";
	rename -uid "675D5069-4116-7139-90BD-478621C28087";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_ear_L_1_visibility";
	rename -uid "F8570E5C-4CBF-D89F-7B0B-14918CFD0675";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ear_L_1_translateX";
	rename -uid "5650AC44-468A-B79E-BBC6-618DD5E40414";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ear_L_1_translateY";
	rename -uid "6530DE8F-42FB-C89A-5DE6-06A2C507B8FB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ear_L_1_translateZ";
	rename -uid "A72C4C6C-4CFF-9323-A00A-A880EAD23477";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_ear_L_1_rotateX";
	rename -uid "EAA95D4D-447E-4A15-1221-298695D4968F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_ear_L_1_rotateY";
	rename -uid "7E7771AD-40A8-D5BF-CB57-FA97D17D4C65";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 13 -16.073941562225439 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_ear_L_1_rotateZ";
	rename -uid "589A92A1-4128-70AE-5F95-99BD2C8EBF96";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_ear_L_1_scaleX";
	rename -uid "F51EA7C3-4355-1BE1-3012-EDBA09A56956";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_ear_L_1_scaleY";
	rename -uid "87ED7A64-471F-D29F-4C83-D5A16F379383";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_ear_L_1_scaleZ";
	rename -uid "78A8795C-4E37-9CD6-BDBA-A383AEC7BA5A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_ear_L_2_visibility";
	rename -uid "8124C889-4BB9-C273-A2A8-0FB3E741D1D9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ear_L_2_translateX";
	rename -uid "4C7703A0-4EB6-721A-DE48-4C81509FBC11";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ear_L_2_translateY";
	rename -uid "E5013EC0-4F41-7BA3-B555-7FA42E561075";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ear_L_2_translateZ";
	rename -uid "96453B49-4648-F816-CEAA-7D96D8400045";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_ear_L_2_rotateX";
	rename -uid "0F23B833-4B0E-7762-47B4-FB84174DAB28";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_ear_L_2_rotateY";
	rename -uid "6916FB35-4C1D-6846-0823-E893FC29D490";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_ear_L_2_rotateZ";
	rename -uid "5E98AD5A-428C-1F25-67EE-FAA6B50FD755";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_ear_L_2_scaleX";
	rename -uid "D9171D3D-4C12-98F5-70B5-638C1293C1AF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_ear_L_2_scaleY";
	rename -uid "23B0FBD6-4C44-5F9E-DFDB-898AA676B140";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_ear_L_2_scaleZ";
	rename -uid "FDE0F695-480F-1393-D262-DF87E4F75549";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode pairBlend -n "pairBlend1";
	rename -uid "8CF27AB8-404D-CDFE-5377-FEA2A874C048";
createNode animCurveTL -n "pairBlend1_inTranslateX1";
	rename -uid "7298B3B5-4AF2-FDBD-77E6-39AF5C5341BC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -115.59432515744592 30 -115.59432515744592;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "pairBlend1_inTranslateY1";
	rename -uid "E654BDFF-46B2-1CBD-F0DA-619193BAC203";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -77.479143375776232 30 -77.479143375776232;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "pairBlend1_inTranslateZ1";
	rename -uid "CBAB7636-433C-BDAE-7341-37965DFD1ADF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 30.84322143017279 30 30.84322143017279;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "pairBlend1_inRotateX1";
	rename -uid "9F9268AC-4EDA-06E3-0E8D-658B55C24F8C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 38.519784652756812 30 38.519784652756812;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "pairBlend1_inRotateY1";
	rename -uid "C9F26F9F-481D-D96B-8290-D2BBF4FF705B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -79.724430114001379 30 -79.724430114001379;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "pairBlend1_inRotateZ1";
	rename -uid "7E6CACEA-4D2A-1F39-F28F-38AECCFD3A29";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -178.82631680872834 30 -178.82631680872834;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_fingers_L_figerCurl";
	rename -uid "502C6B38-4B07-99CD-F136-F7A510675CAF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_fingers_L_blendParent1";
	rename -uid "940719B7-4F5A-749D-E9D5-9CA212A13284";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_pointerF_L_1_visibility";
	rename -uid "57C9BE0E-4632-4660-8765-0EB369F7F606";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pointerF_L_1_translateX";
	rename -uid "9F815028-45CB-198E-90AC-74B31A278BA5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pointerF_L_1_translateY";
	rename -uid "4A074158-4377-77DC-BAAB-6FB7D711B177";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pointerF_L_1_translateZ";
	rename -uid "9F07496B-4BBE-A189-EAB8-FCA8386BB697";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_pointerF_L_2_visibility";
	rename -uid "B96E95F1-4117-CFC1-9428-5B85AB64764A";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kit[0:1]"  9 1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pointerF_L_2_translateX";
	rename -uid "E94CD443-417E-9D1F-7C7A-11B9E6888AEF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pointerF_L_2_translateY";
	rename -uid "80E4363D-49CA-E704-68B2-5AA5FA10B483";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pointerF_L_2_translateZ";
	rename -uid "8C92C7D2-4253-4937-C81C-2891143184BD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_pointerF_L_3_visibility";
	rename -uid "135E7950-457C-4024-22F6-A7A466E49732";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kit[0:1]"  9 1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pointerF_L_3_translateX";
	rename -uid "D4CB06BE-4BE8-D765-A8AB-5DA9F820EC6A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pointerF_L_3_translateY";
	rename -uid "DC67C28C-4D4C-09CE-8619-988BEEADC104";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pointerF_L_3_translateZ";
	rename -uid "BA573694-4221-962A-185E-C7B7143036DF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_middleF_L_1_visibility";
	rename -uid "C9A35E8B-4AFD-BAE1-99DB-3C914A5E8A3C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_middleF_L_1_translateX";
	rename -uid "2C9D3485-41C6-6321-746F-D89D9ECA16D3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_middleF_L_1_translateY";
	rename -uid "B07C5F71-4DCD-A9B6-AC08-A7AA3DF80FE9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_middleF_L_1_translateZ";
	rename -uid "264262B4-454E-9830-E880-7A828292EA0B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_middleF_L_2_visibility";
	rename -uid "446AA422-4D0D-0E02-37C5-55A10026A066";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kit[0:1]"  9 1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_middleF_L_2_translateX";
	rename -uid "0183A71B-45F1-E686-DF3B-8699314C22E0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_middleF_L_2_translateY";
	rename -uid "BBD4C5DF-44F3-4E01-DD17-EF95FF081E98";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_middleF_L_2_translateZ";
	rename -uid "C412D13C-4AEC-7E82-3A92-89B86ED1A863";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_middleF_L_3_visibility";
	rename -uid "A6BDEB9D-4403-D152-317A-3896580AF48E";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kit[0:1]"  9 1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_middleF_L_3_translateX";
	rename -uid "618D0729-4422-206E-FB57-4F8F7C593507";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_middleF_L_3_translateY";
	rename -uid "2CB06BD1-4CDE-85B6-4DC3-489823334D87";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_middleF_L_3_translateZ";
	rename -uid "024667CC-43EE-D9DF-F9AD-F0A47C198363";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_ringF_L_1_visibility";
	rename -uid "8294F948-4C0E-51E7-B6BB-6081A49A014C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ringF_L_1_translateX";
	rename -uid "81ED4384-4108-3248-8EB0-BAAE9AEE34BE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ringF_L_1_translateY";
	rename -uid "135288F6-4E79-EA2E-3279-EDB0A8939987";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ringF_L_1_translateZ";
	rename -uid "4B3A4413-44B2-38F5-4D85-E2B493DD6A57";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_ringF_L_2_visibility";
	rename -uid "C3EAC93F-4987-E78C-C18A-7986C3E4CF41";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kit[0:1]"  9 1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ringF_L_2_translateX";
	rename -uid "A0AC4D5C-48B0-DDEF-3FB2-5288DA48504E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ringF_L_2_translateY";
	rename -uid "70FD5AFA-475B-AF98-8635-2FBA08CCAEFC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ringF_L_2_translateZ";
	rename -uid "B61DF379-4CDB-A4F8-C6B0-FAA50BC4ACA9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_ringF_L_3_visibility";
	rename -uid "1C9D1E85-4F44-4011-8925-07B84169E9E3";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kit[0:1]"  9 1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ringF_L_3_translateX";
	rename -uid "92ACEC62-4021-07B8-85E9-08ADD07A3BD2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ringF_L_3_translateY";
	rename -uid "BB8496F3-4EAB-5B1E-E4BA-2EA45F1598DE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ringF_L_3_translateZ";
	rename -uid "1A39694E-4BB3-2051-7DC4-13BDA4AD9751";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_pinkyF_L_1_visibility";
	rename -uid "F1B28D25-4E5B-942B-55D5-E9AA370A95BE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pinkyF_L_1_translateX";
	rename -uid "858349D1-4664-BF95-30FC-4EBB16A9EEEF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pinkyF_L_1_translateY";
	rename -uid "58A1D035-49CD-DF1B-1BE3-C0B32BF49B50";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pinkyF_L_1_translateZ";
	rename -uid "7F145EAF-40EE-14E5-CFE0-3BA81BB10D5C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_pinkyF_L_2_visibility";
	rename -uid "C68D1F3D-489E-E3E9-57EB-7FB0A8CD2651";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kit[0:1]"  9 1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pinkyF_L_2_translateX";
	rename -uid "E85D23FE-4E35-6D06-10D1-FE8ADF891543";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pinkyF_L_2_translateY";
	rename -uid "73FDCADC-450A-0831-41FF-79A3CF49D743";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pinkyF_L_2_translateZ";
	rename -uid "53762152-495C-16B1-D1E2-21ABDD47F71D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_pinkyF_L_3_visibility";
	rename -uid "FF27D1BF-40E3-1AA8-52BC-D5A838DA1C6B";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kit[0:1]"  9 1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pinkyF_L_3_translateX";
	rename -uid "643F1B67-4C7C-3FA2-B397-23AFA1FCED48";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pinkyF_L_3_translateY";
	rename -uid "610637E9-4BD0-1DC0-535B-F29F5EA9B28C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pinkyF_L_3_translateZ";
	rename -uid "C4F6207F-4850-A7D9-B175-899B0A689255";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_thumb_L_1_visibility";
	rename -uid "331F7DB9-4938-0AC4-EB3B-9E88D5815303";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_thumb_L_1_translateX";
	rename -uid "88866E19-4AA3-B621-45B1-02BA59D21E95";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_thumb_L_1_translateY";
	rename -uid "9055088B-4E74-0AA6-C7A9-9C812914DB33";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_thumb_L_1_translateZ";
	rename -uid "78C3FBBB-4949-D40E-D5EE-68A331F3676E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_thumb_L_2_visibility";
	rename -uid "4072230E-4778-80D4-8FC2-278AFEA0B7E8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_thumb_L_2_translateX";
	rename -uid "25249BD7-475F-CFD3-A796-A7BC8C342EDF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_thumb_L_2_translateY";
	rename -uid "41EC436C-4CB5-EBD9-144C-C6B4EC082C0F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_thumb_L_2_translateZ";
	rename -uid "D5084DF8-466C-0C1A-D25A-52991EA06ECE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_thumb_L_3_visibility";
	rename -uid "B93EB264-400F-0AE2-20CE-FC98DFBE8B9D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_thumb_L_3_translateX";
	rename -uid "A27EFF2A-4152-0F93-563B-6181117BFFBF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_thumb_L_3_translateY";
	rename -uid "1FAF7A8B-46CA-8C9C-E211-B4A9661A9D3E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_thumb_L_3_translateZ";
	rename -uid "C7200D71-4C22-2754-1CE3-1C97E0B8F6BC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode pairBlend -n "pairBlend2";
	rename -uid "BC6A8188-425A-3DBF-3E3F-929917206CEA";
createNode animCurveTL -n "pairBlend2_inTranslateX1";
	rename -uid "335A1CA1-4DA6-9904-2BD1-D481EB06DF2F";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 104.40168242949902 30 104.40168242949902;
	setAttr -s 2 ".kit[1]"  18;
	setAttr -s 2 ".kot[1]"  18;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "pairBlend2_inTranslateY1";
	rename -uid "3539016E-4CD3-07A6-9145-D2B83EF56A22";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -87.287286046923839 30 -87.287286046923839;
	setAttr -s 2 ".kit[1]"  18;
	setAttr -s 2 ".kot[1]"  18;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "pairBlend2_inTranslateZ1";
	rename -uid "4610727D-4491-0B48-D5AC-58ACE85EBEF4";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 89.921915781874148 30 89.921915781874148;
	setAttr -s 2 ".kit[1]"  18;
	setAttr -s 2 ".kot[1]"  18;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "pairBlend2_inRotateX1";
	rename -uid "389ABD5E-4F08-D570-3CEF-7F8195B29AEB";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 106.92173356690203 30 106.92173356690203;
	setAttr -s 2 ".kit[1]"  18;
	setAttr -s 2 ".kot[1]"  18;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "pairBlend2_inRotateY1";
	rename -uid "C49B5C4D-4134-93E2-479F-EB860BDB8194";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 26.663033853630907 30 26.663033853630907;
	setAttr -s 2 ".kit[1]"  18;
	setAttr -s 2 ".kot[1]"  18;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "pairBlend2_inRotateZ1";
	rename -uid "2863A6A6-4055-EA3E-59ED-D3B9AF36A2C6";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 120.00638091442532 30 120.00638091442532;
	setAttr -s 2 ".kit[1]"  18;
	setAttr -s 2 ".kot[1]"  18;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_fingers_R_blendParent1";
	rename -uid "894F3F61-4603-28EC-A067-4F976389669F";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  18;
	setAttr -s 2 ".kot[1]"  18;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_fingers_R_fingerCurl";
	rename -uid "5F8C4D89-45E9-34C2-F8D4-65A0026381CA";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 2.6 30 2.6;
	setAttr -s 2 ".kit[1]"  18;
	setAttr -s 2 ".kot[1]"  18;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_pointerF_R_1_visibility";
	rename -uid "3EC6CA5C-4DB9-D53A-3D10-E281A120CF20";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pointerF_R_1_translateX";
	rename -uid "319C0DAA-4B4A-D02A-E959-A7A381D176F7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pointerF_R_1_translateY";
	rename -uid "BC3BC2E2-4F41-5A18-4DCA-6AB332FD17D0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pointerF_R_1_translateZ";
	rename -uid "D5125142-41E7-F48D-EC58-8588880E1627";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_pointerF_R_1_rotateX";
	rename -uid "0E2D8617-457E-14CD-4DF3-5AB5C07B41F3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_pointerF_R_1_rotateY";
	rename -uid "50BCA999-42E9-F626-EEFB-A1AAEF36F2F4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_pointerF_R_2_visibility";
	rename -uid "74670A75-4657-59BE-E00B-9CA29882A23D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pointerF_R_2_translateX";
	rename -uid "0D82B099-4CC0-E5B3-3AD3-3B90D0B55981";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pointerF_R_2_translateY";
	rename -uid "CBFEC5F6-469D-131F-7C2E-2C84E46A0EDF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pointerF_R_2_translateZ";
	rename -uid "661C913B-408A-A282-F48B-E89ECCB29ADB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_pointerF_R_2_rotateX";
	rename -uid "36E9754E-4663-16CD-8341-3EA441A3453C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_pointerF_R_2_rotateY";
	rename -uid "27962352-438A-2BEC-4DA3-44B24DFB5A4D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_pointerF_R_3_visibility";
	rename -uid "57DC5B6E-49EB-C07B-57D0-B5923CCCF13F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pointerF_R_3_translateX";
	rename -uid "61FBA601-4004-9F01-FF28-FCB5779D6EFE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pointerF_R_3_translateY";
	rename -uid "4182C1F6-4A9B-B994-7A34-AEAB70CD5A18";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pointerF_R_3_translateZ";
	rename -uid "2C951126-4BD0-A8DB-039E-8A8A6AB92B50";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_pointerF_R_3_rotateX";
	rename -uid "03BD2E85-45F3-840A-0C91-FFB39FFF261B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_pointerF_R_3_rotateY";
	rename -uid "BD44647C-4C34-EAE5-1DF0-10A7787D9FFD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_middleF_R_1_visibility";
	rename -uid "B2C03AE2-490F-1BE7-1DF4-539767E15798";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_middleF_R_1_translateX";
	rename -uid "E371118F-4879-A2F8-CCC8-E1A8066F2B5D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_middleF_R_1_translateY";
	rename -uid "4973F566-4965-E56D-DD97-1FAF3D697F32";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_middleF_R_1_translateZ";
	rename -uid "C3E14C17-4231-A499-D39C-8E913BFF8513";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_middleF_R_1_rotateX";
	rename -uid "B213A510-4B72-F1DC-EC4B-AA849375CEB8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_middleF_R_1_rotateY";
	rename -uid "534C1075-4EA6-5253-27CB-8CAE82557A7A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_middleF_R_2_visibility";
	rename -uid "C7F3CBB0-45FB-6856-B9EF-21BBE71A0709";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_middleF_R_2_translateX";
	rename -uid "EE05B1DA-4BB6-DF4E-6F45-72A2474A05E4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_middleF_R_2_translateY";
	rename -uid "54FC55FC-41BE-F7E7-9BA6-3E954EF929AA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_middleF_R_2_translateZ";
	rename -uid "E520AB1F-46E4-C975-30AA-8C825DC38E73";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_middleF_R_2_rotateX";
	rename -uid "8E6E7DA6-4C4E-B82B-C9C2-A084711255BA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_middleF_R_2_rotateY";
	rename -uid "2794542D-48C7-4068-387F-F9A69161B8B2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_middleF_R_3_visibility";
	rename -uid "F92D6745-4BE3-8481-325F-498D2E285BE2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_middleF_R_3_translateX";
	rename -uid "01AC4D02-4D09-B6E5-BFEB-A6A018F7BBF2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_middleF_R_3_translateY";
	rename -uid "4E914A06-4303-AE46-9467-95ACE658BAC7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_middleF_R_3_translateZ";
	rename -uid "C63EF9F5-4174-FB8C-9ACF-FFBCD76541F9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_middleF_R_3_rotateX";
	rename -uid "3F36E54C-4699-69A9-840E-919ACB671F2E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_middleF_R_3_rotateY";
	rename -uid "0E2B3CCD-4A5C-1026-0413-2FBBA459B3E0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_ringF_R_1_visibility";
	rename -uid "8B01D62D-43F9-7150-ECA1-05965D30736F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ringF_R_1_translateX";
	rename -uid "2A3899AF-4073-B672-5FA2-6299ABBD813C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ringF_R_1_translateY";
	rename -uid "6F4C556B-401B-BFDA-17D2-D8B367D22F88";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ringF_R_1_translateZ";
	rename -uid "3335A717-4FC3-6D65-D484-6AB0809EB8C1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_ringF_R_1_rotateX";
	rename -uid "1D64E1F2-41BE-6F53-7C25-8F831B0159D4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_ringF_R_1_rotateY";
	rename -uid "19AE64A4-4380-BBF8-403C-E188EA81B0FF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_ringF_R_2_visibility";
	rename -uid "C2DEF950-45F9-DD6F-A62E-BCA929BE8899";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ringF_R_2_translateX";
	rename -uid "10B1D960-46D0-A626-F05A-C6903D8BC740";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ringF_R_2_translateY";
	rename -uid "561720F5-4E39-0725-63E3-AE9F7928C354";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ringF_R_2_translateZ";
	rename -uid "03D61B1C-4A79-D20C-5C82-DE940A8C2EB6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_ringF_R_2_rotateX";
	rename -uid "DDC27B62-45F3-1C0D-6966-9694D6E1E944";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_ringF_R_2_rotateY";
	rename -uid "ABEEF5DC-4355-9635-E920-85A7C2CB06BA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_ringF_R_3_visibility";
	rename -uid "A856887F-4520-7C76-9C1C-59AF1CB1782A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ringF_R_3_translateX";
	rename -uid "9D7A8F4A-4B5E-37C9-C7BA-2899AB696133";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ringF_R_3_translateY";
	rename -uid "A6D7694D-4057-9245-EF1B-45AAA73E62E9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_ringF_R_3_translateZ";
	rename -uid "920E3182-47F1-F3ED-494A-9B8744AF40BF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_ringF_R_3_rotateX";
	rename -uid "5150B2BC-4530-5F74-F405-1EB9C8381A47";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_ringF_R_3_rotateY";
	rename -uid "72ED74D3-4F53-2621-CBFC-E6B7F6353296";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_pinkyF_R_1_visibility";
	rename -uid "A16937BE-4CA1-3E62-71DF-2480062C17E8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pinkyF_R_1_translateX";
	rename -uid "E9007BB5-4C4C-35DC-6DE1-C4974536E2B7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pinkyF_R_1_translateY";
	rename -uid "99C6CDCA-4314-FAC0-2A83-09864275E700";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pinkyF_R_1_translateZ";
	rename -uid "536866EA-4506-6414-EFC9-C6AE2684DBE9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_pinkyF_R_1_rotateX";
	rename -uid "29EB89A7-4280-606A-96F6-5496F6CB9EA6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_pinkyF_R_1_rotateY";
	rename -uid "D96FFA74-4057-4E7C-AD40-F8ADF8D9BBC0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_pinkyF_R_2_visibility";
	rename -uid "4587CA42-4544-0938-0F00-D18E9704A908";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pinkyF_R_2_translateX";
	rename -uid "A81B68B3-4AFB-5370-94C0-8E82C8256ACF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pinkyF_R_2_translateY";
	rename -uid "4755E7BF-4658-85C7-6881-4C9FF1C0B974";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pinkyF_R_2_translateZ";
	rename -uid "3DA5FDBE-436A-8C1D-B86B-279350BA9F6F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_pinkyF_R_2_rotateX";
	rename -uid "A134009F-41BF-D49B-3317-26BE5C0F199D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_pinkyF_R_2_rotateY";
	rename -uid "888A62BE-48B9-520D-E990-5180EC160C8D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_pinkyF_R_3_visibility";
	rename -uid "C31D928D-4D97-617D-04F2-138A29D6DB95";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pinkyF_R_3_translateX";
	rename -uid "9DC583DF-4D0B-BB23-7202-17953C52D619";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pinkyF_R_3_translateY";
	rename -uid "DE217954-4D0E-CB9B-15A3-949B62A73FDE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_pinkyF_R_3_translateZ";
	rename -uid "C20CDCE9-423C-6E11-484D-D1977F8AAE9F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_pinkyF_R_3_rotateX";
	rename -uid "12937B66-4EBF-C91C-6D44-228A422834B6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_pinkyF_R_3_rotateY";
	rename -uid "60B09247-4EBE-9742-F954-038FC1CC6160";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_thumb_R_1_visibility";
	rename -uid "BCECD39E-4314-CF86-261A-0ABF46103558";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_thumb_R_1_translateX";
	rename -uid "68931C02-44B0-CA86-8E16-C79C6F979E5A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_thumb_R_1_translateY";
	rename -uid "F4AD6F01-4975-8E18-92DF-44A5301F52FE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_thumb_R_1_translateZ";
	rename -uid "1EA7D46F-404B-93D3-F3EF-4BBBC7F84A6D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_thumb_R_2_visibility";
	rename -uid "4CF076E4-4F3B-9E0B-2E23-AEAABF96ED7C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_thumb_R_2_translateX";
	rename -uid "4E24B6D3-43F6-C493-22A3-419CFAB1DC3A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_thumb_R_2_translateY";
	rename -uid "4DDABB93-4E17-BE99-E412-4E8F925E3D62";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_thumb_R_2_translateZ";
	rename -uid "693B6A73-40F8-5EFC-8006-0D8FB80FE255";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_thumb_R_3_visibility";
	rename -uid "C949834A-4255-95CE-4D5B-708878E52CA4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_thumb_R_3_translateX";
	rename -uid "842F6D80-4FCC-7CC5-A4BD-27850CCF2095";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_thumb_R_3_translateY";
	rename -uid "271B9C6C-4673-271D-3EF4-97B004904A67";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_thumb_R_3_translateZ";
	rename -uid "1D25734F-4A5C-50C7-CE2C-CEB1D0162BB1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_shoulderpad_R_visibility";
	rename -uid "C028630F-448D-E2C7-2A72-24AFD9DBCCB2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_shoulderpad_R_translateX";
	rename -uid "CB4D8A91-4E41-8BB5-9228-ECA457F9A515";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_shoulderpad_R_translateY";
	rename -uid "23806A94-4EEE-40D6-FDC9-3DBF92EFB8DE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_shoulderpad_R_translateZ";
	rename -uid "FF9C372B-48F9-6CCF-FE39-71A539A9A3AB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_shoulderpad_R_rotateX";
	rename -uid "B22CE716-47FA-35D3-FE7E-438060DD22CB";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  0 -1.6088923636350496 2 0 11 7.5322326923918821
		 24 -4.0606599110581092 30 -1.6088923636350496;
	setAttr -s 5 ".kit[2:4]"  18 18 1;
	setAttr -s 5 ".kot[2:4]"  18 18 1;
	setAttr -s 5 ".kix[0:4]"  0.95230191946029663 0.93345534801483154 
		1 1 0.92275232076644897;
	setAttr -s 5 ".kiy[0:4]"  0.30515739321708679 0.35869359970092773 
		0 0 0.38539358973503113;
	setAttr -s 5 ".kox[0:4]"  0.95230191946029663 0.93345546722412109 
		1 1 0.92275232076644897;
	setAttr -s 5 ".koy[0:4]"  0.30515739321708679 0.35869327187538147 
		0 0 0.38539370894432068;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_shoulderpad_R_rotateY";
	rename -uid "4BBD7574-461D-EF18-E9A7-99BC3F79DD4B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_shoulderpad_R_rotateZ";
	rename -uid "55CD9315-4412-6CE8-3968-E38A0BDA3B7E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_shoulderpad_L_visibility";
	rename -uid "B9D52AC4-407A-C629-4ECF-3FBD51DEE4AA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_shoulderpad_L_translateX";
	rename -uid "9AEDBC9A-4CDD-5E05-DE1F-609E1AEC6348";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_shoulderpad_L_translateY";
	rename -uid "3AC6A65B-43E0-E194-242B-05A0E4FA2E2A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_shoulderpad_L_translateZ";
	rename -uid "7DD2578F-4783-FD46-6D08-B1B0251FF8CD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_shoulderpad_L_rotateX";
	rename -uid "4FF31908-48AD-F1E5-1A4F-8391884D3F8B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  0 -1.8108223136018591 2 0 11 7.5389900227471598
		 24 -3.9239830938316711 30 -1.8108223136018591;
	setAttr -s 5 ".kit[2:4]"  18 18 1;
	setAttr -s 5 ".kot[2:4]"  18 18 1;
	setAttr -s 5 ".kix[0:4]"  0.94078934192657471 0.89064866304397583 
		1 1 0.96492904424667358;
	setAttr -s 5 ".kiy[0:4]"  0.3389916718006134 0.45469227433204651 
		0 0 0.262510746717453;
	setAttr -s 5 ".kox[0:4]"  0.94078940153121948 0.89064854383468628 
		1 1 0.96492910385131836;
	setAttr -s 5 ".koy[0:4]"  0.33899176120758057 0.45469236373901367 
		0 0 0.26251077651977539;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_shoulderpad_L_rotateY";
	rename -uid "23BA7396-4005-938E-B7A4-AAA84950C527";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_shoulderpad_L_rotateZ";
	rename -uid "7A2434F5-40BD-C026-3F0A-63AD4196171C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_arm_L_IK_PV_translateX";
	rename -uid "D618BE76-4DC1-AFCD-AB6D-07B765C542FC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 9.0099471225937577 30 9.0099471225937577;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_arm_L_IK_PV_translateY";
	rename -uid "15C50277-4AEF-5175-8A89-A482F9A3B9C2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -110.42814235672154 30 -110.42814235672154;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_arm_L_IK_PV_translateZ";
	rename -uid "6FCE0BFB-417F-C2B6-4FBF-229A80C151AE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -66.860157248479595 30 -66.860157248479595;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_arm_L_IK_PV_rotateX";
	rename -uid "0C9FCDC9-4DEC-E1A4-F3F0-B4A416FF37E4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_arm_L_IK_PV_rotateY";
	rename -uid "F651EAFD-46D5-7824-0E5B-5096327D7875";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_arm_L_IK_PV_rotateZ";
	rename -uid "0A899991-453E-A99F-503A-CA825268782C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_arm_L_IK_PV_scaleX";
	rename -uid "7EE01C31-49CF-D33B-A778-AC86B10F1157";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_arm_L_IK_PV_scaleY";
	rename -uid "FE0D8893-4971-ECFE-371A-22B3C8D5EB86";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_arm_L_IK_PV_scaleZ";
	rename -uid "A17CDFA0-4BCC-23EA-BF55-E0A49B633F03";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_arm_R_IK_PV_translateX";
	rename -uid "970931B5-40A8-F030-F1EF-3090D6537474";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -50.518145519256962 30 -50.518145519256962;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_arm_R_IK_PV_translateY";
	rename -uid "A5ECFCA4-4533-FE26-F3B6-F397B4FE8741";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -94.724346540655517 30 -94.724346540655517;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_arm_R_IK_PV_translateZ";
	rename -uid "5282EA93-4DB8-C239-A713-929E3E66AAB2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 11.382826367814346 30 11.382826367814346;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_arm_R_IK_PV_rotateX";
	rename -uid "87E5637C-4BE3-AAE3-CEBB-D78C6F4EEAFB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_arm_R_IK_PV_rotateY";
	rename -uid "DED911BC-4E8C-D893-0B68-B9908A4DCD12";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_arm_R_IK_PV_rotateZ";
	rename -uid "6E44FB7E-403C-F216-598A-45A4F7B9E796";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_arm_R_IK_PV_scaleX";
	rename -uid "C025B7D1-4D59-1B07-763C-1099BAB9714A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_arm_R_IK_PV_scaleY";
	rename -uid "DB3FF5C6-446F-A1BF-6A49-7B9287193188";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_arm_R_IK_PV_scaleZ";
	rename -uid "9008C3A4-4BC2-EE30-2BEE-6D94234A70B4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_guitar_visibility";
	rename -uid "D1AF6B98-427B-DE15-9E07-D5997FFBE0E6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_guitar_translateX";
	rename -uid "C9E34D6C-4A94-6768-9F0A-A88219B6BDA2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 14.210481695334991 30 14.210481695334991;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_guitar_translateY";
	rename -uid "3FD0327F-4DA9-1A37-21F9-BDA91280418D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 14.792214794542495 7 11.855709080127875
		 21 20.617080609933922 30 14.792214794542495;
	setAttr -s 4 ".kit[0:3]"  1 18 18 1;
	setAttr -s 4 ".kot[0:3]"  1 18 18 1;
	setAttr -s 4 ".kix[0:3]"  0.040014982223510742 1 1 0.033406991511583328;
	setAttr -s 4 ".kiy[0:3]"  -0.99919909238815308 0 0 -0.99944180250167847;
	setAttr -s 4 ".kox[0:3]"  0.040014982223510742 1 1 0.033406965434551239;
	setAttr -s 4 ".koy[0:3]"  -0.99919909238815308 0 0 -0.99944180250167847;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "ctrl_guitar_translateZ";
	rename -uid "7FB25FB8-40FF-3959-0ABF-BC893A68F215";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 -1.497838984405875 7 -0.40727420942159009
		 21 -3.6621024868525276 30 -1.497838984405875;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_guitar_rotateX";
	rename -uid "359AF346-4C04-22B1-A705-30B67F774E4B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -15.133138020662605 30 -15.133138020662605;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_guitar_rotateY";
	rename -uid "E6956CE7-414B-D6C5-CC12-C2ABC4D7F48D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -0.29719482692686422 30 -0.29719482692686422;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "ctrl_guitar_rotateZ";
	rename -uid "32DEFD04-4727-F403-6306-D5A0A7157EB6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -0.46766116562646382 30 -0.46766116562646382;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_guitar_scaleX";
	rename -uid "AA9FFA1A-496D-1A6E-7B4C-028D0E80F391";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_guitar_scaleY";
	rename -uid "99A3C671-4077-A5A6-31B1-328BE50FEBC0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "ctrl_guitar_scaleZ";
	rename -uid "D05E1596-414F-6E34-F969-94B3230495F5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode gameFbxExporter -n "gameExporterPreset1";
	rename -uid "FB3022B3-45A0-8C71-53FD-5CBC351A1B13";
	setAttr ".pn" -type "string" "Model Default";
	setAttr ".ils" yes;
	setAttr ".esi" 3;
	setAttr ".ssn" -type "string" "Boss_rig_reference:BNDall";
	setAttr ".ebm" yes;
createNode gameFbxExporter -n "gameExporterPreset2";
	rename -uid "1D1ED807-46D8-4454-D8CF-198B818C66B8";
	setAttr ".pn" -type "string" "Anim Default";
	setAttr ".ils" yes;
	setAttr ".ilu" yes;
	setAttr ".eti" 2;
	setAttr ".esi" 3;
	setAttr ".ssn" -type "string" "Boss_rig_reference:BNDall";
	setAttr ".ac[0].acn" -type "string" "Boss_idle";
	setAttr ".ac[0].ace" 29;
	setAttr ".spt" 2;
	setAttr ".ebm" yes;
	setAttr ".exp" -type "string" "F:/Mary's Files/school_Spring 2016/DanShredder/Repo/capstone-2015-16-t7-git/Trunk/RockinRails/Art/Enemies/Boss//scenes";
	setAttr ".exf" -type "string" "anim_idle";
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 10;
	setAttr ".unw" 10;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 8 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 10 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".u";
select -ne :defaultRenderingList1;
	setAttr -s 2 ".r";
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 6 ".dsm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -s 2 ".gn";
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultResolution;
	setAttr -k on ".cch";
	setAttr -k on ".nds";
	setAttr -av ".w";
	setAttr -av ".h";
	setAttr ".pa" 1;
	setAttr -k on ".al";
	setAttr -av ".dar";
	setAttr -k on ".ldar";
	setAttr -k on ".off";
	setAttr -k on ".fld";
	setAttr -k on ".zsl";
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k off ".ctrs" 256;
	setAttr -av -k off ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -k on ".hwcc";
	setAttr -k on ".hwdp";
	setAttr -k on ".hwql";
	setAttr -k on ".hwfr";
select -ne :ikSystem;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".gsn";
	setAttr -k on ".gsv";
	setAttr -s 4 ".sol";
connectAttr "ctrl_root_translateX.o" "Boss_rig_referenceRN.phl[1]";
connectAttr "ctrl_root_translateY.o" "Boss_rig_referenceRN.phl[2]";
connectAttr "ctrl_root_translateZ.o" "Boss_rig_referenceRN.phl[3]";
connectAttr "ctrl_root_rotateX.o" "Boss_rig_referenceRN.phl[4]";
connectAttr "ctrl_root_rotateY.o" "Boss_rig_referenceRN.phl[5]";
connectAttr "ctrl_root_rotateZ.o" "Boss_rig_referenceRN.phl[6]";
connectAttr "ctrl_root_scaleX.o" "Boss_rig_referenceRN.phl[7]";
connectAttr "ctrl_root_scaleY.o" "Boss_rig_referenceRN.phl[8]";
connectAttr "ctrl_root_scaleZ.o" "Boss_rig_referenceRN.phl[9]";
connectAttr "ctrl_foot_L_LowerLegTwist.o" "Boss_rig_referenceRN.phl[10]";
connectAttr "ctrl_foot_L_toeLift.o" "Boss_rig_referenceRN.phl[11]";
connectAttr "ctrl_foot_L_translateX.o" "Boss_rig_referenceRN.phl[12]";
connectAttr "ctrl_foot_L_translateY.o" "Boss_rig_referenceRN.phl[13]";
connectAttr "ctrl_foot_L_translateZ.o" "Boss_rig_referenceRN.phl[14]";
connectAttr "ctrl_foot_L_rotateX.o" "Boss_rig_referenceRN.phl[15]";
connectAttr "ctrl_foot_L_rotateY.o" "Boss_rig_referenceRN.phl[16]";
connectAttr "ctrl_foot_L_rotateZ.o" "Boss_rig_referenceRN.phl[17]";
connectAttr "ctrl_leg_L_PV_SPRING_translateX.o" "Boss_rig_referenceRN.phl[18]";
connectAttr "ctrl_leg_L_PV_SPRING_translateY.o" "Boss_rig_referenceRN.phl[19]";
connectAttr "ctrl_leg_L_PV_SPRING_translateZ.o" "Boss_rig_referenceRN.phl[20]";
connectAttr "ctrl_leg_L_PV_SPRING_visibility.o" "Boss_rig_referenceRN.phl[21]";
connectAttr "ctrl_leg_L_PV_SPRING_rotateX.o" "Boss_rig_referenceRN.phl[22]";
connectAttr "ctrl_leg_L_PV_SPRING_rotateY.o" "Boss_rig_referenceRN.phl[23]";
connectAttr "ctrl_leg_L_PV_SPRING_rotateZ.o" "Boss_rig_referenceRN.phl[24]";
connectAttr "ctrl_foot_R_lowerLegTwist.o" "Boss_rig_referenceRN.phl[25]";
connectAttr "ctrl_foot_R_toeLift.o" "Boss_rig_referenceRN.phl[26]";
connectAttr "ctrl_foot_R_translateX.o" "Boss_rig_referenceRN.phl[27]";
connectAttr "ctrl_foot_R_translateY.o" "Boss_rig_referenceRN.phl[28]";
connectAttr "ctrl_foot_R_translateZ.o" "Boss_rig_referenceRN.phl[29]";
connectAttr "ctrl_foot_R_rotateX.o" "Boss_rig_referenceRN.phl[30]";
connectAttr "ctrl_foot_R_rotateY.o" "Boss_rig_referenceRN.phl[31]";
connectAttr "ctrl_foot_R_rotateZ.o" "Boss_rig_referenceRN.phl[32]";
connectAttr "ctrl_leg_R_PV_SPRING_translateX.o" "Boss_rig_referenceRN.phl[33]";
connectAttr "ctrl_leg_R_PV_SPRING_translateY.o" "Boss_rig_referenceRN.phl[34]";
connectAttr "ctrl_leg_R_PV_SPRING_translateZ.o" "Boss_rig_referenceRN.phl[35]";
connectAttr "ctrl_leg_R_PV_SPRING_visibility.o" "Boss_rig_referenceRN.phl[36]";
connectAttr "ctrl_leg_R_PV_SPRING_rotateX.o" "Boss_rig_referenceRN.phl[37]";
connectAttr "ctrl_leg_R_PV_SPRING_rotateY.o" "Boss_rig_referenceRN.phl[38]";
connectAttr "ctrl_leg_R_PV_SPRING_rotateZ.o" "Boss_rig_referenceRN.phl[39]";
connectAttr "ctrl_leg_L_upperTwist_rotateX.o" "Boss_rig_referenceRN.phl[40]";
connectAttr "ctrl_leg_L_upperTwist_rotateY.o" "Boss_rig_referenceRN.phl[41]";
connectAttr "ctrl_leg_L_upperTwist_rotateZ.o" "Boss_rig_referenceRN.phl[42]";
connectAttr "ctrl_leg_L_upperTwist_translateX.o" "Boss_rig_referenceRN.phl[43]";
connectAttr "ctrl_leg_L_upperTwist_translateY.o" "Boss_rig_referenceRN.phl[44]";
connectAttr "ctrl_leg_L_upperTwist_translateZ.o" "Boss_rig_referenceRN.phl[45]";
connectAttr "ctrl_leg_L_upperTwist_visibility.o" "Boss_rig_referenceRN.phl[46]";
connectAttr "ctrl_leg_R_upperTwist_rotateX.o" "Boss_rig_referenceRN.phl[47]";
connectAttr "ctrl_leg_R_upperTwist_rotateY.o" "Boss_rig_referenceRN.phl[48]";
connectAttr "ctrl_leg_R_upperTwist_rotateZ.o" "Boss_rig_referenceRN.phl[49]";
connectAttr "ctrl_leg_R_upperTwist_translateX.o" "Boss_rig_referenceRN.phl[50]";
connectAttr "ctrl_leg_R_upperTwist_translateY.o" "Boss_rig_referenceRN.phl[51]";
connectAttr "ctrl_leg_R_upperTwist_translateZ.o" "Boss_rig_referenceRN.phl[52]";
connectAttr "ctrl_leg_R_upperTwist_visibility.o" "Boss_rig_referenceRN.phl[53]";
connectAttr "ctrl_hips_trans_translateX.o" "Boss_rig_referenceRN.phl[54]";
connectAttr "ctrl_hips_trans_translateY.o" "Boss_rig_referenceRN.phl[55]";
connectAttr "ctrl_hips_trans_translateZ.o" "Boss_rig_referenceRN.phl[56]";
connectAttr "ctrl_hips_trans_rotateX.o" "Boss_rig_referenceRN.phl[57]";
connectAttr "ctrl_hips_trans_rotateY.o" "Boss_rig_referenceRN.phl[58]";
connectAttr "ctrl_hips_trans_rotateZ.o" "Boss_rig_referenceRN.phl[59]";
connectAttr "ctrl_hips_trans_visibility.o" "Boss_rig_referenceRN.phl[60]";
connectAttr "ctrl_hips_translateX.o" "Boss_rig_referenceRN.phl[61]";
connectAttr "ctrl_hips_translateY.o" "Boss_rig_referenceRN.phl[62]";
connectAttr "ctrl_hips_translateZ.o" "Boss_rig_referenceRN.phl[63]";
connectAttr "ctrl_hips_rotateX.o" "Boss_rig_referenceRN.phl[64]";
connectAttr "ctrl_hips_rotateY.o" "Boss_rig_referenceRN.phl[65]";
connectAttr "ctrl_hips_rotateZ.o" "Boss_rig_referenceRN.phl[66]";
connectAttr "ctrl_hips_visibility.o" "Boss_rig_referenceRN.phl[67]";
connectAttr "ctrl_hip_up_translateX.o" "Boss_rig_referenceRN.phl[68]";
connectAttr "ctrl_hip_up_translateY.o" "Boss_rig_referenceRN.phl[69]";
connectAttr "ctrl_hip_up_translateZ.o" "Boss_rig_referenceRN.phl[70]";
connectAttr "ctrl_hip_up_rotateX.o" "Boss_rig_referenceRN.phl[71]";
connectAttr "ctrl_hip_up_rotateY.o" "Boss_rig_referenceRN.phl[72]";
connectAttr "ctrl_hip_up_rotateZ.o" "Boss_rig_referenceRN.phl[73]";
connectAttr "ctrl_hip_up_visibility.o" "Boss_rig_referenceRN.phl[74]";
connectAttr "ctrl_spine_1_translateX.o" "Boss_rig_referenceRN.phl[75]";
connectAttr "ctrl_spine_1_translateY.o" "Boss_rig_referenceRN.phl[76]";
connectAttr "ctrl_spine_1_translateZ.o" "Boss_rig_referenceRN.phl[77]";
connectAttr "ctrl_spine_1_rotateX.o" "Boss_rig_referenceRN.phl[78]";
connectAttr "ctrl_spine_1_rotateY.o" "Boss_rig_referenceRN.phl[79]";
connectAttr "ctrl_spine_1_rotateZ.o" "Boss_rig_referenceRN.phl[80]";
connectAttr "ctrl_spine_1_visibility.o" "Boss_rig_referenceRN.phl[81]";
connectAttr "ctrl_spine_2_translateX.o" "Boss_rig_referenceRN.phl[82]";
connectAttr "ctrl_spine_2_translateY.o" "Boss_rig_referenceRN.phl[83]";
connectAttr "ctrl_spine_2_translateZ.o" "Boss_rig_referenceRN.phl[84]";
connectAttr "ctrl_spine_2_rotateX.o" "Boss_rig_referenceRN.phl[85]";
connectAttr "ctrl_spine_2_rotateY.o" "Boss_rig_referenceRN.phl[86]";
connectAttr "ctrl_spine_2_rotateZ.o" "Boss_rig_referenceRN.phl[87]";
connectAttr "ctrl_spine_2_visibility.o" "Boss_rig_referenceRN.phl[88]";
connectAttr "ctrl_spine_3_translateX.o" "Boss_rig_referenceRN.phl[89]";
connectAttr "ctrl_spine_3_translateY.o" "Boss_rig_referenceRN.phl[90]";
connectAttr "ctrl_spine_3_translateZ.o" "Boss_rig_referenceRN.phl[91]";
connectAttr "ctrl_spine_3_rotateX.o" "Boss_rig_referenceRN.phl[92]";
connectAttr "ctrl_spine_3_rotateY.o" "Boss_rig_referenceRN.phl[93]";
connectAttr "ctrl_spine_3_rotateZ.o" "Boss_rig_referenceRN.phl[94]";
connectAttr "ctrl_spine_3_visibility.o" "Boss_rig_referenceRN.phl[95]";
connectAttr "ctrl_spine_4_translateX.o" "Boss_rig_referenceRN.phl[96]";
connectAttr "ctrl_spine_4_translateY.o" "Boss_rig_referenceRN.phl[97]";
connectAttr "ctrl_spine_4_translateZ.o" "Boss_rig_referenceRN.phl[98]";
connectAttr "ctrl_spine_4_rotateX.o" "Boss_rig_referenceRN.phl[99]";
connectAttr "ctrl_spine_4_rotateY.o" "Boss_rig_referenceRN.phl[100]";
connectAttr "ctrl_spine_4_rotateZ.o" "Boss_rig_referenceRN.phl[101]";
connectAttr "ctrl_spine_4_visibility.o" "Boss_rig_referenceRN.phl[102]";
connectAttr "ctrl_wrist_R_IK_translateX.o" "Boss_rig_referenceRN.phl[103]";
connectAttr "ctrl_wrist_R_IK_translateY.o" "Boss_rig_referenceRN.phl[104]";
connectAttr "ctrl_wrist_R_IK_translateZ.o" "Boss_rig_referenceRN.phl[105]";
connectAttr "ctrl_wrist_R_IK_rotateX.o" "Boss_rig_referenceRN.phl[106]";
connectAttr "ctrl_wrist_R_IK_rotateY.o" "Boss_rig_referenceRN.phl[107]";
connectAttr "ctrl_wrist_R_IK_rotateZ.o" "Boss_rig_referenceRN.phl[108]";
connectAttr "ctrl_wrist_L_IK_translateX.o" "Boss_rig_referenceRN.phl[109]";
connectAttr "ctrl_wrist_L_IK_translateY.o" "Boss_rig_referenceRN.phl[110]";
connectAttr "ctrl_wrist_L_IK_translateZ.o" "Boss_rig_referenceRN.phl[111]";
connectAttr "ctrl_wrist_L_IK_rotateX.o" "Boss_rig_referenceRN.phl[112]";
connectAttr "ctrl_wrist_L_IK_rotateY.o" "Boss_rig_referenceRN.phl[113]";
connectAttr "ctrl_wrist_L_IK_rotateZ.o" "Boss_rig_referenceRN.phl[114]";
connectAttr "ctrl_arm_L_ikfk_ikfk.o" "Boss_rig_referenceRN.phl[115]";
connectAttr "ctrl_arm_R_ikfk_ikfk.o" "Boss_rig_referenceRN.phl[116]";
connectAttr "ctrl_neck_1_translateX.o" "Boss_rig_referenceRN.phl[117]";
connectAttr "ctrl_neck_1_translateY.o" "Boss_rig_referenceRN.phl[118]";
connectAttr "ctrl_neck_1_translateZ.o" "Boss_rig_referenceRN.phl[119]";
connectAttr "ctrl_neck_1_rotateX.o" "Boss_rig_referenceRN.phl[120]";
connectAttr "ctrl_neck_1_rotateY.o" "Boss_rig_referenceRN.phl[121]";
connectAttr "ctrl_neck_1_rotateZ.o" "Boss_rig_referenceRN.phl[122]";
connectAttr "ctrl_neck_1_visibility.o" "Boss_rig_referenceRN.phl[123]";
connectAttr "ctrl_neck_2_translateX.o" "Boss_rig_referenceRN.phl[124]";
connectAttr "ctrl_neck_2_translateY.o" "Boss_rig_referenceRN.phl[125]";
connectAttr "ctrl_neck_2_translateZ.o" "Boss_rig_referenceRN.phl[126]";
connectAttr "ctrl_neck_2_rotateX.o" "Boss_rig_referenceRN.phl[127]";
connectAttr "ctrl_neck_2_rotateY.o" "Boss_rig_referenceRN.phl[128]";
connectAttr "ctrl_neck_2_rotateZ.o" "Boss_rig_referenceRN.phl[129]";
connectAttr "ctrl_neck_2_visibility.o" "Boss_rig_referenceRN.phl[130]";
connectAttr "ctrl_head_translateX.o" "Boss_rig_referenceRN.phl[131]";
connectAttr "ctrl_head_translateY.o" "Boss_rig_referenceRN.phl[132]";
connectAttr "ctrl_head_translateZ.o" "Boss_rig_referenceRN.phl[133]";
connectAttr "ctrl_head_rotateX.o" "Boss_rig_referenceRN.phl[134]";
connectAttr "ctrl_head_rotateY.o" "Boss_rig_referenceRN.phl[135]";
connectAttr "ctrl_head_rotateZ.o" "Boss_rig_referenceRN.phl[136]";
connectAttr "ctrl_head_visibility.o" "Boss_rig_referenceRN.phl[137]";
connectAttr "ctrl_ear_R_1_translateX.o" "Boss_rig_referenceRN.phl[138]";
connectAttr "ctrl_ear_R_1_translateY.o" "Boss_rig_referenceRN.phl[139]";
connectAttr "ctrl_ear_R_1_translateZ.o" "Boss_rig_referenceRN.phl[140]";
connectAttr "ctrl_ear_R_1_rotateX.o" "Boss_rig_referenceRN.phl[141]";
connectAttr "ctrl_ear_R_1_rotateY.o" "Boss_rig_referenceRN.phl[142]";
connectAttr "ctrl_ear_R_1_rotateZ.o" "Boss_rig_referenceRN.phl[143]";
connectAttr "ctrl_ear_R_1_scaleX.o" "Boss_rig_referenceRN.phl[144]";
connectAttr "ctrl_ear_R_1_scaleY.o" "Boss_rig_referenceRN.phl[145]";
connectAttr "ctrl_ear_R_1_scaleZ.o" "Boss_rig_referenceRN.phl[146]";
connectAttr "ctrl_ear_R_1_visibility.o" "Boss_rig_referenceRN.phl[147]";
connectAttr "ctrl_ear_R_2_translateX.o" "Boss_rig_referenceRN.phl[148]";
connectAttr "ctrl_ear_R_2_translateY.o" "Boss_rig_referenceRN.phl[149]";
connectAttr "ctrl_ear_R_2_translateZ.o" "Boss_rig_referenceRN.phl[150]";
connectAttr "ctrl_ear_R_2_rotateX.o" "Boss_rig_referenceRN.phl[151]";
connectAttr "ctrl_ear_R_2_rotateY.o" "Boss_rig_referenceRN.phl[152]";
connectAttr "ctrl_ear_R_2_rotateZ.o" "Boss_rig_referenceRN.phl[153]";
connectAttr "ctrl_ear_R_2_scaleX.o" "Boss_rig_referenceRN.phl[154]";
connectAttr "ctrl_ear_R_2_scaleY.o" "Boss_rig_referenceRN.phl[155]";
connectAttr "ctrl_ear_R_2_scaleZ.o" "Boss_rig_referenceRN.phl[156]";
connectAttr "ctrl_ear_R_2_visibility.o" "Boss_rig_referenceRN.phl[157]";
connectAttr "ctrl_ear_L_1_translateX.o" "Boss_rig_referenceRN.phl[158]";
connectAttr "ctrl_ear_L_1_translateY.o" "Boss_rig_referenceRN.phl[159]";
connectAttr "ctrl_ear_L_1_translateZ.o" "Boss_rig_referenceRN.phl[160]";
connectAttr "ctrl_ear_L_1_rotateX.o" "Boss_rig_referenceRN.phl[161]";
connectAttr "ctrl_ear_L_1_rotateY.o" "Boss_rig_referenceRN.phl[162]";
connectAttr "ctrl_ear_L_1_rotateZ.o" "Boss_rig_referenceRN.phl[163]";
connectAttr "ctrl_ear_L_1_scaleX.o" "Boss_rig_referenceRN.phl[164]";
connectAttr "ctrl_ear_L_1_scaleY.o" "Boss_rig_referenceRN.phl[165]";
connectAttr "ctrl_ear_L_1_scaleZ.o" "Boss_rig_referenceRN.phl[166]";
connectAttr "ctrl_ear_L_1_visibility.o" "Boss_rig_referenceRN.phl[167]";
connectAttr "ctrl_ear_L_2_translateX.o" "Boss_rig_referenceRN.phl[168]";
connectAttr "ctrl_ear_L_2_translateY.o" "Boss_rig_referenceRN.phl[169]";
connectAttr "ctrl_ear_L_2_translateZ.o" "Boss_rig_referenceRN.phl[170]";
connectAttr "ctrl_ear_L_2_rotateX.o" "Boss_rig_referenceRN.phl[171]";
connectAttr "ctrl_ear_L_2_rotateY.o" "Boss_rig_referenceRN.phl[172]";
connectAttr "ctrl_ear_L_2_rotateZ.o" "Boss_rig_referenceRN.phl[173]";
connectAttr "ctrl_ear_L_2_scaleX.o" "Boss_rig_referenceRN.phl[174]";
connectAttr "ctrl_ear_L_2_scaleY.o" "Boss_rig_referenceRN.phl[175]";
connectAttr "ctrl_ear_L_2_scaleZ.o" "Boss_rig_referenceRN.phl[176]";
connectAttr "ctrl_ear_L_2_visibility.o" "Boss_rig_referenceRN.phl[177]";
connectAttr "ctrl_fingers_L_figerCurl.o" "Boss_rig_referenceRN.phl[178]";
connectAttr "pairBlend1.otx" "Boss_rig_referenceRN.phl[179]";
connectAttr "pairBlend1.oty" "Boss_rig_referenceRN.phl[180]";
connectAttr "pairBlend1.otz" "Boss_rig_referenceRN.phl[181]";
connectAttr "pairBlend1.orx" "Boss_rig_referenceRN.phl[182]";
connectAttr "pairBlend1.ory" "Boss_rig_referenceRN.phl[183]";
connectAttr "pairBlend1.orz" "Boss_rig_referenceRN.phl[184]";
connectAttr "ctrl_fingers_L_blendParent1.o" "Boss_rig_referenceRN.phl[185]";
connectAttr "ctrl_pointerF_L_1_translateX.o" "Boss_rig_referenceRN.phl[186]";
connectAttr "ctrl_pointerF_L_1_translateY.o" "Boss_rig_referenceRN.phl[187]";
connectAttr "ctrl_pointerF_L_1_translateZ.o" "Boss_rig_referenceRN.phl[188]";
connectAttr "ctrl_pointerF_L_1_visibility.o" "Boss_rig_referenceRN.phl[189]";
connectAttr "ctrl_pointerF_L_2_translateX.o" "Boss_rig_referenceRN.phl[190]";
connectAttr "ctrl_pointerF_L_2_translateY.o" "Boss_rig_referenceRN.phl[191]";
connectAttr "ctrl_pointerF_L_2_translateZ.o" "Boss_rig_referenceRN.phl[192]";
connectAttr "ctrl_pointerF_L_2_visibility.o" "Boss_rig_referenceRN.phl[193]";
connectAttr "ctrl_pointerF_L_3_translateX.o" "Boss_rig_referenceRN.phl[194]";
connectAttr "ctrl_pointerF_L_3_translateY.o" "Boss_rig_referenceRN.phl[195]";
connectAttr "ctrl_pointerF_L_3_translateZ.o" "Boss_rig_referenceRN.phl[196]";
connectAttr "ctrl_pointerF_L_3_visibility.o" "Boss_rig_referenceRN.phl[197]";
connectAttr "ctrl_middleF_L_1_translateX.o" "Boss_rig_referenceRN.phl[198]";
connectAttr "ctrl_middleF_L_1_translateY.o" "Boss_rig_referenceRN.phl[199]";
connectAttr "ctrl_middleF_L_1_translateZ.o" "Boss_rig_referenceRN.phl[200]";
connectAttr "ctrl_middleF_L_1_visibility.o" "Boss_rig_referenceRN.phl[201]";
connectAttr "ctrl_middleF_L_2_translateX.o" "Boss_rig_referenceRN.phl[202]";
connectAttr "ctrl_middleF_L_2_translateY.o" "Boss_rig_referenceRN.phl[203]";
connectAttr "ctrl_middleF_L_2_translateZ.o" "Boss_rig_referenceRN.phl[204]";
connectAttr "ctrl_middleF_L_2_visibility.o" "Boss_rig_referenceRN.phl[205]";
connectAttr "ctrl_middleF_L_3_translateX.o" "Boss_rig_referenceRN.phl[206]";
connectAttr "ctrl_middleF_L_3_translateY.o" "Boss_rig_referenceRN.phl[207]";
connectAttr "ctrl_middleF_L_3_translateZ.o" "Boss_rig_referenceRN.phl[208]";
connectAttr "ctrl_middleF_L_3_visibility.o" "Boss_rig_referenceRN.phl[209]";
connectAttr "ctrl_ringF_L_1_translateX.o" "Boss_rig_referenceRN.phl[210]";
connectAttr "ctrl_ringF_L_1_translateY.o" "Boss_rig_referenceRN.phl[211]";
connectAttr "ctrl_ringF_L_1_translateZ.o" "Boss_rig_referenceRN.phl[212]";
connectAttr "ctrl_ringF_L_1_visibility.o" "Boss_rig_referenceRN.phl[213]";
connectAttr "ctrl_ringF_L_2_translateX.o" "Boss_rig_referenceRN.phl[214]";
connectAttr "ctrl_ringF_L_2_translateY.o" "Boss_rig_referenceRN.phl[215]";
connectAttr "ctrl_ringF_L_2_translateZ.o" "Boss_rig_referenceRN.phl[216]";
connectAttr "ctrl_ringF_L_2_visibility.o" "Boss_rig_referenceRN.phl[217]";
connectAttr "ctrl_ringF_L_3_translateX.o" "Boss_rig_referenceRN.phl[218]";
connectAttr "ctrl_ringF_L_3_translateY.o" "Boss_rig_referenceRN.phl[219]";
connectAttr "ctrl_ringF_L_3_translateZ.o" "Boss_rig_referenceRN.phl[220]";
connectAttr "ctrl_ringF_L_3_visibility.o" "Boss_rig_referenceRN.phl[221]";
connectAttr "ctrl_pinkyF_L_1_translateX.o" "Boss_rig_referenceRN.phl[222]";
connectAttr "ctrl_pinkyF_L_1_translateY.o" "Boss_rig_referenceRN.phl[223]";
connectAttr "ctrl_pinkyF_L_1_translateZ.o" "Boss_rig_referenceRN.phl[224]";
connectAttr "ctrl_pinkyF_L_1_visibility.o" "Boss_rig_referenceRN.phl[225]";
connectAttr "ctrl_pinkyF_L_2_translateX.o" "Boss_rig_referenceRN.phl[226]";
connectAttr "ctrl_pinkyF_L_2_translateY.o" "Boss_rig_referenceRN.phl[227]";
connectAttr "ctrl_pinkyF_L_2_translateZ.o" "Boss_rig_referenceRN.phl[228]";
connectAttr "ctrl_pinkyF_L_2_visibility.o" "Boss_rig_referenceRN.phl[229]";
connectAttr "ctrl_pinkyF_L_3_translateX.o" "Boss_rig_referenceRN.phl[230]";
connectAttr "ctrl_pinkyF_L_3_translateY.o" "Boss_rig_referenceRN.phl[231]";
connectAttr "ctrl_pinkyF_L_3_translateZ.o" "Boss_rig_referenceRN.phl[232]";
connectAttr "ctrl_pinkyF_L_3_visibility.o" "Boss_rig_referenceRN.phl[233]";
connectAttr "ctrl_thumb_L_1_translateX.o" "Boss_rig_referenceRN.phl[234]";
connectAttr "ctrl_thumb_L_1_translateY.o" "Boss_rig_referenceRN.phl[235]";
connectAttr "ctrl_thumb_L_1_translateZ.o" "Boss_rig_referenceRN.phl[236]";
connectAttr "ctrl_thumb_L_1_visibility.o" "Boss_rig_referenceRN.phl[237]";
connectAttr "ctrl_thumb_L_2_translateX.o" "Boss_rig_referenceRN.phl[238]";
connectAttr "ctrl_thumb_L_2_translateY.o" "Boss_rig_referenceRN.phl[239]";
connectAttr "ctrl_thumb_L_2_translateZ.o" "Boss_rig_referenceRN.phl[240]";
connectAttr "ctrl_thumb_L_2_visibility.o" "Boss_rig_referenceRN.phl[241]";
connectAttr "ctrl_thumb_L_3_translateX.o" "Boss_rig_referenceRN.phl[242]";
connectAttr "ctrl_thumb_L_3_translateY.o" "Boss_rig_referenceRN.phl[243]";
connectAttr "ctrl_thumb_L_3_translateZ.o" "Boss_rig_referenceRN.phl[244]";
connectAttr "ctrl_thumb_L_3_visibility.o" "Boss_rig_referenceRN.phl[245]";
connectAttr "Boss_rig_referenceRN.phl[246]" "pairBlend1.itx2";
connectAttr "Boss_rig_referenceRN.phl[247]" "pairBlend1.ity2";
connectAttr "Boss_rig_referenceRN.phl[248]" "pairBlend1.itz2";
connectAttr "Boss_rig_referenceRN.phl[249]" "pairBlend1.irx2";
connectAttr "Boss_rig_referenceRN.phl[250]" "pairBlend1.iry2";
connectAttr "Boss_rig_referenceRN.phl[251]" "pairBlend1.irz2";
connectAttr "ctrl_fingers_R_fingerCurl.o" "Boss_rig_referenceRN.phl[252]";
connectAttr "pairBlend2.otx" "Boss_rig_referenceRN.phl[253]";
connectAttr "pairBlend2.oty" "Boss_rig_referenceRN.phl[254]";
connectAttr "pairBlend2.otz" "Boss_rig_referenceRN.phl[255]";
connectAttr "pairBlend2.orx" "Boss_rig_referenceRN.phl[256]";
connectAttr "pairBlend2.ory" "Boss_rig_referenceRN.phl[257]";
connectAttr "pairBlend2.orz" "Boss_rig_referenceRN.phl[258]";
connectAttr "ctrl_fingers_R_blendParent1.o" "Boss_rig_referenceRN.phl[259]";
connectAttr "ctrl_pointerF_R_1_rotateX.o" "Boss_rig_referenceRN.phl[260]";
connectAttr "ctrl_pointerF_R_1_rotateY.o" "Boss_rig_referenceRN.phl[261]";
connectAttr "ctrl_pointerF_R_1_translateX.o" "Boss_rig_referenceRN.phl[262]";
connectAttr "ctrl_pointerF_R_1_translateY.o" "Boss_rig_referenceRN.phl[263]";
connectAttr "ctrl_pointerF_R_1_translateZ.o" "Boss_rig_referenceRN.phl[264]";
connectAttr "ctrl_pointerF_R_1_visibility.o" "Boss_rig_referenceRN.phl[265]";
connectAttr "ctrl_pointerF_R_2_rotateX.o" "Boss_rig_referenceRN.phl[266]";
connectAttr "ctrl_pointerF_R_2_rotateY.o" "Boss_rig_referenceRN.phl[267]";
connectAttr "ctrl_pointerF_R_2_translateX.o" "Boss_rig_referenceRN.phl[268]";
connectAttr "ctrl_pointerF_R_2_translateY.o" "Boss_rig_referenceRN.phl[269]";
connectAttr "ctrl_pointerF_R_2_translateZ.o" "Boss_rig_referenceRN.phl[270]";
connectAttr "ctrl_pointerF_R_2_visibility.o" "Boss_rig_referenceRN.phl[271]";
connectAttr "ctrl_pointerF_R_3_rotateX.o" "Boss_rig_referenceRN.phl[272]";
connectAttr "ctrl_pointerF_R_3_rotateY.o" "Boss_rig_referenceRN.phl[273]";
connectAttr "ctrl_pointerF_R_3_translateX.o" "Boss_rig_referenceRN.phl[274]";
connectAttr "ctrl_pointerF_R_3_translateY.o" "Boss_rig_referenceRN.phl[275]";
connectAttr "ctrl_pointerF_R_3_translateZ.o" "Boss_rig_referenceRN.phl[276]";
connectAttr "ctrl_pointerF_R_3_visibility.o" "Boss_rig_referenceRN.phl[277]";
connectAttr "ctrl_middleF_R_1_rotateX.o" "Boss_rig_referenceRN.phl[278]";
connectAttr "ctrl_middleF_R_1_rotateY.o" "Boss_rig_referenceRN.phl[279]";
connectAttr "ctrl_middleF_R_1_translateX.o" "Boss_rig_referenceRN.phl[280]";
connectAttr "ctrl_middleF_R_1_translateY.o" "Boss_rig_referenceRN.phl[281]";
connectAttr "ctrl_middleF_R_1_translateZ.o" "Boss_rig_referenceRN.phl[282]";
connectAttr "ctrl_middleF_R_1_visibility.o" "Boss_rig_referenceRN.phl[283]";
connectAttr "ctrl_middleF_R_2_rotateX.o" "Boss_rig_referenceRN.phl[284]";
connectAttr "ctrl_middleF_R_2_rotateY.o" "Boss_rig_referenceRN.phl[285]";
connectAttr "ctrl_middleF_R_2_translateX.o" "Boss_rig_referenceRN.phl[286]";
connectAttr "ctrl_middleF_R_2_translateY.o" "Boss_rig_referenceRN.phl[287]";
connectAttr "ctrl_middleF_R_2_translateZ.o" "Boss_rig_referenceRN.phl[288]";
connectAttr "ctrl_middleF_R_2_visibility.o" "Boss_rig_referenceRN.phl[289]";
connectAttr "ctrl_middleF_R_3_rotateX.o" "Boss_rig_referenceRN.phl[290]";
connectAttr "ctrl_middleF_R_3_rotateY.o" "Boss_rig_referenceRN.phl[291]";
connectAttr "ctrl_middleF_R_3_translateX.o" "Boss_rig_referenceRN.phl[292]";
connectAttr "ctrl_middleF_R_3_translateY.o" "Boss_rig_referenceRN.phl[293]";
connectAttr "ctrl_middleF_R_3_translateZ.o" "Boss_rig_referenceRN.phl[294]";
connectAttr "ctrl_middleF_R_3_visibility.o" "Boss_rig_referenceRN.phl[295]";
connectAttr "ctrl_ringF_R_1_rotateX.o" "Boss_rig_referenceRN.phl[296]";
connectAttr "ctrl_ringF_R_1_rotateY.o" "Boss_rig_referenceRN.phl[297]";
connectAttr "ctrl_ringF_R_1_translateX.o" "Boss_rig_referenceRN.phl[298]";
connectAttr "ctrl_ringF_R_1_translateY.o" "Boss_rig_referenceRN.phl[299]";
connectAttr "ctrl_ringF_R_1_translateZ.o" "Boss_rig_referenceRN.phl[300]";
connectAttr "ctrl_ringF_R_1_visibility.o" "Boss_rig_referenceRN.phl[301]";
connectAttr "ctrl_ringF_R_2_rotateX.o" "Boss_rig_referenceRN.phl[302]";
connectAttr "ctrl_ringF_R_2_rotateY.o" "Boss_rig_referenceRN.phl[303]";
connectAttr "ctrl_ringF_R_2_translateX.o" "Boss_rig_referenceRN.phl[304]";
connectAttr "ctrl_ringF_R_2_translateY.o" "Boss_rig_referenceRN.phl[305]";
connectAttr "ctrl_ringF_R_2_translateZ.o" "Boss_rig_referenceRN.phl[306]";
connectAttr "ctrl_ringF_R_2_visibility.o" "Boss_rig_referenceRN.phl[307]";
connectAttr "ctrl_ringF_R_3_rotateX.o" "Boss_rig_referenceRN.phl[308]";
connectAttr "ctrl_ringF_R_3_rotateY.o" "Boss_rig_referenceRN.phl[309]";
connectAttr "ctrl_ringF_R_3_translateX.o" "Boss_rig_referenceRN.phl[310]";
connectAttr "ctrl_ringF_R_3_translateY.o" "Boss_rig_referenceRN.phl[311]";
connectAttr "ctrl_ringF_R_3_translateZ.o" "Boss_rig_referenceRN.phl[312]";
connectAttr "ctrl_ringF_R_3_visibility.o" "Boss_rig_referenceRN.phl[313]";
connectAttr "ctrl_pinkyF_R_1_rotateX.o" "Boss_rig_referenceRN.phl[314]";
connectAttr "ctrl_pinkyF_R_1_rotateY.o" "Boss_rig_referenceRN.phl[315]";
connectAttr "ctrl_pinkyF_R_1_translateX.o" "Boss_rig_referenceRN.phl[316]";
connectAttr "ctrl_pinkyF_R_1_translateY.o" "Boss_rig_referenceRN.phl[317]";
connectAttr "ctrl_pinkyF_R_1_translateZ.o" "Boss_rig_referenceRN.phl[318]";
connectAttr "ctrl_pinkyF_R_1_visibility.o" "Boss_rig_referenceRN.phl[319]";
connectAttr "ctrl_pinkyF_R_2_rotateX.o" "Boss_rig_referenceRN.phl[320]";
connectAttr "ctrl_pinkyF_R_2_rotateY.o" "Boss_rig_referenceRN.phl[321]";
connectAttr "ctrl_pinkyF_R_2_translateX.o" "Boss_rig_referenceRN.phl[322]";
connectAttr "ctrl_pinkyF_R_2_translateY.o" "Boss_rig_referenceRN.phl[323]";
connectAttr "ctrl_pinkyF_R_2_translateZ.o" "Boss_rig_referenceRN.phl[324]";
connectAttr "ctrl_pinkyF_R_2_visibility.o" "Boss_rig_referenceRN.phl[325]";
connectAttr "ctrl_pinkyF_R_3_rotateX.o" "Boss_rig_referenceRN.phl[326]";
connectAttr "ctrl_pinkyF_R_3_rotateY.o" "Boss_rig_referenceRN.phl[327]";
connectAttr "ctrl_pinkyF_R_3_translateX.o" "Boss_rig_referenceRN.phl[328]";
connectAttr "ctrl_pinkyF_R_3_translateY.o" "Boss_rig_referenceRN.phl[329]";
connectAttr "ctrl_pinkyF_R_3_translateZ.o" "Boss_rig_referenceRN.phl[330]";
connectAttr "ctrl_pinkyF_R_3_visibility.o" "Boss_rig_referenceRN.phl[331]";
connectAttr "ctrl_thumb_R_1_translateX.o" "Boss_rig_referenceRN.phl[332]";
connectAttr "ctrl_thumb_R_1_translateY.o" "Boss_rig_referenceRN.phl[333]";
connectAttr "ctrl_thumb_R_1_translateZ.o" "Boss_rig_referenceRN.phl[334]";
connectAttr "ctrl_thumb_R_1_visibility.o" "Boss_rig_referenceRN.phl[335]";
connectAttr "ctrl_thumb_R_2_translateX.o" "Boss_rig_referenceRN.phl[336]";
connectAttr "ctrl_thumb_R_2_translateY.o" "Boss_rig_referenceRN.phl[337]";
connectAttr "ctrl_thumb_R_2_translateZ.o" "Boss_rig_referenceRN.phl[338]";
connectAttr "ctrl_thumb_R_2_visibility.o" "Boss_rig_referenceRN.phl[339]";
connectAttr "ctrl_thumb_R_3_translateX.o" "Boss_rig_referenceRN.phl[340]";
connectAttr "ctrl_thumb_R_3_translateY.o" "Boss_rig_referenceRN.phl[341]";
connectAttr "ctrl_thumb_R_3_translateZ.o" "Boss_rig_referenceRN.phl[342]";
connectAttr "ctrl_thumb_R_3_visibility.o" "Boss_rig_referenceRN.phl[343]";
connectAttr "Boss_rig_referenceRN.phl[344]" "pairBlend2.itx2";
connectAttr "Boss_rig_referenceRN.phl[345]" "pairBlend2.ity2";
connectAttr "Boss_rig_referenceRN.phl[346]" "pairBlend2.itz2";
connectAttr "Boss_rig_referenceRN.phl[347]" "pairBlend2.irx2";
connectAttr "Boss_rig_referenceRN.phl[348]" "pairBlend2.iry2";
connectAttr "Boss_rig_referenceRN.phl[349]" "pairBlend2.irz2";
connectAttr "ctrl_shoulderpad_R_translateX.o" "Boss_rig_referenceRN.phl[350]";
connectAttr "ctrl_shoulderpad_R_translateY.o" "Boss_rig_referenceRN.phl[351]";
connectAttr "ctrl_shoulderpad_R_translateZ.o" "Boss_rig_referenceRN.phl[352]";
connectAttr "ctrl_shoulderpad_R_rotateX.o" "Boss_rig_referenceRN.phl[353]";
connectAttr "ctrl_shoulderpad_R_rotateY.o" "Boss_rig_referenceRN.phl[354]";
connectAttr "ctrl_shoulderpad_R_rotateZ.o" "Boss_rig_referenceRN.phl[355]";
connectAttr "ctrl_shoulderpad_R_visibility.o" "Boss_rig_referenceRN.phl[356]";
connectAttr "ctrl_shoulderpad_L_translateX.o" "Boss_rig_referenceRN.phl[357]";
connectAttr "ctrl_shoulderpad_L_translateY.o" "Boss_rig_referenceRN.phl[358]";
connectAttr "ctrl_shoulderpad_L_translateZ.o" "Boss_rig_referenceRN.phl[359]";
connectAttr "ctrl_shoulderpad_L_rotateX.o" "Boss_rig_referenceRN.phl[360]";
connectAttr "ctrl_shoulderpad_L_rotateY.o" "Boss_rig_referenceRN.phl[361]";
connectAttr "ctrl_shoulderpad_L_rotateZ.o" "Boss_rig_referenceRN.phl[362]";
connectAttr "ctrl_shoulderpad_L_visibility.o" "Boss_rig_referenceRN.phl[363]";
connectAttr "ctrl_arm_L_IK_PV_translateX.o" "Boss_rig_referenceRN.phl[364]";
connectAttr "ctrl_arm_L_IK_PV_translateY.o" "Boss_rig_referenceRN.phl[365]";
connectAttr "ctrl_arm_L_IK_PV_translateZ.o" "Boss_rig_referenceRN.phl[366]";
connectAttr "ctrl_arm_L_IK_PV_rotateX.o" "Boss_rig_referenceRN.phl[367]";
connectAttr "ctrl_arm_L_IK_PV_rotateY.o" "Boss_rig_referenceRN.phl[368]";
connectAttr "ctrl_arm_L_IK_PV_rotateZ.o" "Boss_rig_referenceRN.phl[369]";
connectAttr "ctrl_arm_L_IK_PV_scaleX.o" "Boss_rig_referenceRN.phl[370]";
connectAttr "ctrl_arm_L_IK_PV_scaleY.o" "Boss_rig_referenceRN.phl[371]";
connectAttr "ctrl_arm_L_IK_PV_scaleZ.o" "Boss_rig_referenceRN.phl[372]";
connectAttr "ctrl_arm_R_IK_PV_translateX.o" "Boss_rig_referenceRN.phl[373]";
connectAttr "ctrl_arm_R_IK_PV_translateY.o" "Boss_rig_referenceRN.phl[374]";
connectAttr "ctrl_arm_R_IK_PV_translateZ.o" "Boss_rig_referenceRN.phl[375]";
connectAttr "ctrl_arm_R_IK_PV_rotateX.o" "Boss_rig_referenceRN.phl[376]";
connectAttr "ctrl_arm_R_IK_PV_rotateY.o" "Boss_rig_referenceRN.phl[377]";
connectAttr "ctrl_arm_R_IK_PV_rotateZ.o" "Boss_rig_referenceRN.phl[378]";
connectAttr "ctrl_arm_R_IK_PV_scaleX.o" "Boss_rig_referenceRN.phl[379]";
connectAttr "ctrl_arm_R_IK_PV_scaleY.o" "Boss_rig_referenceRN.phl[380]";
connectAttr "ctrl_arm_R_IK_PV_scaleZ.o" "Boss_rig_referenceRN.phl[381]";
connectAttr "ctrl_guitar_translateX.o" "Boss_rig_referenceRN.phl[382]";
connectAttr "ctrl_guitar_translateY.o" "Boss_rig_referenceRN.phl[383]";
connectAttr "ctrl_guitar_translateZ.o" "Boss_rig_referenceRN.phl[384]";
connectAttr "ctrl_guitar_rotateX.o" "Boss_rig_referenceRN.phl[385]";
connectAttr "ctrl_guitar_rotateY.o" "Boss_rig_referenceRN.phl[386]";
connectAttr "ctrl_guitar_rotateZ.o" "Boss_rig_referenceRN.phl[387]";
connectAttr "ctrl_guitar_scaleX.o" "Boss_rig_referenceRN.phl[388]";
connectAttr "ctrl_guitar_scaleY.o" "Boss_rig_referenceRN.phl[389]";
connectAttr "ctrl_guitar_scaleZ.o" "Boss_rig_referenceRN.phl[390]";
connectAttr "ctrl_guitar_visibility.o" "Boss_rig_referenceRN.phl[391]";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "pairBlend1_inTranslateX1.o" "pairBlend1.itx1";
connectAttr "pairBlend1_inTranslateY1.o" "pairBlend1.ity1";
connectAttr "pairBlend1_inTranslateZ1.o" "pairBlend1.itz1";
connectAttr "pairBlend1_inRotateX1.o" "pairBlend1.irx1";
connectAttr "pairBlend1_inRotateY1.o" "pairBlend1.iry1";
connectAttr "pairBlend1_inRotateZ1.o" "pairBlend1.irz1";
connectAttr "pairBlend2_inTranslateX1.o" "pairBlend2.itx1";
connectAttr "pairBlend2_inTranslateY1.o" "pairBlend2.ity1";
connectAttr "pairBlend2_inTranslateZ1.o" "pairBlend2.itz1";
connectAttr "pairBlend2_inRotateX1.o" "pairBlend2.irx1";
connectAttr "pairBlend2_inRotateY1.o" "pairBlend2.iry1";
connectAttr "pairBlend2_inRotateZ1.o" "pairBlend2.irz1";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Boss_anim_Idle_v04.ma
